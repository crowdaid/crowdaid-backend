import { Meteor } from 'meteor/meteor';

Meteor.startup(() => {
  const links = document.querySelectorAll('.link');
  const linkOnClick = (e) => {
    window.open(Meteor.settings.public[`${e.currentTarget.dataset.target}Interface`]);
  };
  links.forEach((link) => {
    link.onclick = linkOnClick;
  });
});
