import SimpleSchema from 'simpl-schema';
import { ValidatedMethod } from 'meteor/mdg:validated-method';
import NotificationsCollection from './schema';

export const notify = new ValidatedMethod({
  name: 'notifications.methods.notify',
  validate: NotificationsCollection.schema.pick(
    'to_ids',
    'type',
    'data',
  ).validator({ clean: true }),
  run({ to_ids, type, data }) {
    return NotificationsCollection.insert({
      to_ids,
      type,
      data,
    });
  },
});

export const markRead = new ValidatedMethod({
  name: 'notifications.methods.read',
  validate: new SimpleSchema({
    _id: {
      type: String,
      regEx: SimpleSchema.RegEx.Id,
    },
    userId: {
      type: String,
      regEx: SimpleSchema.RegEx.Id,
    }
  }).validator({ clean: true }),
  run({ _id, userId }) {
    return NotificationsCollection.update({_id: _id, 'seen.id': {$ne: userId}}, {
      $push: {
        seen: {
          id: userId,
          date: new Date,
        },
      }
    });
  },
});

export const markUnread = new ValidatedMethod({
  name: 'notifications.methods.unread',
  validate: new SimpleSchema({
    _id: {
      type: String,
      regEx: SimpleSchema.RegEx.Id,
    },
    userId: {
      type: String,
      regEx: SimpleSchema.RegEx.Id,
    },
  }).validator({ clean: true }),
  run({ _id, userId }) {
    const Notification = NotificationsCollection.find(_id);
    return NotificationsCollection.update(_id, {
      $set: {
        seen: $.grep(Notification.seen, function(s) {
          return s.id != userId;
        }),
      }
    });
  },
});

export const removeNotification = new ValidatedMethod({
  name: 'notifications.methods.remove',
  validate: new SimpleSchema({
    _id: {
      type: String,
      regEx: SimpleSchema.RegEx.Id,
    },
  }).validator({ clean: true }),
  run({ _id }) {
    return NotificationsCollection.remove(_id);
  },
});
