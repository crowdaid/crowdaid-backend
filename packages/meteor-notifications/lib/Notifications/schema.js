// Meteor core import
import { Mongo } from 'meteor/mongo';
import { Tracker } from 'meteor/tracker';

// node imports
import SimpleSchema from 'simpl-schema';

const NotificationsCollection = new Mongo.Collection('notifications');

NotificationsCollection.schema = new SimpleSchema({
  _id: {
    type: String,
    regEx: SimpleSchema.RegEx.Id,
  },
  to_ids: {
    type: Array,
  },
  'to_ids.$': {
    type: String,
    regEx: SimpleSchema.RegEx.Id,
  },
  type: {
    type: String,
  },
  data: {
    type: Object,
    blackbox: true,
  },
  seen: {
    type: Array,
    optional: true,
  },
  'seen.$': {
    type: Object,
  },
  'seen.$.id': {
    type: String,
    regEx: SimpleSchema.RegEx.Id,
  },
  'seen.$.date': {
    type: Date,
  },
  createdDate: {
    type: Date,
    defaultValue: new Date,
  },
  createdTime: {
    type: Number,
    defaultValue: Math.round(Number(new Date().getTime())/1000), // Unix timestamp
  }
}, { trimStrings: true, tracker: Tracker });

NotificationsCollection.attachSchema(NotificationsCollection.schema);

NotificationsCollection.deny({
  insert() {
    return true;
  },
  update() {
    return true;
  },
  remove() {
    return true;
  },
});

export default NotificationsCollection;
