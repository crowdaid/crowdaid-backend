import { Meteor } from 'meteor/meteor'
import NotificationsCollection from './Notifications/schema'
import { notify, markRead, markUnread, removeNotification } from './Notifications/methods'

class Notifications_ {
  constructor () {
    this.config = {};
  }

  configure (config) {
    this.config = config;
  }

  unreadCount() {
    const userId = Meteor.userId();
    return NotificationsCollection.find({'seen.id': {$ne: userId}}).count();
  }

  get(limit) {
    const self = this;
    const userId = Meteor.userId();
    limit = limit || 7;
    return NotificationsCollection.find({to_ids: userId}, {
      limit: limit, sort: {createdTime: -1}
    }).fetch().map(not => {
      not.seen = not.seen ? not.seen : [];
      const seen = not.seen.find(s => s.id === userId);
      not.read = seen !== undefined;
      not.readAt = seen ? seen.date : '';
      return not;
    });
  }

  // Create notification
  notify(userIds, type, data, done) {
    userIds = userIds instanceof Array ? userIds : [userIds];
    data = data || {};
    notify.call({
      to_ids: userIds,
      type: type,
      data: data,
    }, (err, res) => {
      if (err) {
        console.error(err);
      } else {
        if (done && done instanceof Function) {
          done(res);
        }
      }
    })
  }

  markRead(notId, userId, done) {
    markRead.call({
      _id: notId,
      userId: userId,
    }, (err, res) => {
      if (err) {
        console.error(err);
      } else {
        if (done && done instanceof Function) {
          done(notId);
        }
      }
    })
  }

  markUnread(notId, userId, done) {
    markUnread.call({
      _id: notId,
      userId: userId,
    }, (err, res) => {
      if (err) {
        console.error(err);
      } else {
        if (done && done instanceof Function) {
          done(notId);
        }
      }
    })
  }

  remove(notId, done) {
    removeNotification.call({ _id: notId } , (err, res) => {
      if (err) {
        console.error(err);
      } else {
        if (done && done instanceof Function) {
          done(notId);
        }
      }
    })
  }
}

const Notifications = new Notifications_()

Meteor.startup(() => {
  if (Meteor.isServer) {
    Meteor.publish('notifications', function() {
      return NotificationsCollection.find({to_ids: Meteor.userId()});
    });
  }
})

export default Notifications
