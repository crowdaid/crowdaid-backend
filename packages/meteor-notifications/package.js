Package.describe({
  name: 'meteorize:notifications',
  summary: 'Notifications library for meteor',
  version: '1.0.0',
  documentation: 'README.md',
  git: 'https://github.com/Mohamed-Hamed/meteorize:notifications'
})

Package.onUse(api => {
  api.versionsFrom('1.6.1')

  api.use([
    'ecmascript',
    'tracker',
    'mongo',
    'mdg:validated-method@1.1.0',
    'check'
  ], ['client', 'server'])

  api.use('service-configuration', { weak: true })
  api.use('http', 'server')

  api.mainModule('index.js', ['client', 'server'])
})
