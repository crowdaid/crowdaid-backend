import { Meteor } from 'meteor/meteor';
import Files from '/imports/api/files/files';
import Organizations from '../../organizations/organizations';
import Beneficiaries from '../../beneficiaries/beneficiaries';

Meteor.publish('files.legal.org', function ({
  filter = {}, limit = 10, skip = 0, sort = { _id: -1 }, orgId,
}) {
  let count = 0;
  let initializing = true;

  const countHandle = Files.find({ ...filter, 'meta.orgId': orgId })
    .observeChanges({
      added: (id) => {
        count += 1;
        if (!initializing) {
          this.changed('CollectionsMeta', 'Files', { count });
        }
      },
      removed: (id) => {
        count -= 1;
        this.changed('CollectionsMeta', 'Files', { count });
      },
    });
    // Instead, we'll send one `added` message right after `observeChanges` has
    // returned, and mark the subscription as ready.
  this.added('CollectionsMeta', 'Files', { count });
  const docsHandle = Files.find({ ...filter, 'meta.orgId': orgId },
    {
      skip, limit, sort, fields: { services: 0, mergedServices: 0, onBoarding: 0 },
    }).observeChanges({
    added: (id, entry) => {
      this.added('files', id, entry);
    },
    changed: (id, entry) => {
      this.changed('files', id, entry);
    },
    removed: (id, entry) => {
      this.removed('files', id, entry);
    },
  });

  initializing = false;
  this.ready();

  // Stop observing the cursor when the client unsubscribes. Stopping a
  // subscription automatically takes care of sending the client any `removed`
  // messages.
  this.onStop(() => {
    countHandle.stop();
    docsHandle.stop();
  });
});

Meteor.publish('files.cases.attachments', function ({
  filter = {}, limit = 10, skip = 0, sort = { _id: -1 }, caseId,
}) {
  let count = 0;
  let initializing = true;

  const countHandle = Files.find({ ...filter, 'meta.caseId': caseId })
    .observeChanges({
      added: (id) => {
        count += 1;
        if (!initializing) {
          this.changed('CollectionsMeta', 'Files', { count });
        }
      },
      removed: (id) => {
        count -= 1;
        this.changed('CollectionsMeta', 'Files', { count });
      },
    });
    // Instead, we'll send one `added` message right after `observeChanges` has
    // returned, and mark the subscription as ready.
  this.added('CollectionsMeta', 'Files', { count });
  const docsHandle = Files.find({ ...filter, 'meta.caseId': caseId },
    {
      skip, limit, sort, fields: { services: 0, mergedServices: 0, onBoarding: 0 },
    }).observeChanges({
    added: (id, entry) => {
      this.added('files', id, entry);
    },
    changed: (id, entry) => {
      this.changed('files', id, entry);
    },
    removed: (id, entry) => {
      this.removed('files', id, entry);
    },
  });

  initializing = false;
  this.ready();

  // Stop observing the cursor when the client unsubscribes. Stopping a
  // subscription automatically takes care of sending the client any `removed`
  // messages.
  this.onStop(() => {
    countHandle.stop();
    docsHandle.stop();
  });
});

Meteor.publish('files.needs.attachments', function ({
  filter = {}, limit = 10, skip = 0, sort = { _id: -1 }, needId,
}) {
  let count = 0;
  let initializing = true;

  const countHandle = Files.find({ ...filter, 'meta.needId': needId })
    .observeChanges({
      added: (id) => {
        count += 1;
        if (!initializing) {
          this.changed('CollectionsMeta', 'Files', { count });
        }
      },
      removed: (id) => {
        count -= 1;
        this.changed('CollectionsMeta', 'Files', { count });
      },
    });
    // Instead, we'll send one `added` message right after `observeChanges` has
    // returned, and mark the subscription as ready.
  this.added('CollectionsMeta', 'Files', { count });
  const docsHandle = Files.find({ ...filter, 'meta.needId': needId },
    {
      skip, limit, sort, fields: { services: 0, mergedServices: 0, onBoarding: 0 },
    }).observeChanges({
    added: (id, entry) => {
      this.added('files', id, entry);
    },
    changed: (id, entry) => {
      this.changed('files', id, entry);
    },
    removed: (id, entry) => {
      this.removed('files', id, entry);
    },
  });

  initializing = false;
  this.ready();

  // Stop observing the cursor when the client unsubscribes. Stopping a
  // subscription automatically takes care of sending the client any `removed`
  // messages.
  this.onStop(() => {
    countHandle.stop();
    docsHandle.stop();
  });
});

Meteor.publish('files.avatar', function userAvatarPublisher(userId) {
  if (!this.userId) return this.ready();
  const query = userId ? { _id: userId } : { _id: this.userId };
  let prevId;
  const targetUserHandle = Meteor.users.find(query, { fields: { 'profile.avatar': 1 } }).observeChanges({
    added: (id, entry) => {
      // first run
      const fileColec = Files.findOne({ _id: entry.profile && entry.profile.avatar, 'meta.type': 'avatar' });
      if (fileColec) {
        const avatar = fileColec.get();
        this.added('files', avatar._id, avatar);
        prevId = avatar._id;
      }
    },
    changed: (id, entry) => {
      // user avatar changed
      const avatar = Files.findOne({ _id: entry.profile && entry.profile.avatar, 'meta.type': 'avatar' }).get();
      if (prevId) this.removed('files', prevId);
      this.added('files', avatar._id, avatar);
      prevId = avatar._id;
    },
  });

  this.onStop(() => {
    targetUserHandle.stop();
  });
});

Meteor.publish('files.organizations.logo', function orgLogoPublisher(orgId) {
  if (!this.userId) return this.ready();
  const { organizationId } = Meteor.user();
  const query = orgId ? { _id: orgId } : { _id: organizationId };
  let prevId;
  const targetOrgHandle = Organizations.find(query, { fields: { logo: 1 } }).observeChanges({
    added: (id, entry) => {
      // first run
      const fileColec = Files.findOne({ _id: entry.logo, 'meta.type': 'logo' });
      if (fileColec) {
        const logo = fileColec.get();
        this.added('files', logo._id, logo);
        prevId = logo._id;
      }
    },
    changed: (id, entry) => {
      // user avatar changed
      const logo = Files.findOne({ _id: entry.logo, 'meta.type': 'logo' }).get();
      if (prevId) this.removed('files', prevId);
      this.added('files', logo._id, logo);
      prevId = logo._id;
    },
  });
  this.ready();
  this.onStop(() => {
    targetOrgHandle.stop();
  });
});

Meteor.publish('files.beneficiaries.image', function beneficiariesImagePublisher(benId) {
  if (!this.userId) return this.ready();
  let prevId;
  const targetBenHandle = Beneficiaries.find({ _id: benId },
    { fields: { photo: 1 } }).observeChanges({
    added: (id, entry) => {
      // first run
      const fileColec = Files.findOne({ _id: entry.photo, 'meta.type': 'photo' });
      if (fileColec) {
        const photo = fileColec.get();
        this.added('files', photo._id, photo);
        prevId = photo._id;
      }
    },
    changed: (id, entry) => {
      // user avatar changed
      const photo = Files.findOne({ _id: entry.photo, 'meta.type': 'photo' }).get();
      if (prevId) this.removed('files', prevId);
      this.added('files', photo._id, photo);
      prevId = photo._id;
    },
  });
  this.ready();
  this.onStop(() => {
    targetBenHandle.stop();
  });
});
