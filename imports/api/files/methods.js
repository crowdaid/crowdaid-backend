import SimpleSchema from 'simpl-schema';
import { ValidatedMethod } from 'meteor/mdg:validated-method';
import Files from '/imports/api/files/files';
import { join as pathJoin } from 'path';
import {
  Authorize, userCheck, isDonor, mkDirInStorage,
} from '/imports/api/utils';
import { Meteor } from 'meteor/meteor';

// eslint-disable-next-line import/prefer-default-export
export const removeFile = new ValidatedMethod({
  name: 'files.remove',
  validate: new SimpleSchema({
    _id: {
      type: String,
      regEx: SimpleSchema.RegEx.Id,
    },
  }).validator(),
  run({ _id }) {
    const file = Files.findOne({ _id });

    if (!file) throw new Meteor.Error(404, 'File not found');

    if (file.meta.orgId) Authorize(['admin', 'owner']);
    else if (file.meta.caseId || file.meta.needId) Authorize(['admin', 'owner', 'employee']);
    else Authorize(['admin']);

    Files.remove({ _id });
  },
});


export const updateDonorAvatar = new ValidatedMethod({
  name: 'users.methods.update_donor_avatar',
  validate: new SimpleSchema({
    image: {
      type: Object,
    },
    'image.fileName': {
      type: String,
    },
    'image.type': {
      type: String,
    },
    data: {
      type: String,
    },
  }).validator({ clean: true }),
  run({ image, data }) {
    //  validate user
    userCheck(Meteor.userId(), 'users.methods.update_donor_avatar');
    if (!isDonor(Meteor.userId())) {
      throw new Meteor.Error(400, 'You are not allowed to do this action ');
    }
    // make async upload
    const writeFile = Meteor.wrapAsync((buffer, options, callback) => {
      Files.write(buffer, options, callback, true);
    });
    // create folder for user if not exists
    const storageDir = pathJoin('users', Meteor.userId(), 'avatar');
    mkDirInStorage(storageDir);
    // encode image data to base64
    const buffer = Buffer.from(data, 'base64');
    writeFile(buffer, {
      fileName: image.fileName,
      type: image.type,
      userId: Meteor.userId(),
      meta: {
        namespace: 'users',
        slug: Meteor.userId(),
        type: 'avatar',
      },
    }, (err, fileRef) => {
      if (!err) {
        Meteor.users.update(Meteor.userId(), {
          $set: {
            'profile.avatar': fileRef._id,
          },
        });
      } else {
        throw new Meteor.Error(400, err);
      }
    });
  },
});


export const getDonorAvaters = new ValidatedMethod({
  name: 'files.methods.get_donor_avatar',
  validate: new SimpleSchema({
    userId: {
      type: String,
      regEx: SimpleSchema.RegEx.Id,
      optional: true,
    },
  }).validator({ clean: true }),
  run({
    userId,
  }) {
    if (!isDonor(userId || Meteor.userId())) {
      throw new Meteor.Error(500, 'Not allow to do this action ');
    }
    const query = userId ? { _id: userId } : { _id: Meteor.userId() };
    const { profile: { avatar }, profile } = Meteor.users.findOne(query,
      { fields: { 'profile.avatar': 1 } });

    const file = Files.findOne({ _id: profile && avatar, 'meta.type': 'avatar' });
    return file && file.link();
  },
});
