// Meteor core imports
import { Meteor } from 'meteor/meteor';

// Meteor packages imports
import { FilesCollection } from 'meteor/ostrio:files';

// node imports
import SimpleSchema from 'simpl-schema';

// internal imports
import commonData from '/imports/api/sharedSchemas';
import * as path from 'path';

SimpleSchema.extendOptions(['autoform']);

const Files = new FilesCollection({
  collectionName: 'files',
  allowClientCode: true,
  // Disallow remove files from Client
  storagePath(file) {
    if (Object.keys(file).length === 0 || !file.meta) {
      return Meteor.settings.storage.path;
    }

    // if file is uploaded for a case
    if (file.meta.caseId) {
      return path.join(Meteor.settings.storage.path,
        file.meta.namespace, file.meta.caseId, file.meta.type);
    }

    // if file is uploaded for a need
    if (file.meta.needId) {
      return path.join(Meteor.settings.storage.path,
        file.meta.namespace, file.meta.needId, file.meta.type);
    }

    return path.join(Meteor.settings.storage.path,
      file.meta.namespace, file.meta.slug, file.meta.type);
  },
  onBeforeUpload(file) {
    // Allow upload files under 10MB, and only in png/jpg/jpeg formats
    if (file.size <= 1024 * 1024 * 10 && /png|jpg|jpeg|pdf|doc|docx/i.test(file.extension)) {
      return true;
    }
    return 'Please upload image, pdf, or word document, with size equal or less than 10MB';
  },
});

Files.schema = new SimpleSchema(Files.schema);
Files.schema.extend(commonData);

Files.collection.attachSchema(Files.schema);

// Deny all client-side updates since we will be using methods to manage this collection
Files.denyClient();

export default Files;
