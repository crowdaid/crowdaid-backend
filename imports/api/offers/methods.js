import SimpleSchema from 'simpl-schema';
import { ValidatedMethod } from 'meteor/mdg:validated-method';

import Needs from '/imports/api/needs/needs';
import { userCheck } from '/imports/api/utils';
import { needStatuses } from '/imports/api/commonValues';

export const createNeed = new ValidatedMethod({
  name: 'needs.methods.create',
  validate: Needs.schema.pick(
    'caseId',
    'description',
    'dueDate',
    'userId',
  ).validator({ clean: true }),
  run({
    caseId,
    description,
    dueDate,
    userId,
  }) {
    userCheck(userId, 'needs.methods.create');

    // we should check for permessions later
    Needs.insert({
      caseId,
      description,
      dueDate,
      userId,
    });
  },
});

// export const updateNeed = new ValidatedMethod({
//     name: 'needs.methods.update',
//     validate: Needs.schema.pick('caseId', 'text', 'quantity', 'frequent')
//      .validator({clean: true}),
//     run({ caseId, userId, text, quantity, frequent }){
//         console.log('inside method', userId, caseId, userId, text, quantity);
//
//         userCheck(userId, 'needs.methods.update');
//
//         // we should check for permessions later
//
//         Needs.update({caseId, userId, text, quantity, frequent});
//     }
// });


// this won't actually remove it, it will only flip the isRemoved flag
export const removeNeed = new ValidatedMethod({
  name: 'needs.methods.remove',
  validate: new SimpleSchema({
    needId: {
      type: SimpleSchema.RegEx.Id,
    },
    userId: {
      type: SimpleSchema.RegEx.Id,
    },
  }).validator({ clean: true }),
  run({ needId, userId }) {
    userCheck(userId, 'needs.methods.remove');

    // we should check user ownership/admin to be able to let him do this

    // we should check for permessions later

    // we should check if the attribute already isActive: false

    Needs.update({ _id: needId }, { $set: { isRemoved: true, removedAt: new Date() } });
  },
});

export const setNeedStatus = new ValidatedMethod({
  name: 'needs.methods.setStatus',
  validate: new SimpleSchema({
    status: {
      type: String,
      allowedValues: needStatuses,
    },
    userId: { type: SimpleSchema.RegEx.Id },
    needId: { type: SimpleSchema.RegEx.Id },
  }).validator({ clean: true }),
  run({ needId, userId, status }) {
    userCheck(userId, 'needs.methods.setStatus');
    Needs.update(needId, { $set: { status } });
  },
});
