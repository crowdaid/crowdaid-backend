import { Meteor } from 'meteor/meteor';

import Offers from '/imports/api/offers/offers';

Meteor.publish('offers.all', function () {
  if (!this.userId) return this.ready();
  return Offers.find({});
});
