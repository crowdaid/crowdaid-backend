// Meteor core imports
import { Mongo } from 'meteor/mongo';
import { Tracker } from 'meteor/tracker';

// node imports
import SimpleSchema from 'simpl-schema';

// internal imports
import commonData from '/imports/api/sharedSchemas';
import { offerStatuses } from '/imports/api/commonValues';


const Offers = new Mongo.Collection('offers');

SimpleSchema.extendOptions(['autoform']);
Offers.schema = new SimpleSchema({
  offeree: {
    type: Object,
  },
  'offeree.type': {
    type: String,
    allowedValues: ['need', 'public'], // need type, for offers related to specific need,
    // the public one, related to offering something to the public
  },
  'offeree.id': {
    type: Object,
    optional: true,
  },
  'offeree.id.case': {
    type: SimpleSchema.RegEx.Id,
  },
  'offeree.id.need': {
    type: SimpleSchema.RegEx.Id,
  },
  // Note: this will store the data collected when you offer to help with the need.
  // TODO: refactor to a more structured or modular object.
  details: {
    type: Object,
    blackbox: true,
  },
  notes: {
    type: String,
    optional: true,
  },
  isQuantifiable: {
    type: Boolean,
    defaultValue: true,
  },
  // TODO: This need to be 1. optional conditionally based on the isQuantifiable value 2. need more structuring.
  unitOfMeasure: {
    type: String,
    defaultValue: 'Pound',
  },
  quantityOffered: {
    type: Number,
    defaultValue: 1,
  },
  // TODO: This need to be 1. optional conditionally based on the isQuantifiable value.
  quantityCollected: {
    type: Number,
    defaultValue: 0,
  },
  status: {
    type: String,
    allowedValues: offerStatuses,
    defaultValue: 'offered',
  },
  dueDate: {
    type: Date,
    optional: true,
  },
  remindAt: {
    type: Date,
    optional: true,
  },
  attachments: {
    type: Array,
    optional: true,
  },
  'attachments.$': {
    type: Object,
    blackbox: true,
  },
}, { trimStrings: true, tracker: Tracker });

Offers.schema.extend(commonData);

Offers.attachSchema(Offers.schema);


// Deny all client-side updates since we will be using methods to manage this collection
Offers.deny({
  insert() {
    return true;
  },
  update() {
    return true;
  },
  remove() {
    return true;
  },
});

export default Offers;
