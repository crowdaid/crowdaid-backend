import { Meteor } from 'meteor/meteor';
import families from '../families';
import { isOwner, isStuff } from '/imports/api/utils';

Meteor.publish('familiesTabular', function ({
  filter = {}, limit = 10, skip = 0, sort = { _id: -1 }, beneficiaryId,
}) {
  if (!isStuff(this.userId) && !isOwner(this.userId)) return this.ready();
  let count = 0;
  let initializing = true;

  const countHandle = families.find({ ...filter, beneficiaryId, isRemoved: { $ne: true } })
    .observeChanges({
      added: (id) => {
        count += 1;
        if (!initializing) {
          this.changed('CollectionsMeta', 'Families', { count });
        }
      },
      removed: (id) => {
        count -= 1;
        this.changed('CollectionsMeta', 'Families', { count });
      },
    });
  // Instead, we'll send one `added` message right after `observeChanges` has
  // returned, and mark the subscription as ready.
  this.added('CollectionsMeta', 'Families', { count });
  const docsHandle = families.find({ ...filter, beneficiaryId, isRemoved: { $ne: true } },
    {
      skip, limit, sort, fields: { services: 0, mergedServices: 0, onBoarding: 0 },
    }).observeChanges({
    added: (id, entry) => {
      this.added('families', id, entry);
    },
    changed: (id, entry) => {
      this.changed('families', id, entry);
    },
    removed: (id, entry) => {
      this.removed('families', id, entry);
    },
  });

  initializing = false;
  this.ready();

  // Stop observing the cursor when the client unsubscribes. Stopping a
  // subscription automatically takes care of sending the client any `removed`
  // messages.
  this.onStop(() => {
    countHandle.stop();
    docsHandle.stop();
  });
});

Meteor.publish('families.single', function (_id) {
  if (!isStuff(this.userId) && !isOwner(this.userId)) return this.ready();
  return families.find(_id, { isRemoved: { $ne: true } });
});
