import { Mongo } from 'meteor/mongo';
import { Tracker } from 'meteor/tracker';
import SimpleSchema from 'simpl-schema';
import commonData from '/imports/api/sharedSchemas';
import { familyRelations } from '/imports/api/commonValues';

const Families = new Mongo.Collection('families');

SimpleSchema.extendOptions(['autoform']);


Families.schema = new SimpleSchema({
  _id: {
    type: String,
    optional: true,
    regEx: SimpleSchema.RegEx.Id,
  },
  name: {
    type: String,
  },
  age: {
    type: String,
    optional: true,
  },
  birthDate: {
    type: Date,
  },
  gender: {
    type: String,
    allowedValues: ['Male', 'Female'],
  },
  relationType: {
    type: String,
    allowedValues: familyRelations,
  },
  profession: {
    type: String,
    optional: true,
  },
  socialStatus: {
    type: String,
  },
  medicalConditions: {
    type: String,
    optional: true,
  },
  notes: {
    type: String,
    optional: true,
  },
  nationalIdNumber: {
    type: String,
  },
  organizationId: {
    type: String,
    optional: true,
    regEx: SimpleSchema.RegEx.Id,
  },
  beneficiaryId: {
    type: String,
    regEx: SimpleSchema.RegEx.Id,
  },
}, { trimStrings: true, tracker: Tracker });

Families.schema.extend(commonData);

Families.attachSchema(Families.schema);

// Deny all client-side updates since we will be using methods to manage this collection
Families.deny({
  insert() {
    return true;
  },
  update() {
    return true;
  },
  remove() {
    return true;
  },
});

export default Families;
