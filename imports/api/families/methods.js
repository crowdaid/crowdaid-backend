import { ValidatedMethod } from 'meteor/mdg:validated-method';

import Families from './families';
import Organizations from '../organizations/organizations';
import { Authorize } from '/imports/api/utils';


export const createFamilyMember = new ValidatedMethod({
  name: 'families.methods.create',
  validate: Families.schema.pick('name', 'age',
    'gender', 'birthDate', 'relationType', 'profession',
    'socialStatus', 'medicalConditions', 'notes', 'nationalIdNumber',
    'beneficiaryId').validator({ clean: true }),
  run(familyMember) {
    Authorize(['owner', 'employee']);
    const { organizationId } = Meteor.user();
    if (!organizationId) {
      throw new Meteor.Error(403, 'User has no organizationId');
    }
    // find if the national number is already exists
    const duplicateNationalNum = Families
      .find({
        nationalIdNumber: familyMember.nationalIdNumber,
        isRemoved: { $ne: true },
        beneficiaryId: familyMember.beneficiaryId,
      }).count();
    if (duplicateNationalNum) {
      throw new Meteor.Error(403, 'National ID number already exist in this family');
    }
    Families.insert({ ...familyMember, organizationId });
    return familyMember.beneficiaryId;
  },
});

export const updateMember = new ValidatedMethod({
  name: 'families.methods.update',
  validate: Families.schema.pick('name', 'age', '_id',
    'gender', 'birthDate', 'relationType', 'profession',
    'socialStatus', 'medicalConditions', 'notes', 'nationalIdNumber')
    .validator({ clean: true }),
  run(familyMember) {
    Authorize(['owner', 'employee']);
    // find if the national number is already exists
    const duplicateNationalNum = Families
      .find({
        _id: { $ne: familyMember._id },
        nationalIdNumber: familyMember.nationalIdNumber,
        isRemoved: { $ne: true },
        beneficiaryId: familyMember.beneficiaryId,
      }).count();
    if (duplicateNationalNum) {
      throw new Meteor.Error(403, 'National ID number already exist in this family');
    }
    Families.update(familyMember._id, {
      $set: familyMember,
    });
    return Families.findOne({ _id: familyMember._id }).beneficiaryId;
  },
});

export const removeMember = new ValidatedMethod({
  name: 'families.methods.remove',
  validate: Families.schema.pick('_id').validator({ clean: true }),
  run({ _id }) {
    Authorize(['owner', 'employee']);
    Families.update({
      _id,
    },
    {
      $setOnInsert: { removedAt: new Date() },
      $set: {
        isRemoved: true,
      },
    });
  },
});

export const checkIDNumber = new ValidatedMethod({
  name: 'families.methods.check_ID_number',
  validate: Families.schema.pick('_id', 'nationalIdNumber').validator({ clean: true }),
  run({ _id, nationalIdNumber }) {
    Authorize(['owner', 'employee']);
    let member;
    if (_id) {
      member = Families.findOne({ _id: { $ne: _id }, nationalIdNumber, isRemoved: { $ne: true } });
    } else {
      member = Families.findOne({ nationalIdNumber, isRemoved: { $ne: true } });
    }
    if (member) {
      // exist as a beneficiary in same organization
      if ((
        member.organizationId === Meteor.user().organizationId
       && (member.relationType === 'self')
      )) {
        return {
          type: 'warning',
          name: member.name,
          typeExistents: 'the same',
          as: 'beneficiary',
        };
      }
      // exist as a beneficiary in other organization
      if ((
        member.organizationId !== Meteor.user().organizationId
       && (member.relationType === 'self')
      )) {
        const org = Organizations.findOne({ _id: member.organizationId }, { fields: { name: 1 } });
        return {
          type: 'warning',
          name: member.name,
          typeExistents: 'other',
          orgName: org.name.ar,
          as: 'beneficiary',
        };
      }
      // exist as a member in other family in the same organization
      if ((
        member.organizationId === Meteor.user().organizationId
       && (member.relationType !== 'self')
      )) {
        return {
          type: 'warning',
          name: member.name,
          typeExistents: 'the same',
          as: 'member',
        };
      }
    }
    return false;
  },
});
