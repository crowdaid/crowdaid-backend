import { Meteor } from 'meteor/meteor';
import Donations from '/imports/api/donations/donations';
import Cases from '/imports/api/cases/cases';
import Needs from '/imports/api/needs/needs';
import { Authorize, isAdmin } from '/imports/api/utils';

// donations table

Meteor.publish('DonationsTabular', function ({
  filter = {}, limit = 10, skip = 0, sort = { createdAt: -1 }, orgId, setOrgId = true,
}) {
  if (!Authorize(['owner', 'superadmin', 'admin', 'employee'])) return this.ready();
  let count = 0;
  let initializing = true;

  const query = { ...filter, isRemoved: { $ne: true } };

  const user = Meteor.users.findOne({ _id: this.userId }, { fields: { organizationId: 1 } });

  if (!isAdmin(this.userId) && setOrgId) {
    if (user.organizationId) query.orgId = user.organizationId;
  } else if (orgId) {
    query.orgId = orgId;
  }

  const countHandle = Donations.find(query)
    .observeChanges({
      added: (id) => {
        count += 1;
        if (!initializing) {
          this.changed('CollectionsMeta', 'Donations', { count });
        }
      },
      removed: (id) => {
        count -= 1;
        this.changed('CollectionsMeta', 'Donations', { count });
      },
    });
  // Instead, we'll send one `added` message right after `observeChanges` has
  // returned, and mark the subscription as ready.
  this.added('CollectionsMeta', 'Donations', { count });
  const docsHandle = Donations.find(query,
    {
      skip, limit, sort, fields: { services: 0, mergedServices: 0, onBoarding: 0 },
    }).observeChanges({
    added: (id, entry) => {
      this.added('donations', id, entry);
    },
    changed: (id, entry) => {
      this.changed('donations', id, entry);
    },
    removed: (id, entry) => {
      this.removed('donations', id, entry);
    },
  });

  initializing = false;
  this.ready();

  // Stop observing the cursor when the client unsubscribes. Stopping a
  // subscription automatically takes care of sending the client any `removed`
  // messages.
  this.onStop(() => {
    countHandle.stop();
    docsHandle.stop();
  });
});


Meteor.publish('donations.case', function (caseId) {
  if (!this.userId) return this.ready();
  if (!Authorize(['owner', 'superadmin', 'admin', 'employee'])) return this.ready();
  return Donations.find({ caseId });
});

Meteor.publish('donations.need', function (needId) {
  if (!this.userId) return this.ready();
  if (!Authorize(['owner', 'superadmin', 'admin', 'employee'])) return this.ready();
  return Donations.find({ needId });
});

Meteor.publish('donations.user', function (userId) {
  if (!this.userId || this.userId !== userId) return this.ready();
  return Donations.find({ userId: userId || this.userId });
});

Meteor.publish('donations.single', function (donId) {
  if (!this.userId) return this.ready();
  const donation = Donations.findOne({ _id: donId });
  return [
    Donations.find({ _id: donId }),
    // publish the donation case
    Cases.find({ _id: donation.caseId }),
    // publish the donation need
    Needs.find({ _id: donation.needId }),
    // publish the donation user
    Meteor.users.find({ _id: donation.userId }),
  ];
});
