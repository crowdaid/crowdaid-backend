// Meteor core imports
import { Mongo } from 'meteor/mongo';
import { Tracker } from 'meteor/tracker';

// node imports
import SimpleSchema from 'simpl-schema';

// internal imports
import commonData from '/imports/api/sharedSchemas';
import { donationMethods, donationStatuses } from '/imports/api/commonValues';

const Donations = new Mongo.Collection('donations');

SimpleSchema.extendOptions(['autoform']);

Donations.schema = new SimpleSchema({
  _id: {
    type: String,
    regEx: SimpleSchema.RegEx.Id,
  },
  // caseId and needId are optional because they are not required for auto donations
  caseId: {
    type: SimpleSchema.RegEx.Id,
    optional: true,
  },
  needId: {
    type: SimpleSchema.RegEx.Id,
    optional: true,
  },
  orgId: {
    type: SimpleSchema.RegEx.Id,
    optional: true,
    custom: function validateOrgId() {
      // orgId not required only if it's status is offer or unallocated
      if (!this.value && !['Offer', 'Unallocated'].includes(this.field('status').value)) {
        return 'orgId is required';
      }
    },
  },
  // Quantity represents the donated amount in EGP for money type donations
  // And the donated items for items type donations whether (user donated item money value or items)
  quantity: {
    type: Number,
    defaultValue: 0,
  },
  // Used for needs with unitOfMeasure as Item and donation with type 'money'
  // As reference to the donated amount of money
  moneyValue: {
    type: Number,
    defaultValue: 0,
  },
  type: {
    type: String,
    allowedValues: ['money', 'items', 'service'],
  },
  notes: {
    type: String,
    optional: true,
  },
  donationMethod: {
    type: String,
    allowedValues: donationMethods,
  },
  status: {
    type: String,
    allowedValues: donationStatuses,
    defaultValue: 'Pending',
  },
  // get it from the donor profile if exist
  collectionAddress: {
    type: String,
    optional: true,
  },
  // get it from the donor profile if exist
  phone: {
    type: String,
    optional: true,
  },
  collectionTime: {
    type: Object,
    optional: true,
  },
  'collectionTime.date': {
    type: Date,
  },
  'collectionTime.timeRange': {
    type: Array,
    optional: true,
  },
  'collectionTime.timeRange.$': {
    type: String,
  },
  // Did the donor sponsor the whole case?
  wholeCase: {
    type: Boolean,
    defaultValue: false,
  },
  // Did the donor sponsor the whole need?
  wholeNeed: {
    type: Boolean,
    defaultValue: false,
  },
  isAutomated: {
    type: Boolean,
    defaultValue: false,
  },
  isAnonymous: {
    type: Boolean,
    defaultValue: false,
  },
  autoDonationId: {
    type: SimpleSchema.RegEx.Id,
    optional: true,
  },
  collectedTime: {
    type: Date,
    optional: true,
  },
  itemId: {
    type: String,
    optional: true,
    regEx: SimpleSchema.RegEx.Id,
  },
  itemDescription: {
    type: String,
    optional: true,
  },
  attachments: {
    type: Array,
    optional: true,
  },
  'attachments.$': {
    type: String,
    regEx: SimpleSchema.RegEx.Id,
  },
  fromOrganization: {
    type: Boolean,
    defaultValue: false,
  },
}, { trimStrings: true, tracker: Tracker });

Donations.schema.extend(commonData);

Donations.attachSchema(Donations.schema);

// Deny all client-side updates since we will be using methods to manage this collection
Donations.deny({
  insert() {
    return true;
  },
  update() {
    return true;
  },
  remove() {
    return true;
  },
});

export default Donations;
