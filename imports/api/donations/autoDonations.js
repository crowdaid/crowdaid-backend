// Meteor core imports
import { Mongo } from 'meteor/mongo';
import { Tracker } from 'meteor/tracker';

// node imports
import SimpleSchema from 'simpl-schema';

// internal imports
import commonData from '/imports/api/sharedSchemas';
import { donationMethods, autoDonationFrequencies } from '/imports/api/commonValues';

const AutoDonations = new Mongo.Collection('autoDonations');

SimpleSchema.extendOptions(['autoform']);

AutoDonations.schema = new SimpleSchema({
  _id: {
    type: String,
    regEx: SimpleSchema.RegEx.Id,
  },
  quantity: {
    type: Number,
  },
  notes: {
    type: String,
    optional: true,
  },
  donationMethod: {
    type: String,
    allowedValues: donationMethods,
  },
  type: {
    type: String,
    allowedValues: ['money', 'items', 'service'],
  },
  collectionAddress: {
    type: String,
    optional: true,
  },
  collectionTime: {
    type: Object,
    optional: true,
  },
  'collectionTime.date': {
    type: Date,
  },
  'collectionTime.timeRange': {
    type: Array,
  },
  'collectionTime.timeRange.$': {
    type: String,
  },
  isActive: {
    type: Boolean,
    defaultValue: true,
  },
  frequency: {
    type: String,
    allowedValues: autoDonationFrequencies,
    defaultValue: 'Once',
  },
  isSelective: {
    type: Boolean,
    defaultValue: false,
  },
  orgs: {
    type: Array,
    optional: true,
  },
  'orgs.$': {
    type: Object,
  },
  'orgs.$._id': {
    type: SimpleSchema.RegEx.Id,
  },
  'orgs.$.quantity': {
    type: Number,
  },
  restMoney: { // just to skip the validation
    type: Number,
    optional: true,
  },
}, { trimStrings: true, tracker: Tracker });

AutoDonations.schema.extend(commonData);

AutoDonations.attachSchema(AutoDonations.schema);

// Deny all client-side updates since we will be using methods to manage this collection
AutoDonations.deny({
  insert() {
    return true;
  },
  update() {
    return true;
  },
  remove() {
    return true;
  },
});

export default AutoDonations;
