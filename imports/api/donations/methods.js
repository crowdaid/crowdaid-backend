/* eslint-disable no-param-reassign */
import SimpleSchema from 'simpl-schema';
import { ValidatedMethod } from 'meteor/mdg:validated-method';

import Donations from '/imports/api/donations/donations';
import AutoDonations from '/imports/api/donations/autoDonations';
import Needs from '/imports/api/needs/needs';
import { userCheck, Authorize } from '/imports/api/utils';
import Cases from '../cases/cases';
import Organizations from '../organizations/organizations';

// ? This file contain a very complicated calculations
// ? You must have a very solid understanding for the business
// ? and the comment will help (I hope)


const validatePayment = (donation) => {
  const user = Meteor.users.findOne({ _id: donation.userId });
  const org = Organizations.findOne({ _id: donation.orgId });
  const orgAddress = org && org.HQLocation.address;

  const paymentInfo = {};
  // if donation method is cash
  if (donation.donationMethod === 'Cash on door') {
    // assign the address to the user profile address if any
    if (donation.collectionAddress) {
      paymentInfo.collectionAddress = donation.collectionAddress;
    } else if (user.profile && user.profile.address) {
      paymentInfo.collectionAddress = user.profile.address;
    } else if (donation.fromOrganization) {
      paymentInfo.collectionAddress = orgAddress;
    } else {
      throw new Meteor.Error(400,
        'You haven\'t specify any address to collect the money from');
    }

    // assign the phone to the user profile phone if any
    if (donation.phone) {
      paymentInfo.phone = donation.phone;
    } else if (user.profile && user.profile.phone) {
      paymentInfo.phone = user.profile.phone;
    } else if (donation.fromOrganization) {
      // TODO: add phone in organization
      paymentInfo.phone = 'ORG Phone';
    } else {
      throw new Meteor.Error(400,
        'You haven\'t specify any mobile number for the donation');
    }

    if (!donation.collectionTime.date
      && (!donation.fromOrganization && donation.collectionTime.timeRange.length === 0)) {
      throw new Meteor.Error(400, 'collectionTime is required in cash payment method');
    }
  }
  return paymentInfo;
};


/**
 * change the need quantityFulfilled and change it's status
 */
export const changeNeedState = new ValidatedMethod({
  name: 'donations.methods.change_need_state',
  validate: new SimpleSchema({
    donationDoc: {
      type: Object,
      blackbox: true,
    },
  }).validator(),
  run({ donationDoc }) {
    Authorize(['owner', 'employee', 'donor']);

    if (donationDoc.status !== 'Pending') {
      throw new Meteor.Error('Invalid status', 'The donation status must be pending');
    }

    const Need = Needs.findOne(donationDoc.needId);

    if (Need && Need.isQuantifiable) {
      let totalFulfilled = 0;
      // the need is item and the donation is money
      if (donationDoc.type === 'money' && Need.unitOfMeasure === 'Item') {
        // if no money left
        const moneyLeft = donationDoc.quantity % Need.itemPrice;
        if (moneyLeft === 0) {
          totalFulfilled = Number(donationDoc.quantity / Need.itemPrice) + Number(Need.quantityFulfilled);
        } else {
          // take the amount needed
          totalFulfilled = Number((donationDoc.quantity - moneyLeft) / Need.itemPrice) + Number(Need.quantityFulfilled);
          // TODO: donate the rest with different status 'moneyLeft'
        }
        // const quantityNeeded = (Need.quantityNeeded - Need.quantityFulfilled)
        //  Need.itemPrice;
        // status = totalFulfilled >= quantityNeeded ? 'fulfilled' : 'touched';
      } else {
        // the need is money and the donation is money
        // need is item and the donation is item
        // the need is match the donation
        totalFulfilled = Number(donationDoc.quantity) + Number(Need.quantityFulfilled);
      }

      const status = totalFulfilled >= Need.quantityNeeded ? 'fulfilled' : 'touched';

      Needs.update(donationDoc.needId, {
        $set: {
          quantityFulfilled: totalFulfilled,
          status,
        },
      });
    } else if (Need) {
      // Unquantifiable need
      Needs.update(donationDoc.needId, {
        $set: {
          quantityFulfilled: 1,
          status: 'fulfilled',
        },
      });
    }

    // check if the case if fulfilled
    const allNeeds = Needs.find({
      caseId: Need.caseId,
      isRemoved: { $ne: true },
    }).count();
    const fulfilledNeed = Needs.find({
      caseId: Need.caseId,
      isRemoved: { $ne: true },
      status: 'fulfilled',
    }).count();
    if (allNeeds - fulfilledNeed === 0) {
      Cases.update({ _id: Need.caseId }, {
        $set: {
          status: 'fulfilled',
        },
      });
    }
  },
});

// donate to a specific need
export const needDonate = new ValidatedMethod({
  name: 'donations.methods.need_donate',
  validate: Donations.schema.pick(
    'caseId', 'needId', 'quantity', 'itemDescription',
    'type', 'notes', 'isAnonymous', 'donationMethod',
    'collectionAddress', 'collectionTime', 'phone', 'fromOrganization',
    'wholeCase', 'wholeNeed', 'orgId', 'userId', 'isAutomated',
  ).validator({ clean: true }),
  run(donation) {
    if (!donation.fromOrganization) {
      donation.userId = Meteor.userId();
      userCheck(donation.userId, 'donations.methods.need_donate');
    }
    // check the role of the user
    Authorize(['admin', 'owner', 'employee', 'donor']);

    const donatedNeed = Needs.findOne({ _id: donation.needId });
    if (!donatedNeed) {
      throw new Meteor.Error(404, 'The need is not found');
    } else if (donatedNeed.status === 'fulfilled') {
      throw new Meteor.Error(400, 'The need is already fulfilled');
    } else if (!donatedNeed.isQuantifiable) {
      throw new Meteor.Error(400, 'The need is not is quantifiable');
    } else { // set the case to the need case
      donation.caseId = donatedNeed.caseId;
      donation.itemId = donatedNeed.itemId;
    }

    if (!donation.quantity) {
      if (donation.type === 'money') {
        throw new Meteor.Error(400, 'You must specify the money amount of the donation');
      } else {
        throw new Meteor.Error(400, 'You must specify the items quantity of the donation');
      }
    }
    // if the donation is items and the need needed mony
    if (donatedNeed.unitOfMeasure === 'EGP' && donation.type === 'items') {
      throw new Meteor.Error(400, "Can't donate items and the need unit of measure is money");
    }

    const quantityNeeded = (donatedNeed.quantityNeeded - donatedNeed.quantityFulfilled)
     * donatedNeed.itemPrice;


    if ((donatedNeed.unitOfMeasure === 'Item'
     && donation.type === 'items')
     && donation.quantity > (donatedNeed.quantityNeeded - donatedNeed.quantityFulfilled)) {
      throw new Meteor.Error(400, 'The number of the donated items is exceeded the number of items the case need');
    }

    // user donated money or items and the need is needed money
    if (donation.quantity > quantityNeeded) {
      throw new Meteor.Error(400, 'The donated amount of money is exceeded the money needed');
    }

    // check it the donation money > itemPrice
    if ((donatedNeed.unitOfMeasure === 'Item'
    && donation.type === 'money')
    && (donation.quantity < donatedNeed.itemPrice)) {
      throw new Meteor.Error(400, 'The donated amount of not enough to get one item needed for this case ');
    }
    // if the user donated items to an need needed items
    if (donation.type === 'items' && !donation.fromOrganization) {
      if (!donation.itemDescription) throw new Meteor.Error(400, 'Please specify a description for this item');
    }

    const donationDoc = { ...donation, ...validatePayment(donation) };

    const insertedDonation = Donations.insert(donationDoc, (err) => {
      if (err) throw new Meteor.Error(503, 'Something went wrong, Please try again later');
    });

    // change the need status and the quantityFulfilled
    changeNeedState.call({ donationDoc });
    return { _id: insertedDonation, ...donationDoc };
  },
});


// Offer items on the organizations
export const itemsOffers = new ValidatedMethod({
  name: 'donations.methods.items_offer',
  validate: Donations.schema.pick(
    'quantity', 'itemDescription', 'type',
    'notes', 'isAnonymous', 'donationMethod', 'status',
    'collectionAddress', 'collectionTime', 'phone',
    'orgId', 'userId', 'isAutomated', 'itemId',
  ).validator({ clean: true }),
  run(donation) {
    donation.userId = Meteor.userId();
    userCheck(donation.userId, 'donations.methods.need_donate');

    // check the role of the user
    Authorize(['owner', 'employee', 'donor']);

    if (donation.donationMethod !== 'Cash on door') {
      throw new Meteor.Error(400, 'In items offering only cash on door allowed');
    }

    if (!donation.itemId) throw new Meteor.Error(400, 'Please select the item you want to donate with');

    if (!donation.itemDescription) throw new Meteor.Error(400, 'Please specify a description for this item');

    const donationDoc = { ...donation, ...validatePayment(donation) };

    const insertedDonation = Donations.insert(donationDoc, (err) => {
      if (err) throw new Meteor.Error(503, 'Something went wrong, Please try again later');
    });


    return { _id: insertedDonation, ...donationDoc };
  },
});


// distribute money on each need
const moneyDistribution = (moneyAmount, donationInfo, organizationId) => {
  const selectiveQuery = {};
  if (organizationId) {
    selectiveQuery.organizationId = organizationId;
  }
  // sort the needs by the large money value and old date
  const needs = Needs.find({
    unitOfMeasure: 'EGP',
    ...selectiveQuery, // in case of selective donation
    quantityNeeded: { $ne: 0 },
    isQuantifiable: true,
    status: { $ne: 'fulfilled' },
    isRemoved: { $ne: true },
  },
  { sort: { createdAt: 1, quantityNeeded: -1 } }).fetch();
  // initialize rate
  let rate = 0.1;

  needs.map((need) => {
    const quantityNeeded = need.quantityNeeded - need.quantityFulfilled;
    // if the the need money not fulfilled yet and amount if money != 0
    if (quantityNeeded > 0 && moneyAmount !== 0) {
      let donationAmount = 0; // the quantity of the donation for each need
      if (moneyAmount - (quantityNeeded * rate) > 0) {
        // keep distrusting if the rest of the amount is > 0
        donationAmount = Math.round(quantityNeeded * rate);
        moneyAmount -= donationAmount;
      } else if (quantityNeeded >= moneyAmount) {
        // if their any money and also a need give all to it
        donationAmount = Math.floor(moneyAmount);
        moneyAmount = 0; // set the amount to 0
      }
      const donationDoc = {
        quantity: donationAmount,
        orgId: need.organizationId,
        type: 'money',
        isAutomated: true,
        donationMethod: donationInfo.donationMethod,
        collectionTime: donationInfo.collectionTime,
        collectionAddress: donationInfo.collectionAddress,
        isAnonymous: donationInfo.isAnonymous,
        phone: donationInfo.phone,
        needId: need._id,
      };
      // TODO: remove this logs
      console.log('----------');
      console.log('Quantity Needed-: ', quantityNeeded, ' Donation Amount-: ',
        donationAmount, ' Rest of The Money-: ', moneyAmount, ' Rate-: ', rate);
      // reset rate after 10 needs
      rate = rate >= 1 ? 0.1 : Math.round((rate + 0.1) * 10) / 10;
      // donate to this need
      if (donationAmount) return needDonate.call(donationDoc);
    }
    return 0;
  });

  // donate to the rest of the donations money with Unallocated status * appear on admin dashboard
  if (moneyAmount) {
    const donationDoc = {
      quantity: moneyAmount,
      orgId: null,
      type: 'money',
      isAutomated: true,
      donationMethod: donationInfo.donationMethod,
      collectionTime: donationInfo.collectionTime,
      collectionAddress: donationInfo.collectionAddress,
      isAnonymous: donationInfo.isAnonymous,
      phone: donationInfo.phone,
      status: 'Unallocated',
    };
    Donations.insert(donationDoc, (err) => {
      if (err) throw new Meteor.Error(503, 'Something went wrong, Please try again later');
    });
  }
};


export const autoGive = new ValidatedMethod({
  name: 'donations.methods.auto_give',
  validate: AutoDonations.schema.pick(
    'quantity', 'notes', 'donationMethod', 'type', 'collectionAddress',
    'collectionTime', 'frequency', 'isSelective', 'orgs', 'restMoney',
  ).extend({
    isAutomated: {
      type: Boolean,
      defaultValue: false,
    },
    isAnonymous: {
      type: Boolean,
      defaultValue: false,
    },
    itemDescription: {
      type: String,
      optional: true,
    },
    itemId: {
      type: String,
      regEx: SimpleSchema.RegEx.Id,
      optional: true,
    },
    phone: {
      type: String,
      optional: true,
    },
  }).validator({ clean: true }),
  run(donation) {
    userCheck(donation.userId || Meteor.userId(), 'donations.methods.auto_give');
    // check the role of the user
    Authorize(['admin', 'owner', 'donor']);
    if (donation.type === 'money') {
      if (!donation.isSelective) {
        moneyDistribution(donation.quantity, donation);
      } else if (donation.isSelective && donation.orgs) {
        // distribute money on each organization needs based on it's quantity
        donation.orgs.map(org => moneyDistribution(org.quantity, donation, org._id));
        // if their rest of money, distribute it on none selective way
        if (donation.restMoney && (donation.restMoney !== donation.quantity)) {
          moneyDistribution(donation.restMoney, donation);
        }
      } else {
        throw new Meteor.Error(400, 'Orgs is required if it\'s selective donation');
      }
    } else if (donation.type === 'items') {
      donation.status = 'Offer';
      if (!donation.isSelective) {
        // the items will appear as offers in all organizations
        donation.orgId = null;
        itemsOffers.call(donation);
      } else if (donation.isSelective && donation.orgs) {
        // the items will appear as offers in these organizations only
        donation.orgs.map(org => itemsOffers.call({
          ...donation,
          orgId: org._id,
          quantity: org.quantity,
        }));
      } else {
        throw new Meteor.Error(400, 'Orgs is required if it\'s selective donation');
      }
    }
    // Reference for the whole auto donation in case of money
    // because money auto donation have frequency
    if (donation.type === 'money') {
      return AutoDonations.insert(donation);
    }
    return true;
  },
});

// higher function to call auto give and need donation
export const donate = new ValidatedMethod({
  name: 'donations.methods.donate',
  validate: new SimpleSchema({
    donationDoc: {
      type: Object,
      blackbox: true,
    },
  }).validator(),
  run({ donationDoc }) {
    userCheck(donationDoc.userId || Meteor.userId(), 'donations.methods.donate');
    // check the role of the user
    Authorize(['admin', 'owner', 'donor']);

    if (donationDoc.isAutomated || !donationDoc.needId) {
      autoGive.call(donationDoc);
    } else {
      needDonate.call(donationDoc);
    }
  },
});

// just change the donation status from the dashboard
export const markCollected = new ValidatedMethod({
  name: 'donations.methods.mark_as_collected',
  validate: new SimpleSchema({
    donation: {
      type: Object,
      blackbox: true,
    },
  }).validator(),
  run({ donation }) {
    userCheck(Meteor.userId(), 'donations.methods.mark_collected');
    Authorize(['owner', 'employee']);
    if (donation.status !== 'Pending') {
      throw new Meteor.Error('Invalid status', 'The donation status must be on pending status');
    }

    Donations.update(donation._id, {
      $set: { status: 'Collected', collectedTime: (new Date()).getTime() },
    });
  },
});

export const updateDonation = new ValidatedMethod({
  name: 'donations.methods.update',
  validate: Donations.schema.pick(
    '_id', 'caseId', 'needId', 'quantity', 'moneyValue', 'type', 'notes', 'donationMethod',
    'collectionAddress', 'collectionTime', 'wholeCase', 'wholeNeed', 'isRecurring', 'isActive', 'userId',
  ).validator({ clean: true }),
  run(donationData) {
    userCheck(donationData.userId, 'donations.methods.update');
    return Donations.update(donationData._id, {
      $set: donationData,
    });
  },
});

export const cancelDonation = new ValidatedMethod({
  name: 'donations.methods.cancel',
  validate: new SimpleSchema({
    donation: {
      type: Object,
      blackbox: true,
    },
  }).validator(),
  run({ donation }) {
    userCheck(donation.userId, 'donations.methods.cancel');
    Donations.update(donation._id, {
      $set: { status: 'Cancelled' },
    });
  },
});

// this won't actually remove it, it will only alter the isRemoved flag
export const removeDonation = new ValidatedMethod({
  name: 'donations.methods.remove',
  validate: Donations.schema.pick('_id', 'userId').validator({ clean: true }),
  run({ _id, userId }) {
    userCheck(userId, 'donations.methods.remove');
    Donations.remove(_id);
  },
});

export const gerUserDonationsList = new ValidatedMethod({
  name: 'donations.methods.donor_donations_list',
  validate: new SimpleSchema({
    userId: {
      type: String,
      regEx: SimpleSchema.RegEx.Id,
      optional: true,
    },
    limit: {
      type: Number,
      defaultValue: 10,
    },
    skip: {
      type: Number,
      defaultValue: 10,
    },
  }).validator({ clean: true }),
  run({ limit, skip, userId }) {
    // check the role of the user
    Authorize(['admin', 'owner', 'donor']);
    const donations = Donations.createQuery({
      $filters: {
        userId: userId || Meteor.userId(),
        isRemoved: { $ne: true },
      },
      $options: { sort: { createdAt: -1 }, limit, skip },
      quantity: 1,
      createdAt: 1,
      type: 1,
      status: 1,
      need: {
        title: 1,
      },
    }).fetch();
    return donations;
  },
});

export const donorDonationActivities = new ValidatedMethod({
  name: 'donations.methods.donor_donation_activities',
  validate: new SimpleSchema({
    userId: {
      type: String,
      regEx: SimpleSchema.RegEx.Id,
      optional: true,
    },
  }).validator({ clean: true }),
  async run({ userId }) {
    // check the role of the user
    Authorize(['admin', 'owner', 'donor']);

    // get the total amount of donations the user made
    const totalUserDonations = await Donations.rawCollection().aggregate([
      { $match: { userId: userId || Meteor.userId() } },
      { $sort: { createdAt: -1 } },
      {
        $group: {
          _id: { userId: '$userId', type: '$type' },
          count: { $sum: 1 },
          totalPrice: { $sum: '$quantity' },
        },
      }, {
        $project: {
          totalPrice: '$totalPrice',
          count: 1,
          _id: 0,
        },
      },
    ]).toArray();

    // get the cases the user donated to and the total price of each case
    const casesDonations = await Donations.rawCollection().aggregate([
      { $match: { userId: userId || Meteor.userId() } },
      { $sort: { createdAt: -1 } },
      {
        $group: {
          _id: '$caseId',
          count: { $sum: 1 },
          totalItems: {
            $sum: {
              $cond: [
                { $eq: ['$type', 'items'] },
                '$quantity',
                0,
              ],
            },
          },
          totalPrice: {
            $sum: {
              $cond: [
                { $eq: ['$type', 'money'] },
                '$quantity',
                0,
              ],
            },
          },
        },
      }, {
        $lookup: {
          from: 'cases',
          localField: '_id',
          foreignField: '_id',
          as: 'case',
        },
      }, {
        $project: {
          totalPrice: '$totalPrice',
          totalItems: 1,
          _id: 1,
          'case.title': 1,
        },
      },
    ]).toArray();

    // get the organizations the user donated to
    // and get the total price and totalItems for each organization
    const orgsDonations = await Donations.rawCollection().aggregate([
      { $match: { userId: userId || Meteor.userId() } },
      { $sort: { createdAt: -1 } },
      {
        $group: {
          _id: '$orgId',
          count: { $sum: 1 },
          totalItems: {
            $sum: {
              $cond: [
                { $eq: ['$type', 'items'] },
                '$quantity',
                0,
              ],
            },
          },
          totalPrice: {
            $sum: {
              $cond: [
                { $eq: ['$type', 'money'] },
                '$quantity',
                0,
              ],
            },
          },
        },
      }, {
        $lookup: {
          from: 'organizations',
          localField: '_id',
          foreignField: '_id',
          as: 'org',
        },
      }, {
        $project: {
          totalPrice: '$totalPrice',
          _id: 1,
          totalItems: 1,
          'org.name': 1,
        },
      },
    ]).toArray();

    // forming result
    const result = await Promise.all([totalUserDonations, casesDonations, orgsDonations])
      .then((values) => {
        let totalCount = 0;
        if (values[0].length !== 0) {
          if (values[0][0] && values[0][1]) {
            totalCount = values[0][0].count + values[0][1].count;
          } else if (values[0][0]) totalCount = values[0][0].count;
          else if (values[0][1]) totalCount = values[0][1].count;
        }
        return {
          userDonations: {
            items: values[0][0],
            money: values[0][1],
            totalCount,
          },
          casesDonations: values[1],
          orgsDonations: values[2],
        };
      });


    return result;
  },
});
