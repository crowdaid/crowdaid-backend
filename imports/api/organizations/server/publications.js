import { Meteor } from 'meteor/meteor';
import Organizations from '/imports/api/organizations/organizations';
import Cases from '/imports/api/cases/cases';
import Needs from '/imports/api/needs/needs';
import Beneficiaries from '/imports/api/beneficiaries/beneficiaries';
import { Authorize, isAdmin } from '/imports/api/utils';

Meteor.publish('OrganizationsTabular', function ({
  filter = {}, limit = 10, skip = 0, sort = { _id: -1 },
}) {
  if (!isAdmin(this.userId)) return this.ready();
  let count = 0;
  let initializing = true;

  const countHandle = Organizations.find({ ...filter, isRemoved: { $ne: true } })
    .observeChanges({
      added: (id) => {
        count += 1;
        if (!initializing) {
          this.changed('CollectionsMeta', 'Organizations', { count });
        }
      },
      removed: (id) => {
        count -= 1;
        this.changed('CollectionsMeta', 'Organizations', { count });
      },
    });
  // Instead, we'll send one `added` message right after `observeChanges` has
  // returned, and mark the subscription as ready.
  this.added('CollectionsMeta', 'Organizations', { count });
  const docsHandle = Organizations.find({ ...filter, isRemoved: { $ne: true } },
    {
      skip, limit, sort, fields: { services: 0, mergedServices: 0, onBoarding: 0 },
    }).observeChanges({
    added: (id, entry) => {
      this.added('organizations', id, entry);
    },
    changed: (id, entry) => {
      this.changed('organizations', id, entry);
    },
    removed: (id, entry) => {
      this.removed('organizations', id, entry);
    },
  });

  initializing = false;
  this.ready();

  // Stop observing the cursor when the client unsubscribes. Stopping a
  // subscription automatically takes care of sending the client any `removed`
  // messages.
  this.onStop(() => {
    countHandle.stop();
    docsHandle.stop();
  });
});

Meteor.publish('organizations.all', function () {
  if (!isAdmin(this.userId)) return this.ready();
  return Organizations.find({ isRemoved: { $ne: true } });
});

Meteor.publish('organizations.single', (orgId) => {
  if (!isAdmin(this.userId)) {
    return Organizations.find(orgId, {
      fields: {
        name: 1, about: 1, HQLocation: 1, claimedBy: 1, verificationStatus: 1,
      },
    });
  }
  return Organizations.find(orgId);
});

Meteor.publish('user.organizations', function (userId) {
  const authorized = Authorize(['employee', 'owner', 'admin'], true);
  if (!authorized) return this.ready();
  return [
    Organizations.find({
      $or: [{ claimedBy: userId || this.userId },
        { employees: userId || this.userId }],
      isRemoved: { $ne: true },
    }),
    Meteor.users.find({ _id: userId || this.userId }, { fields: { organizationId: 1 } }),
  ];
});

Meteor.publish('org.dashboard.stats', function () {
  const authorized = Authorize(['employee', 'owner'], true);
  if (!authorized) return this.ready();
  const { organizationId } = Meteor.user();
  return [
    Cases.find({ organizationId, isRemoved: { $ne: true } }),
    Needs.find({ organizationId, isRemoved: { $ne: true } }),
    Beneficiaries.find({ orgId: organizationId, isRemoved: { $ne: true } }),
    Meteor.users.find({ organizationId, roles: { $in: ['employee'] }, isRemoved: { $ne: true } }),
  ];
});
