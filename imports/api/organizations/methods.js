import SimpleSchema from 'simpl-schema';
import { ValidatedMethod } from 'meteor/mdg:validated-method';
import Organizations from '/imports/api/organizations/organizations';
import {
  Authorize, isOwner, isDonor, inviteUser, isAdmin,
} from '/imports/api/utils';
import { Accounts } from 'meteor/accounts-base';
import { Roles } from 'meteor/alanning:roles';
import sendEmail from '../EmailsUtils';

export const setOrganizationOwner = new ValidatedMethod({
  name: 'organizations.methods.setOwner',
  validate: new SimpleSchema({
    orgId: {
      type: String,
      regEx: SimpleSchema.RegEx.Id,
    },
    email: {
      type: String,
      regEx: SimpleSchema.RegEx.Email,
    },
  }).validator({ clean: true }),
  run({ orgId, email }) {
    Authorize('admin');
    if (!Meteor.isServer) return false;

    const user = inviteUser('owner', email, orgId);

    // User already exists make him the manager for this organization
    Organizations.update({ _id: orgId }, {
      $set: {
        claimedBy: user._id,
      },
    });

    return {
      ownerId: user._id,
      message: !user.invited ? 'Organization manager set successfully' : `An invitation email has been sent to ${email}`,
    };
  },
});

export const createOrganization = new ValidatedMethod({
  name: 'organizations.methods.create',
  validate: Organizations.schema.pick(
    'name',
    'about',
    'registrationCode',
    'registrationDate',
    'HQLocation',
    'legalFiles',
    'categories',
    'owner',
  ).validator({ clean: true }),
  run({
    name,
    about,
    registrationCode,
    registrationDate,
    HQLocation,
    legalFiles,
    owner,
    categories,
  }) {
    if (!isAdmin(this.userId)) {
      throw new Meteor.Error('unauthorized', "You don't have the permission to add organization");
    }
    const dublicateArabicName = Organizations.find({ 'name.ar': name.ar.trim() }).count();
    if (dublicateArabicName) {
      throw new Meteor.Error(400, 'The arabic name is already taken');
    }
    const _id = Organizations.insert({
      name,
      about,
      registrationCode,
      registrationDate,
      HQLocation,
      legalFiles,
      owner,
      categories,
    });

    setOrganizationOwner.call({ orgId: _id, email: owner });
    return Organizations.findOne({ _id });
  },
});

export const updateOrganization = new ValidatedMethod({
  name: 'organizations.methods.update',
  validate: Organizations.schema.pick(
    '_id',
    'name',
    'about',
    'registrationCode',
    'registrationDate',
    'HQLocation',
    'legalFiles',
    'owner',
    'categories',
  ).validator({ clean: true }),
  run({
    _id,
    name,
    about,
    registrationCode,
    registrationDate,
    HQLocation,
    owner,
    categories,
  }) {
    if (!isAdmin(this.userId)) {
      throw new Meteor.Error('unauthorized', "You don't have the permission to add organization");
    }
    const dublicateArabicName = Organizations.find({ 'name.ar': name.ar.trim(), _id: { $ne: _id } }).count();
    if (dublicateArabicName) {
      throw new Meteor.Error(400, 'The arabic name is already taken');
    }
    const orgBeforeUpdate = Organizations.findOne({ _id });
    // get the previous owner of this org
    const prevOnwer = Accounts.findUserByEmail(orgBeforeUpdate.owner);
    if (orgBeforeUpdate.owner !== owner) {
      // check if the previous owner is owner of other orgs
      // if he is not at any org make the organizationId null
      const ownerOrgs = Organizations.find({
        _id: { $ne: _id },
        employees: prevOnwer._id,
        isRemoved: { $ne: true },
      }, { fields: { _id: 1 } }).fetch();
      if (ownerOrgs.length === 0) {
        Meteor.users.update({ _id: prevOnwer._id }, { $set: { organizationId: null } });
      } else {
        Meteor.users.update({ _id: prevOnwer._id }, { $set: { organizationId: ownerOrgs[0]._id } });
      }
      setOrganizationOwner.call({ orgId: _id, email: owner });
      Organizations.update({ _id }, {
        $pull: { // remove the previous owner from the org employees list
          employees: prevOnwer._id,
        },
      });
    }
    Organizations.update({ _id }, {
      $set: {
        name,
        about,
        registrationCode,
        registrationDate,
        HQLocation,
        categories,
        owner,
      },
    });

    return Organizations.findOne({ _id });
  },
});

export const addOrganizationFile = new ValidatedMethod({
  name: 'organizations.methods.addLegalFile',
  validate: new SimpleSchema({
    id: {
      type: String,
      regEx: SimpleSchema.RegEx.Id,
    },
    fileId: {
      type: String,
      regEx: SimpleSchema.RegEx.Id,
    },
  }).validator({ clean: true }),
  run({
    id, fileId,
  }) {
    Authorize('admin');
    // we should check for permissions
    Organizations.update({ _id: id }, {
      $addToSet: {
        legalFiles: fileId,
      },
    });
  },
});
export const updateOrganizationLogo = new ValidatedMethod({
  name: 'organizations.methods.update_logo',
  validate: new SimpleSchema({
    orgId: {
      type: String,
      regEx: SimpleSchema.RegEx.Id,
    },
    fileId: {
      type: String,
      regEx: SimpleSchema.RegEx.Id,
    },
  }).validator({ clean: true }),
  run({
    orgId, fileId,
  }) {
    Authorize('admin');
    // we should check for permissions
    Organizations.update({ _id: orgId }, {
      $set: {
        logo: fileId,
      },
    });
  },
});


// this won't actually remove it, it will only alter the isRemoved flag
export const removeOrganization = new ValidatedMethod({
  name: 'organizations.methods.remove',
  validate: new SimpleSchema({
    _id: {
      type: String,
      regEx: SimpleSchema.RegEx.Id,
    },
  }).validator({ clean: true }),
  run({ _id }) {
    Authorize('admin');
    Organizations.update({ _id }, {
      $set: {
        isRemoved: true,
        removedAt: new Date(),
      },
    });

    // remove the owner role if not owner of any other org
    const currentOrg = Organizations.findOne({ _id });
    // logout all the employees in this organization
    Meteor.users.update({ _id: { $in: currentOrg.employees } }, {
      $set: {
        'services.resume.loginTokens': [],
      },
    });
    const ownerOtherOrgsCount = Organizations.find({
      _id: { $ne: currentOrg._id },
      owner: currentOrg.owner,
      isRemoved: { $ne: true },
    }).count();
    const owner = Accounts.findUserByEmail(currentOrg.owner);
    if (ownerOtherOrgsCount === 0) {
      Roles.removeUsersFromRoles(owner._id, 'owner');
    }
  },
});


export const removeOrganizationOwner = new ValidatedMethod({
  name: 'organizations.methods.removeOwner',
  validate: new SimpleSchema({
    userId: {
      type: String,
      regEx: SimpleSchema.RegEx.Id,
    },
    newOwner: {
      type: String,
      regEx: SimpleSchema.RegEx.email,
    },
  }).validator({ clean: true }),
  run({ userId, newOwner }) {
    Authorize('admin');
    if (!Roles.userIsInRole(userId, ['owner'])) return;
    // first remove the owner from the org employees list
    // update the owner of old owner orgs to the new ower
    // mark the old onwner as removed and set his organizationId to null
    // last make the send invitation email to the new onwer and make him the new owner
    // this simply replace the old onwer (multi orgs) with new one
    const owner = Meteor.users.findOne({ _id: userId });
    const ownerOrgs = Organizations.find({
      employees: userId,
    }, { fields: { _id: 1 } }).fetch();
    // set the new owner of the organization
    const { ownerId } = setOrganizationOwner.call({ orgId: owner.organizationId, email: newOwner });
    const currentOrg = Organizations.findOne({ _id: owner.organizationId });
    // remove from the orgs
    Organizations.update({ _id: { $in: ownerOrgs.map(org => org._id) } }, {
      $pull: {
        employees: userId,
      },
      $set: {
        owner: newOwner,
      },
    }, { multi: true });
    // add the new to orgs
    Organizations.update({ _id: { $in: ownerOrgs.map(org => org._id) } }, {
      $addToSet: { employees: ownerId },
    }, { multi: true });
    // send removing email to the owner
    sendEmail(owner.emails[0].address, 'employee', currentOrg, true);
    Meteor.users.update({ _id: userId }, {
      $set: {
        isRemoved: true,
        removedAt: new Date(),
        organizationId: null,
        'services.resume.loginTokens': [], // logout the owner
      },
    });
  },
});

export const inviteEmployee = new ValidatedMethod({
  name: 'organizations.methods.inviteEmployee',
  validate: new SimpleSchema({
    orgId: {
      type: String,
      regEx: SimpleSchema.RegEx.Id,
    },
    email: {
      type: String,
      regEx: SimpleSchema.RegEx.Email,
    },
  }).validator({ clean: true }),
  run({ orgId, email }) {
    Authorize('manager');
    if (Meteor.isServer) {
      const user = Accounts.findUserByEmail(email);
      if (user) {
        const org = Organizations.findOne(orgId);
        if (!org) {
          throw new Meteor.Error('organizations.inviteEmployee', 'Organization not found');
        }

        if (org.employees.indexOf(user._id) !== -1) {
          throw new Meteor.Error('organizations.inviteEmployee', 'User is already working for this organization');
        }

        // User already exists make him the manager for this organization
        Organizations.update({ _id: orgId }, {
          $push: {
            employees: user._id,
          },
        });
        // Add manger role to this user
        Roles.addUsersToRoles(user._id, ['employee', `manage-org-${orgId}`]);
        return `Employee is now working for ${org.name.en}`;
      }
      // User does not exist send an invitation email
      const invitedId = Accounts.createUser({ email, services: { password: {} } });
      Organizations.update({ _id: orgId }, {
        $push: {
          employees: invitedId,
        },
      });
      // Send invitation email
      Accounts.sendEnrollmentEmail(invitedId, email, {
        orgId,
      });
      // Add manger role to this user
      Roles.addUsersToRoles(invitedId, ['employee', `manage-org-${orgId}`]);
      return `An invitation email has been sent to ${email}`;
    }
  },
});

// switch user (owner, employee) org
export const switchOrganization = new ValidatedMethod({
  name: 'organizations.methods.switch_organization',
  validate: new SimpleSchema({
    orgId: {
      type: String,
      regEx: SimpleSchema.RegEx.Id,
    },
  }).validator({ clean: true }),
  run({ orgId }) {
    Authorize(['manager', 'employee', 'owner']);
    Meteor.users.update({ _id: Meteor.userId() }, {
      $set: {
        organizationId: orgId,
      },
    });
  },
});


export const getDonorOrgs = new ValidatedMethod({
  name: 'organizations.methods.get_donor_orgs',
  validate: new SimpleSchema({
    filters: {
      type: Object,
      blackbox: true,
    },
    limit: {
      type: Number,
      optional: true,
      defaultValue: 10,
    },
    skip: {
      type: Number,
      optional: true,
      defaultValue: 0,
    },
  }).validator({ clean: true }),
  run({
    filters, limit, skip,
  }) {
    if (!isDonor(Meteor.userId())) {
      throw new Meteor.Error(500, 'Not allow to do this action ');
    }
    const query = Organizations.createQuery({
      $filters: { ...filters, isRemoved: { $ne: true } },
      $options: { sort: { createdAt: -1 }, limit, skip },
      name: 1,
      logo: 1,
      logoImge: 1,
    });
    return query.fetch();
  },
});
