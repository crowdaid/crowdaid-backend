// Meteor core imports
import { Mongo } from 'meteor/mongo';
import { Tracker } from 'meteor/tracker';

// node imports
import SimpleSchema from 'simpl-schema';

// internal imports
import commonData from '/imports/api/sharedSchemas';
import { verificationStatuses } from '/imports/api/commonValues';

const Organizations = new Mongo.Collection('organizations');

SimpleSchema.extendOptions(['autoform']);
Organizations.schema = new SimpleSchema({
  _id: {
    type: String,
    regEx: SimpleSchema.RegEx.Id,
  },
  name: {
    type: Object,
  },
  'name.en': {
    type: String,
  },
  'name.ar': {
    type: String,
  },
  slug: {
    type: String,
    autoValue() {
      if (this.isInsert) {
        return `${this.field('name.en').value.replace(' ', '-')}-${(new Date()).getTime()}`;
      }
      this.unset(); // Pre
    },
  },
  about: {
    type: String,
    optional: true,
  },
  registrationCode: {
    type: String,
  },
  registrationDate: {
    type: Date,
  },
  HQLocation: {
    type: Object,
    required: true,
  },
  'HQLocation.address': {
    type: String,
    required: true,
  },
  'HQLocation.location': {
    type: Object,
    required: true,
  },
  'HQLocation.location.type': {
    type: String,
    allowedValues: ['Point'],
    required: true,
  },
  'HQLocation.location.coordinates': {
    type: Array,
    maxCount: 2,
    minCount: 2,
    required: true,
  },
  'HQLocation.location.coordinates.$': {
    type: Number,
    required: true,
  },
  legalFiles: { // mutiple file upload
    type: Array,
    defaultValue: [],
    optional: true,
  },
  'legalFiles.$': {
    type: String,
    regEx: SimpleSchema.RegEx.Id,
  },
  logo: {
    type: String,
    regEx: SimpleSchema.RegEx.Id,
    optional: true,
  },
  categories: {
    type: Array,
    optional: true,
  },
  'categories.$': {
    type: String,
    regEx: SimpleSchema.RegEx.Id,
  },
  owner: {
    type: String,
    optional: true, // temp
  },
  verifiedBy: { // admin added the organization
    type: String,
    regEx: SimpleSchema.RegEx.Id,
    optional: true,
  },
  verificationStatus: {
    type: String,
    allowedValues: verificationStatuses,
    defaultValue: 'pending',
  },
  employees: {
    type: Array,
    optional: true,
    defaultValue: [],
  },
  'employees.$': {
    type: String,
    regEx: SimpleSchema.RegEx.Id,
  },
}, { trimStrings: true, tracker: Tracker });

Organizations.schema.extend(commonData);

Organizations.attachSchema(Organizations.schema);


// Deny all client-side updates since we will be using methods to manage this collection
Organizations.deny({
  insert() {
    return true;
  },
  update() {
    return true;
  },
  remove() {
    return true;
  },
});

export default Organizations;
