import SimpleSchema from 'simpl-schema';
import { ValidatedMethod } from 'meteor/mdg:validated-method';
import Needs from '/imports/api/needs/needs';
import Cases from '/imports/api/cases/cases';
import Donations from '/imports/api/donations/donations';
import { userCheck, Authorize } from '/imports/api/utils';
import { needStatuses } from '/imports/api/commonValues';

export const createNeed = new ValidatedMethod({
  name: 'needs.methods.create',
  validate: Needs.schema.pick(
    'title',
    'description',
    'isQuantifiable',
    'quantityNeeded',
    'unitOfMeasure',
    'needTypeId',
    'itemId',
    'itemPrice',
    'status',
    'dueDate',
    'urgency',
    'frequency',
    'caseId',
  ).validator({ clean: true }),
  run({
    title,
    description,
    isQuantifiable,
    quantityNeeded,
    status,
    needTypeId,
    itemId,
    unitOfMeasure,
    itemPrice,
    dueDate,
    urgency,
    frequency,
    caseId,
  }) {
    Authorize(['owner', 'employee']);

    const { organizationId } = Meteor.users.findOne(
      { _id: this.userId },
      { fields: { organizationId: 1 } },
    );
    const { caseStatus } = Cases.findOne({ _id: caseId });

    if (caseStatus === 'draft') Cases.update({ _id: caseId }, { $set: { status: 'pending' } });

    if (isQuantifiable && !quantityNeeded && !itemPrice) {
      throw new Meteor.Error(400, 'Must specify the quantity needed \n and item price if the need is Quantifiable');
    }

    return Needs.insert({
      title,
      caseId,
      description,
      needTypeId,
      itemId,
      isQuantifiable,
      quantityNeeded,
      unitOfMeasure,
      itemPrice,
      status,
      dueDate,
      urgency,
      frequency,
      organizationId,
    });
  },
});

export const updateNeed = new ValidatedMethod({
  name: 'needs.methods.update',
  validate: Needs.schema.pick(
    '_id',
    'title',
    'description',
    'isQuantifiable',
    'needTypeId',
    'itemId',
    'quantityNeeded',
    'unitOfMeasure',
    'itemPrice',
    'dueDate',
    'urgency',
    'frequency',
  ).validator({ clean: true }),
  run({
    _id,
    title,
    needTypeId,
    itemId,
    description,
    isQuantifiable,
    quantityNeeded,
    unitOfMeasure,
    itemPrice,
    dueDate,
    urgency,
    frequency,
  }) {
    Authorize(['employee', 'owner']);

    const { organizationId } = Meteor
      .users.findOne({ _id: this.userId }, { fields: { organizationId: 1 } });
    const need = Needs.findOne({ _id });

    if (organizationId !== need.organizationId) {
      throw new Meteor.Error(401, 'You are not allowed to access this need.');
    }


    if (isQuantifiable && !quantityNeeded && !itemPrice) {
      throw new Meteor.Error(400, 'Must specify the quantity needed \n and item price if the need is Quantifiable');
    }

    Needs.update(_id, {
      $set: {
        title,
        needTypeId,
        itemId,
        description,
        isQuantifiable,
        quantityNeeded,
        unitOfMeasure,
        itemPrice,
        dueDate,
        urgency,
        frequency,
      },
    });
  },
});
// export const addToCase = new ValidatedMethod({
//   name: 'needs.methods.addToCase',
//   validate: new SimpleSchema({
//     caseId: { type: SimpleSchema.RegEx.Id },
//     needId: { type: SimpleSchema.RegEx.Id },
//   }).validator({ clean: true }),
//   run({
//     caseId,
//     needId,
//   }) {
//     Authorize(['employee', 'owner']);
//     Needs.update(needId, {
//       $addToSet: {
//         cases: caseId,
//       },
//     });
//     Cases.update(caseId, {
//       $addToSet: {
//         needs: needId,
//       },
//     });
//   },
// });

// export const updateNeed = new ValidatedMethod({
//     name: 'needs.methods.update',
//     validate: Needs.schema.pick('caseId', 'text', 'quantity', 'frequent')
//      .validator({clean: true}),
//     run({ caseId, userId, text, quantity, frequent }){
//         console.log('inside method', userId, caseId, userId, text, quantity);
//
//         userCheck(userId, 'needs.methods.update');
//
//         // we should check for permessions later
//
//         Needs.update({caseId, userId, text, quantity, frequent});
//     }
// });


// this won't actually remove it, it will only flip the isRemoved flag
export const removeNeed = new ValidatedMethod({
  name: 'needs.methods.remove',
  validate: new SimpleSchema({
    _id: {
      type: SimpleSchema.RegEx.Id,
    },
  }).validator({ clean: true }),
  run({
    _id,
  }) {
    Authorize(['owner', 'employee']);
    Needs.update({ _id }, { $set: { isRemoved: true, removedAt: new Date() } });
    Donations.update({ needId: _id },
      { $set: { isRemoved: true, removedAt: new Date() } },
      { multi: true });
  },
});

export const setNeedStatus = new ValidatedMethod({
  name: 'needs.methods.setStatus',
  validate: new SimpleSchema({
    status: {
      type: String,
      allowedValues: needStatuses,
    },
    userId: { type: SimpleSchema.RegEx.Id },
    needId: { type: SimpleSchema.RegEx.Id },
  }).validator({ clean: true }),
  run({ needId, userId, status }) {
    userCheck(userId, 'needs.methods.setStatus');
    Needs.update(needId, { $set: { status } });
  },
});
