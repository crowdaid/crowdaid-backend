// Meteor core imports
import { Mongo } from 'meteor/mongo';
import { Tracker } from 'meteor/tracker';

// node imports
import SimpleSchema from 'simpl-schema';

// internal imports
import commonData from '/imports/api/sharedSchemas';
import {
  needStatuses, urgencyFlags, Units, needFrequencies,
} from '/imports/api/commonValues';


const Needs = new Mongo.Collection('needs');

SimpleSchema.extendOptions(['autoform']);
Needs.schema = new SimpleSchema({
  _id: {
    type: String,
    regEx: SimpleSchema.RegEx.Id,
  },
  organizationId: {
    type: SimpleSchema.RegEx.Id,
  },
  caseId: {
    type: SimpleSchema.RegEx.Id,
  },
  needTypeId: {
    type: SimpleSchema.RegEx.Id,
  },
  itemId: {
    type: SimpleSchema.RegEx.Id,
    optional: true,
  },
  title: {
    type: String,
  },
  description: {
    type: String,
    optional: true,
  },
  isQuantifiable: {
    type: Boolean,
    defaultValue: true,
  },

  unitOfMeasure: {
    type: String,
    defaultValue: 'EGP',
    allowedValues: Units,
  },
  quantityNeeded: {
    type: Number,
    defaultValue: 1,
  },
  itemPrice: {
    type: Number,
    defaultValue: 1,
  },
  quantityFulfilled: {
    type: Number,
    defaultValue: 0,
  },
  status: {
    type: String,
    allowedValues: needStatuses,
    defaultValue: 'untouched',
  },
  urgency: {
    type: String,
    allowedValues: urgencyFlags,
    defaultValue: 'N/A',
  },
  frequency: {
    type: String,
    allowedValues: needFrequencies,
    defaultValue: 'Once',
  },
  dueDate: {
    type: Date,
    optional: true,
  },
  attachments: {
    type: Array,
    optional: true,
  },
  'attachments.$': {
    type: String,
    regEx: SimpleSchema.RegEx.Id,
  },
}, { trimStrings: true, tracker: Tracker });

Needs.schema.extend(commonData);

Needs.attachSchema(Needs.schema);


// Deny all client-side updates since we will be using methods to manage this collection
Needs.deny({
  insert() {
    return true;
  },
  update() {
    return true;
  },
  remove() {
    return true;
  },
});

export default Needs;
