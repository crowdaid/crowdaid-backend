import { Meteor } from 'meteor/meteor';
import Needs from '/imports/api/needs/needs';
import {
  isAdmin, isEmployee, isOwner, isDonor,
} from '/imports/api/utils';

Meteor.publish('NeedsTabular', function ({
  filter = {}, limit = 10, skip = 0, sort = { _id: -1 }, caseId,
}) {
  if (!isOwner(this.userId) && !isEmployee) return this.ready();
  let count = 0;
  let initializing = true;

  const userId = Meteor.userId();
  const { organizationId } = Meteor.users.findOne({ _id: userId }, { fields: { organizationId: 1 } });

  const query = {
    ...filter, isRemoved: { $ne: true }, organizationId, caseId,
  };
  const countHandle = Needs.find(query)
    .observeChanges({
      added: (id) => {
        count += 1;
        if (!initializing) {
          this.changed('CollectionsMeta', 'Needs', { count });
        }
      },
      removed: (id) => {
        count -= 1;
        this.changed('CollectionsMeta', 'Needs', { count });
      },
    });
    // Instead, we'll send one `added` message right after `observeChanges` has
    // returned, and mark the subscription as ready.
  this.added('CollectionsMeta', 'Needs', { count });
  const docsHandle = Needs.find(query,
    {
      skip, limit, sort, fields: { services: 0, mergedServices: 0, onBoarding: 0 },
    }).observeChanges({
    added: (id, entry) => {
      this.added('needs', id, entry);
    },
    changed: (id, entry) => {
      this.changed('needs', id, entry);
    },
    removed: (id, entry) => {
      this.removed('needs', id, entry);
    },
  });

  initializing = false;
  this.ready();

  // Stop observing the cursor when the client unsubscribes. Stopping a
  // subscription automatically takes care of sending the client any `removed`
  // messages.
  this.onStop(() => {
    countHandle.stop();
    docsHandle.stop();
  });
});

Meteor.publish('needs.all', function () {
  if (isAdmin()) {
    return Needs.find({});
  }
  this.stop();
});

Meteor.publish('needs.case', function (casee) {
  // if (Authorize(['admin', 'manage-org-' + casee.owner.id], true)) {
  if (this.userId) {
    return Needs.find({ caseId: casee._id });
  }
  this.stop();
});

Meteor.publish('needs.case.feed', function (caseId) {
  if (!isDonor(this.userId)) {
    this.stop();
    throw new Meteor.Error(500, 'Not allow to do this action ');
  }
  return Needs.find({ caseId, isRemoved: { $ne: true } });
});

Meteor.publish('needs.single', function (needId) {
  if (isOwner(this.userId) || isEmployee(this.userId)) return Needs.find({ _id: needId });
  this.stop();
});
