import { Meteor } from 'meteor/meteor';
import { Roles } from 'meteor/alanning:roles';
import fs from 'fs';
import * as pathLib from 'path';
import Organizations from './organizations/organizations';
import sendEmail from './EmailsUtils';

const checkAdmin = (userId = Meteor.userId()) => Roles.userIsInRole(userId, ['admin']);
const checkSuperAdmin = (userId = Meteor.userId()) => Roles.userIsInRole(userId, ['superadmin']);

export const Authorize = (roles, opts) => {
  const userId = Meteor.userId();
  const authorized = Roles.userIsInRole(userId, roles);
  if (authorized) return true;
  if (opts === true || (opts && opts.ret)) return false;
  throw new Meteor.Error('Unauthorized', 'Your are not authorized to do this action');
};


export const inviteUser = (role, email, existOrgId) => {
  const ownerId = Meteor.userId();
  const owner = Meteor.users.findOne({ _id: ownerId, roles: 'owner' }, { fields: { organizationId: 1 } });
  const orgId = owner ? owner.organizationId : existOrgId;
  const user = Accounts.findUserByEmail(email);

  if (user) {
    Roles.addUsersToRoles(user._id, [role]);
    const { services: { password } } = user;

    if (role !== 'admin' && orgId) {
      const theOrg = Organizations.findOne({ _id: orgId });
      Organizations.update(orgId, { $addToSet: { employees: user._id } });
      if (user.organizationId !== orgId) {
        Meteor.users.update({ _id: user._id },
          { $set: { organizationId: orgId, isRemoved: false } });
        // send email
        sendEmail(email, 'employee', theOrg);
        return {
          _id: user._id, email, role, enrolled: true,
        };
      } if (password && password.bcrypt) {
        return {
          _id: user._id, email, role, inSystem: true,
        };
      }
    }

    if (role === 'admin') {
      if (user.isRemoved) {
        Meteor.users.update({ _id: user._id }, { $set: { isRemoved: false } });
        // send email
        sendEmail(email, 'admin');
        return {
          _id: user._id, email, role, enrolled: true,
        };
      } if (password && password.bcrypt) {
        return {
          _id: user._id, email, role, inSystem: true,
        };
      }
    }
    // if does exist resend the enrollment email
    Accounts.sendEnrollmentEmail(user._id, email, { role, orgId });

    return {
      _id: user._id, email, role, resent: true,
    };
  }
  // User does not exist send an invitation email
  const userId = Accounts.createUser({ email, services: { password: {} } });
  // add isRemoved, as all tables publications depend on it
  Meteor.users.update({ _id: userId }, { $set: { isRemoved: false } });

  // Add manger role to this user and set its ref
  Roles.addUsersToRoles(userId, [role]);

  if (role !== 'admin' && orgId) {
    Organizations.update(orgId, { $addToSet: { employees: userId } });
    Meteor.users.update({ _id: userId }, { $set: { organizationId: orgId } });
  }

  // Send invitation email
  Accounts.sendEnrollmentEmail(userId, email, { role, orgId });
  return {
    _id: userId, email, role, invited: true,
  };
};

export const isManager = userId => Roles.userIsInRole(userId, ['manager']);

export const isDonor = userId => Roles.userIsInRole(userId, ['donor']);

export const isOwner = (userId) => {
  const user = Meteor.users.findOne({ _id: userId || Meteor.userId() });
  const org = Organizations.findOne({ _id: user && user.organizationId });
  const owner = Roles.userIsInRole(userId, ['owner']);
  if (owner && org && org.owner === user.emails[0].address) {
    return true;
  }
  return false;
};
export const isEmployee = userId => Roles.userIsInRole(userId, ['employee']);

export const isStuff = userId => Roles.userIsInRole(userId, ['manager', 'employee']);

export const userCheck = (userId, methodName) => {
  if (!Meteor.userId()) {
    throw new Meteor.Error(
      `${methodName}.non-user`,
      'only logged in users can do this action',
    );
  }

  if (userId !== Meteor.userId() && !checkAdmin(Meteor.userId())) {
    throw new Meteor.Error(
      `${methodName}.incosistant_userId`,
      "client provided userId doesn't match server userId",
    );
  }
};

export const isAdmin = checkAdmin;
export const isSuperAdmin = checkSuperAdmin;

// validation

// validate email
export const validateEmail = (email) => {
  const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(email).toLowerCase());
};

// create dir recursively
export const mkDirInStorage = (targetDir) => {
  const { sep } = pathLib;
  const initDir = Meteor.settings.storage.path;
  targetDir.split(sep).reduce((parentDir, childDir) => {
    const curDir = pathLib.resolve(parentDir, childDir);
    try {
      if (!fs.existsSync(curDir)) {
        fs.mkdirSync(curDir);
      }
    } catch (err) {
      throw new Meteor.Error(400, 'Error on saving File');
    }
    return curDir;
  }, initDir);
};

export default userCheck;
