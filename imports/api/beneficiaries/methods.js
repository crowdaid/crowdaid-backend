import SimpleSchema from 'simpl-schema';
import { ValidatedMethod } from 'meteor/mdg:validated-method';
import Beneficiaries from '/imports/api/beneficiaries/beneficiaries';
import Cases from '/imports/api/cases/cases';
import { Authorize } from '/imports/api/utils';
import { createFamilyMember } from '../families/methods';
import Families from '../families/families';

export const createBeneficiary = new ValidatedMethod({
  name: 'beneficiaries.methods.create',
  validate: Beneficiaries.schema.pick(
    'name', 'gender', 'areaType', 'homePhone',
    'mobile', 'nationalIdNumber', 'about',
    'birthDate', 'verificationStatus',
  ).validator({ clean: true }),
  run({
    name, gender, areaType, homePhone,
    mobile, nationalIdNumber, about, birthDate,
    verificationStatus,
  }) {
    Authorize(['owner', 'superadmin', 'employee']);
    // find if the national number is already exists
    const duplicateNationalNum = Beneficiaries
      .find({
        nationalIdNumber,
        isRemoved: { $ne: true },
        orgId: Meteor.user().organizationId,
      }).count();
    if (duplicateNationalNum) {
      throw new Meteor.Error(403, 'National ID number already exist in this organization');
    }
    const id = Beneficiaries.insert({
      name,
      gender,
      areaType,
      homePhone,
      mobile,
      nationalIdNumber,
      about,
      birthDate,
      verificationStatus,
      orgId: Meteor.user().organizationId,
      userId: Meteor.userId(),
      verifiedBy: Meteor.userId(),
    });
    // add him to family table
    createFamilyMember.call({
      name: name.ar,
      gender,
      relationType: 'self',
      birthDate,
      socialStatus: about,
      nationalIdNumber,
      beneficiaryId: id,
    });
    return id;
  },
});

export const updateBeneficiary = new ValidatedMethod({
  name: 'beneficiaries.methods.update',
  validate: Beneficiaries.schema.pick(
    '_id',
    'name', 'gender', 'areaType', 'homePhone',
    'mobile', 'nationalIdNumber', 'about',
    'birthDate', 'verificationStatus',
  ).validator({ clean: true }),
  run({
    _id,
    name, gender, areaType, homePhone,
    mobile, nationalIdNumber, about, birthDate,
    verificationStatus,
  }) {
    Authorize(['owner', 'superadmin', 'employee']);
    const { organizationId } = Meteor
      .users.findOne({ _id: this.userId }, { fields: { organizationId: 1 } });
    const beneficiary = Beneficiaries.findOne({ _id });

    if (organizationId !== beneficiary.orgId) {
      throw new Meteor.Error(401, 'You are not allowed to access this need.');
    }

    // find if the national number is already exists
    const duplicateNationalNum = Beneficiaries
      .find({
        _id: { $ne: _id },
        nationalIdNumber,
        isRemoved: { $ne: true },
        orgId: Meteor.user().organizationId,
      }).count();
    if (duplicateNationalNum) {
      throw new Meteor.Error(403, 'National ID number already exist in this organization');
    }
    // we should check for permissions
    return Beneficiaries.update(_id, {
      $set: {
        name,
        gender,
        areaType,
        homePhone,
        mobile,
        nationalIdNumber,
        about,
        birthDate,
        verificationStatus,
        userId: Meteor.userId(),
      },
    });
  },
});

// this won't actually remove it, it will only flip the isRemoved flag
export const removeBeneficiary = new ValidatedMethod({
  name: 'beneficiaries.methods.remove',
  validate: new SimpleSchema({
    _id: {
      type: String,
      regEx: SimpleSchema.RegEx.Id,
    },
  }).validator({ clean: true }),
  run({ _id }) {
    Authorize(['owner', 'superadmin', 'employee']);
    // check if this person is a beneficiary from any case
    // if true you can't remove it before change the beneficiary in the case or delete it
    const benCount = Cases.find({ beneficiary: _id }).count();
    if (benCount !== 0) throw new Meteor.Error(400, 'This person is beneficiary from case');
    Beneficiaries.update({
      _id,
    },
    {
      $set: {
        isRemoved: true,
        removedAt: new Date(),
      },
    });
    // Remove his familt
    Families.update({ beneficiaryId: _id }, {
      $setOnInsert: { removedAt: new Date() },
      $set: { isRemoved: true },
    }, { multi: true });
  },
});


export const updateBeneficiaryImage = new ValidatedMethod({
  name: 'beneficiaries.methods.update_photo',
  validate: new SimpleSchema({
    benId: {
      type: String,
      regEx: SimpleSchema.RegEx.Id,
    },
    fileId: {
      type: String,
      regEx: SimpleSchema.RegEx.Id,
    },
  }).validator({ clean: true }),
  run({
    benId, fileId,
  }) {
    Authorize(['owner', 'employee']);
    // we should check for permissions
    Beneficiaries.update({ _id: benId }, {
      $set: {
        photo: fileId,
      },
    });
  },
});


export const addAndUpdateHomeDescription = new ValidatedMethod({
  name: 'beneficiaries.methods.add_and_update_home_descriptions',
  validate: new SimpleSchema({
    _id: {
      type: String,
      regEx: SimpleSchema.RegEx.Id,
    },
    homeDescription: {
      type: Object, blackbox: true,
    },
  }).validator({ clean: true }),
  run({ _id, homeDescription }) {
    Authorize(['owner', 'superadmin', 'employee']);
    return Beneficiaries.update({ _id }, {
      $set: {
        homeDescription,
      },
    });
  },
});

export const addAttachment = new ValidatedMethod({
  name: 'beneficiaries.methods.addAttachment',
  validate: new SimpleSchema({
    benId: {
      type: SimpleSchema.RegEx.Id,
    },
    fileId: {
      type: SimpleSchema.RegEx.Id,
    },
  }).validator({ clean: true }),
  run({ benId, attachment }) {
    Authorize(['owner', 'employee']);
    Beneficiaries.update(benId, {
      $addToSet: {
        attachments: { _id: attachment.id, description: attachment.description },
      },
    });
  },
});
