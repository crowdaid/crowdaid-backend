// Meteor core imports
import { Mongo } from 'meteor/mongo';
import { Tracker } from 'meteor/tracker';

// node imports
import SimpleSchema from 'simpl-schema';

// internal imports
import commonData from '/imports/api/sharedSchemas';
import { beneficiaryTypes, verificationStatuses } from '/imports/api/commonValues';

const Beneficiaries = new Mongo.Collection('beneficiaries');

SimpleSchema.extendOptions(['autoform']);
Beneficiaries.schema = new SimpleSchema({
  _id: {
    type: String,
    regEx: SimpleSchema.RegEx.Id,
  },
  type: {
    type: String,
    allowedValues: beneficiaryTypes,
    defaultValue: 'family',
  },
  name: {
    type: Object,
  },
  'name.en': {
    type: String,
  },
  'name.ar': {
    type: String,
  },
  about: {
    type: String,
    optional: true,
  },
  nationalIdNumber: {
    type: String,
  },
  birthDate: {
    type: Date,
  },
  gender: {
    type: String,
    allowedValues: ['Male', 'Female'],
  },
  areaType: {
    type: String,
    allowedValues: ['countryside', 'town'],
  },
  homePhone: {
    type: String,
    optional: true,
  },
  mobile: {
    type: String,
    optional: true,
  },
  address: {
    type: String,
    optional: true,
  },
  photo: {
    type: String,
    regEx: SimpleSchema.RegEx.Id,
    optional: true,
  },
  attachments: {
    type: Array,
    optional: true,
  },
  'attachments.$': {
    type: Object,
  },
  'attachments.$._id': {
    type: String,
    optional: true,
  },
  'attachments.$.description': {
    type: String,
    optional: true,
  },
  verifiedBy: {
    type: String,
    regEx: SimpleSchema.RegEx.Id,
    optional: true,
  },
  verificationStatus: {
    type: String,
    allowedValues: verificationStatuses,
    defaultValue: 'verified',
  },
  orgId: {
    type: String,
    regEx: SimpleSchema.RegEx.Id,
  },
  // Home details
  homeDescription: {
    type: Object,
    blackbox: true,
    optional: true,
  },
}, { trimStrings: true, tracker: Tracker });

Beneficiaries.schema.extend(commonData);

Beneficiaries.attachSchema(Beneficiaries.schema);


// Deny all client-side updates since we will be using methods to manage this collection
Beneficiaries.deny({
  insert() {
    return true;
  },
  update() {
    return true;
  },
  remove() {
    return true;
  },
});

export default Beneficiaries;
