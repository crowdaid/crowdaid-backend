import { Meteor } from 'meteor/meteor';
import Beneficiaries from '/imports/api/beneficiaries/beneficiaries';
import { Authorize, isAdmin } from '/imports/api/utils';


Meteor.publish('BeneficiariesTabular', function ({
  filter = {}, limit = 10, skip = 0, sort = { _id: -1 }, orgId,
}) {
  if (!Authorize(['owner', 'superadmin', 'admin', 'employee'])) return this.ready();
  let count = 0;
  let initializing = true;

  const query = { ...filter, orgId, isRemoved: { $ne: true } };

  const user = Meteor.users.findOne({ _id: this.userId }, { fields: { organizationId: 1 } });

  if (!isAdmin(this.userId)) {
    if (user.organizationId) query.orgId = user.organizationId;
  }

  const countHandle = Beneficiaries.find(query)
    .observeChanges({
      added: (id) => {
        count += 1;
        if (!initializing) {
          this.changed('CollectionsMeta', 'Beneficiaries', { count });
        }
      },
      removed: (id) => {
        count -= 1;
        this.changed('CollectionsMeta', 'Beneficiaries', { count });
      },
    });
  // Instead, we'll send one `added` message right after `observeChanges` has
  // returned, and mark the subscription as ready.
  this.added('CollectionsMeta', 'Beneficiaries', { count });
  const docsHandle = Beneficiaries.find(query,
    {
      skip, limit, sort, fields: { services: 0, mergedServices: 0, onBoarding: 0 },
    }).observeChanges({
    added: (id, entry) => {
      this.added('beneficiaries', id, entry);
    },
    changed: (id, entry) => {
      this.changed('beneficiaries', id, entry);
    },
    removed: (id, entry) => {
      this.removed('beneficiaries', id, entry);
    },
  });

  initializing = false;
  this.ready();

  // Stop observing the cursor when the client unsubscribes. Stopping a
  // subscription automatically takes care of sending the client any `removed`
  // messages.
  this.onStop(() => {
    countHandle.stop();
    docsHandle.stop();
  });
});


Meteor.publish('beneficiaries.all', function () {
  if (!isAdmin()) return this.ready();
  return Beneficiaries.find({});
});

Meteor.publish('org.beneficiaries', function (orgId) {
  if (!Authorize(['admin', 'employee', 'owner'], true)) return this.ready();
  return Beneficiaries.find({
    orgId: orgId || Meteor.user().organizationId,
    isRemoved: { $ne: true },
    verificationStatus: 'verified',
  });
});

Meteor.publish('beneficiaries.single', function (benId) {
  if (!this.userId) return this.ready();
  if (!Authorize(['owner', 'superadmin', 'employee'], true)) {
    return Beneficiaries.find(benId, {
      fields: {
        name: 1, about: 1, birthDate: 1, address: 1, type: 1, verificationStatus: 1, nationalIdNumber: 1,
      },
    });
  }
  return Beneficiaries.find({ _id: benId });
});
