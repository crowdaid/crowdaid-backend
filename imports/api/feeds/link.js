import Cases from '../cases/cases';
import Categories from '../categories/categories';
import Needs from '../needs/needs';
import Organizations from '../organizations/organizations';
import Files from '../files/files';
import Donations from '../donations/donations';

Needs.addLinks({
  case: {
    type: 'one',
    collection: Cases,
    field: 'caseId',
  },
});

Donations.addLinks({
  need: {
    type: 'one',
    collection: Needs,
    field: 'needId',
  },
});

Organizations.addReducers({
  logoImge: {
    body: {
      logo: 1,
    },
    reduce(obj) {
      const logo = Files.findOne({ _id: obj.logo, 'meta.type': 'logo' });
      return logo && logo.link();
    },
  },
});

Meteor.users.addReducers({
  userImage: {
    body: {
      profile: 1,
    },
    reduce(obj) {
      const avatar = Files.findOne({ _id: obj.profile && obj.profile.avatar, 'meta.type': 'avatar' });
      return avatar && avatar.link();
    },
  },
});

Cases.addLinks({
  organization: {
    type: 'one',
    collection: Organizations,
    field: 'organizationId',
  },
  tags: {
    type: 'many',
    collection: Categories,
    field: 'categories',
  },
  urgentNeeds: {
    collection: Needs,
    inversedBy: 'case',
  },
});

Categories.addLinks({
  topCategory: {
    type: 'one',
    collection: Categories,
    field: 'parentCategory',
  },
  supCategories: {
    collection: Categories,
    inversedBy: 'topCategory',
  },
});
