import SimpleSchema from 'simpl-schema';
import { ValidatedMethod } from 'meteor/mdg:validated-method';
import { isDonor } from '/imports/api/utils';
import Cases from '../cases/cases';
import Categories from '../categories/categories';


export const getDonorFeed = new ValidatedMethod({
  name: 'feeds.methods.get_donor_feed',
  validate: new SimpleSchema({
    filters: {
      type: Object,
      blackbox: true,
    },
    limit: {
      type: Number,
      optional: true,
      defaultValue: 10,
    },
    skip: {
      type: Number,
      optional: true,
      defaultValue: 0,
    },
    searchKeyword: {
      type: String,
      optional: true,
      defaultValue: '',
    },
    smellerCase: {
      type: SimpleSchema.RegEx.Id,
      optional: true,
    },
  }).validator({ clean: true }),
  run({
    filters, limit, skip, searchKeyword, smellerCase,
  }) {
    if (!isDonor(Meteor.userId())) {
      throw new Meteor.Error(500, 'Not allow to do this action ');
    }
    // search with keyword
    let searchFilter = {};
    if (searchKeyword) {
      searchFilter = {
        $or: [{ title: { $regex: searchKeyword, $options: 'i' } },
          { description: { $regex: searchKeyword, $options: 'i' } }],
      };
    } else if (smellerCase) { // get related cases
      const { categories } = Cases.findOne({ _id: smellerCase });
      searchFilter = {
        _id: { $ne: smellerCase },
        categories: { $in: categories },
      };
    }
    // user preferences
    let interestsQry = {};
    const { preferences } = Meteor.user();
    if (preferences && preferences.interests) {
      interestsQry = { categories: { $in: preferences.interests } };
    }
    // user grapher to prepare the case doc and return it
    const query = Cases.createQuery({
      $filters: {
        ...filters,
        ...searchFilter,
        status: 'approved',
        isRemoved: { $ne: true },
        ...interestsQry,
      },
      $options: { sort: { dueDate: -1 }, limit, skip },
      title: 1,
      description: 1,
      dueDate: 1,
      urgency: 1,
      organization: {
        name: 1,
        logo: 1,
        logoImge: 1,
      },
      urgentNeeds: {
        $filters: {
          status: { $ne: 'fulfilled' },
          isRemoved: { $ne: true },
        },
        $options: { sort: { dueDate: -1 }, limit: 3 },
        title: 1,
        quantityNeeded: 1,
        unitOfMeasure: 1,
        itemPrice: 1,
        quantityFulfilled: 1,
      },
      tags: {
        $filters: { isRemoved: { $ne: true } },
        name: 1,
        icon: 1,
      },
    });
    return query.fetch();
  },
});

export const getDonorCategories = new ValidatedMethod({
  name: 'feeds.methods.get_donor_categories',
  validate: new SimpleSchema({
    filters: {
      type: Object,
      blackbox: true,
    },
    limit: {
      type: Number,
      optional: true,
      defaultValue: 0,
    },
    parentsOnly: {
      type: Boolean,
      defaultValue: false,
    },
  }).validator({ clean: true }),
  run({ filters, limit, parentsOnly }) {
    if (!isDonor(Meteor.userId())) {
      throw new Meteor.Error(500, 'Not allow to do this action ');
    }
    let filterByParents = {};
    if (!parentsOnly) {
      filterByParents = {
        supCategories: {
          name: 1,
          description: 1,
          createdAt: 1,
          icon: 1,
          aliases: 1,
        },
      };
    }
    // user grapher to prepare the cause doc and return it
    const query = Categories.createQuery({
      $filters: {
        ...filters, parentCategory: null, isRemoved: { $ne: true }, categoryType: 'cause',
      },
      $options: { sort: { createdAt: -1 }, limit },
      name: 1,
      description: 1,
      createdAt: 1,
      icon: 1,
      aliases: 1,
      ...filterByParents,
    });

    return query.fetch();
  },
});

// set donor interests in his profile
export const setDonorCategories = new ValidatedMethod({
  name: 'feeds.methods.set_donor_interests',
  validate: new SimpleSchema({
    interests: {
      type: Array,
    },
    'interests.$': {
      type: String,
      regEx: SimpleSchema.RegEx.id,
    },
  }).validator({ clean: true }),
  run({ interests }) {
    Meteor.users.update(Meteor.userId(), {
      $set: {
        'preferences.interests': interests,
      },
    });
  },
});
