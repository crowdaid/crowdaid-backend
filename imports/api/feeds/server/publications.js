import { Meteor } from 'meteor/meteor';
import Cases from '/imports/api/cases/cases';
import { isDonor } from '/imports/api/utils';

Meteor.publish('DonorFeed', function ({
  filter = {}, limit = 10, skip = 0, sort = { _id: -1 },
}) {
  if (!isDonor(this.userId)) return this.ready();
  // TODO: add user interests and check for removed organizations
  const userId = Meteor.userId();
  const { interests } = Meteor.users.findOne({ _id: userId }, { fields: { interests: 1 } });

  const query = { ...filter, isRemoved: { $ne: true } };
  const docsHandle = Cases.find(query,
    {
      skip, limit, sort, fields: { services: 0, mergedServices: 0, onBoarding: 0 },
    }).observeChanges({
    added: (id, entry) => {
      this.added('cases', id, entry);
    },
    changed: (id, entry) => {
      this.changed('cases', id, entry);
    },
    removed: (id, entry) => {
      this.removed('cases', id, entry);
    },
  });

  this.ready();

  // Stop observing the cursor when the client unsubscribes. Stopping a
  // subscription automatically takes care of sending the client any `removed`
  // messages.
  this.onStop(() => {
    docsHandle.stop();
  });
});
