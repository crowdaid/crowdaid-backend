import { Meteor } from 'meteor/meteor';

import SimpleSchema from 'simpl-schema';

SimpleSchema.extendOptions(['autoform']);

const commonData = new SimpleSchema({
  userId: {
    type: SimpleSchema.RegEx.Id,
    defaultValue() {
      return Meteor.userId();
    },
  },

  // Force value to be current date (on server) upon insert
  // and prevent updates thereafter.
  createdAt: {
    type: Date,
    autoValue() {
      if (this.isInsert) {
        return new Date();
      } if (this.isUpsert) {
        return { $setOnInsert: new Date() };
      }
      this.unset(); // Prevent user from supplying their own value
    },
  },
  // Force value to be current date (on server) upon update
  // and don't allow it to be set upon insert.
  updatedAt: {
    type: Date,
    autoValue() {
      if (this.isUpdate) {
        return new Date();
      }
    },
    denyInsert: true,
    optional: true,
  },
  isRemoved: {
    type: Boolean,
    defaultValue: false,
  },
  removedAt: {
    type: Date,
    optional: true,
  },
});

export default commonData;
