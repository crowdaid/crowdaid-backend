// Meteor core imports
import { Mongo } from 'meteor/mongo';
import { Tracker } from 'meteor/tracker';

// node imports
import SimpleSchema from 'simpl-schema';

// internal imports
import commonData from '/imports/api/sharedSchemas';

const caseNotes = new Mongo.Collection('casesNotes');

SimpleSchema.extendOptions(['autoform']);
caseNotes.schema = new SimpleSchema({
  _id: {
    type: String,
    regEx: SimpleSchema.RegEx.Id,
  },
  caseId: {
    type: String,
    regEx: SimpleSchema.RegEx.Id,
  },
  description: {
    type: String,
  },
  title: {
    type: String,
    optional: true,
  },
}, { trimStrings: true, tracker: Tracker });

caseNotes.schema.extend(commonData);

caseNotes.attachSchema(caseNotes.schema);

// Deny all client-side updates since we will be using methods to manage this collection
caseNotes.deny({
  insert() {
    return true;
  },
  update() {
    return true;
  },
  remove() {
    return true;
  },
});

export default caseNotes;
