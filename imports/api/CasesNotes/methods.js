import { ValidatedMethod } from 'meteor/mdg:validated-method';
import CasesNotes from './caseNotes';
import { Authorize } from '/imports/api/utils';

export const createCaseNote = new ValidatedMethod({
  name: 'casesNotes.methods.create',
  validate: CasesNotes.schema.pick('description', 'caseId').validator({ clean: true }),
  run({
    description, caseId,
  }) {
    Authorize(['owner', 'employee']);
    const { firstName, lastName } = Meteor.user().profile;
    const title = `${firstName} ${lastName}`;
    return CasesNotes.insert({
      description: description.trim(),
      caseId,
      userId: Meteor.userId(),
      title,
    });
  },
});

export const updateCaseNotes = new ValidatedMethod({
  name: 'casesNotes.methods.update',
  validate: CasesNotes.schema.pick('_id', 'description').validator({ clean: true }),
  run({
    _id, name,
  }) {
    Authorize(['owner', 'employee']);
    if (CasesNotes.findOne({ _id: { $ne: _id }, name: name.trim() })) {
      throw new Meteor.Error(400, 'Need Type Name should be unique.');
    }

    CasesNotes.update(_id, {
      $set: {
        name,
      },
    });
  },
});
