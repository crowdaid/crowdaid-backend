import { Meteor } from 'meteor/meteor';
import CasesNotes from '../caseNotes';
import { isStuff, isOwner } from '/imports/api/utils';

Meteor.publish('CaseNotesAll', function ({
  filter = {}, limit = 10, skip = 0, sort = { _id: -1 }, caseId,
}) {
  if (!isStuff(this.userId) && !isOwner(this.userId)) return this.ready();
  let count = 0;
  let initializing = true;

  const countHandle = CasesNotes.find({ ...filter, caseId, isRemoved: { $ne: true } })
    .observeChanges({
      added: (id) => {
        count += 1;
        if (!initializing) {
          this.changed('CollectionsMeta', 'CasesNotes', { count });
        }
      },
      removed: (id) => {
        count -= 1;
        this.changed('CollectionsMeta', 'CasesNotes', { count });
      },
    });
  // Instead, we'll send one `added` message right after `observeChanges` has
  // returned, and mark the subscription as ready.
  this.added('CollectionsMeta', 'CasesNotes', { count });
  const docsHandle = CasesNotes.find({ ...filter, caseId, isRemoved: { $ne: true } },
    {
      skip, limit, sort, fields: { services: 0, mergedServices: 0, onBoarding: 0 },
    }).observeChanges({
    added: (id, entry) => {
      this.added('casesNotes', id, entry);
    },
    changed: (id, entry) => {
      this.changed('casesNotes', id, entry);
    },
    removed: (id, entry) => {
      this.removed('casesNotes', id, entry);
    },
  });

  initializing = false;
  this.ready();

  // Stop observing the cursor when the client unsubscribes. Stopping a
  // subscription automatically takes care of sending the client any `removed`
  // messages.
  this.onStop(() => {
    countHandle.stop();
    docsHandle.stop();
  });
});
