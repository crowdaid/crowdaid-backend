// Meteor core imports
import { Mongo } from 'meteor/mongo';
import { Tracker } from 'meteor/tracker';

// node imports
import SimpleSchema from 'simpl-schema';

// internal imports
import commonData from '/imports/api/sharedSchemas';
import {
  caseStatuses, urgencyFlags,
} from '/imports/api/commonValues';

const Cases = new Mongo.Collection('cases');


SimpleSchema.extendOptions(['autoform']);
Cases.schema = new SimpleSchema({
  _id: {
    type: String,
    regEx: SimpleSchema.RegEx.Id,
  },
  title: {
    type: String,
  },
  description: {
    type: String,
  },
  status: {
    type: String,
    allowedValues: caseStatuses,
    defaultValue: 'draft',
  },
  caseCode: {
    type: String,
  },
  caseLocation: {
    type: Object,
    required: true,
  },
  'caseLocation.address': {
    type: String,
    required: true,
  },
  'caseLocation.location': {
    type: Object,
    required: true,
  },
  'caseLocation.location.type': {
    type: String,
    allowedValues: ['Point'],
    required: true,
  },
  'caseLocation.location.coordinates': {
    type: Array,
    maxCount: 2,
    minCount: 2,
    required: true,
  },
  'caseLocation.location.coordinates.$': {
    type: Number,
    required: true,
  },
  categories: {
    type: Array,
  },
  'categories.$': {
    type: String,
    regEx: SimpleSchema.RegEx.Id,
  },
  responsible: {
    type: SimpleSchema.RegEx.Id,
  },
  researcher: {
    type: SimpleSchema.RegEx.Id,
  },
  beneficiary: {
    optional: true,
    type: SimpleSchema.RegEx.Id,
  },
  organizationId: {
    type: String,
    regEx: SimpleSchema.RegEx.Id,
    optional: true,
  },
  urgency: {
    type: String,
    allowedValues: urgencyFlags,
    defaultValue: 'N/A',
  },
  dueDate: {
    type: Date,
    optional: true,
  },
  attachments: {
    type: Array,
    optional: true,
  },
  'attachments.$': {
    type: String,
    regEx: SimpleSchema.RegEx.Id,
  },
  // reports should be refactored to its own collection
  reports: {
    type: Array,
    optional: true,
  },
  'reports.$': {
    type: String,
    regEx: SimpleSchema.RegEx.Id,
  },

  // commenting changelog for now, as it will need a huge work for cases
  // Whenever the text, quantity field is updated, automatically
  // update a history array.
  // changeLog: {
  //     type: Array,
  //     optional: true,
  //     autoValue: function() {
  //         let text = this.field('text');
  //         let quantity = this.field('quantity');
  //
  //         // only tracking text changes for now
  //         if (text.isSet) {
  //             if (this.isInsert) {
  //                 return [{
  //                     createdBy: Meteor.userId(),
  //                     createdAt: new Date(),
  //                     text: text.value
  //                 }];
  //             } else {
  //                 return {
  //                     $push: {
  //                         createdBy: Meteor.userId(),
  //                         createdAt: new Date,
  //                         text: text.value
  //                     }
  //                 };
  //             }
  //         } else {
  //             this.unset();
  //         }
  //     },
  // },
  // 'changeLog.$': {
  //     type: Object,
  // },
  // 'changeLog.$.createdBy': {
  //     type: SimpleSchema.RegEx.Id,
  // },
  // 'changeLog.$.createdAt': {
  //     type: Date
  // },
  // 'changeLog.$.text': {
  //     type: String
  // },
}, { trimStrings: true, tracker: Tracker });

Cases.schema.extend(commonData);

Cases.attachSchema(Cases.schema);

Cases.rawCollection()
  .createIndex({ title: 'text', description: 'text' })
  .then()
  .catch((err) => {});

// Deny all client-side updates since we will be using methods to manage this collection
Cases.deny({
  insert() {
    return true;
  },
  update() {
    return true;
  },
  remove() {
    return true;
  },
});

export default Cases;
