import { Meteor } from 'meteor/meteor';
import Cases from '/imports/api/cases/cases';
import beneficiaries from '/imports/api/beneficiaries/beneficiaries';
import {
  Authorize, isAdmin, isOwner, isEmployee,
} from '/imports/api/utils';
import Needs from '../../needs/needs';

Meteor.publish('CasesTabular', function ({
  filter = {}, limit = 10, skip = 0, sort = { _id: -1 },
}) {
  if (!isOwner(this.userId) && !isEmployee(this.userId)) return this.ready();
  let count = 0;
  let initializing = true;

  const userId = Meteor.userId();
  const { organizationId } = Meteor.users.findOne({ _id: userId }, { fields: { organizationId: 1 } });

  const query = { ...filter, isRemoved: { $ne: true }, organizationId };
  const countHandle = Cases.find(query)
    .observeChanges({
      added: (id) => {
        count += 1;
        if (!initializing) {
          this.changed('CollectionsMeta', 'Cases', { count });
        }
      },
      removed: (id) => {
        count -= 1;
        this.changed('CollectionsMeta', 'Cases', { count });
      },
    });
    // Instead, we'll send one `added` message right after `observeChanges` has
    // returned, and mark the subscription as ready.
  this.added('CollectionsMeta', 'Cases', { count });
  const docsHandle = Cases.find(query,
    {
      skip, limit, sort, fields: { services: 0, mergedServices: 0, onBoarding: 0 },
    }).observeChanges({
    added: (id, entry) => {
      this.added('cases', id, entry);
    },
    changed: (id, entry) => {
      this.changed('cases', id, entry);
    },
    removed: (id, entry) => {
      this.removed('cases', id, entry);
    },
  });

  initializing = false;
  this.ready();

  // Stop observing the cursor when the client unsubscribes. Stopping a
  // subscription automatically takes care of sending the client any `removed`
  // messages.
  this.onStop(() => {
    countHandle.stop();
    docsHandle.stop();
  });
});

Meteor.publish('cases.needs', function ({
  filter = {}, limit = 10, skip = 0, sort = { _id: -1 }, caseId,
}) {
  if (!isOwner(this.userId) && !isEmployee(this.userId) && !isAdmin(this.userId)) return this.ready();
  let count = 0;
  let initializing = true;

  const userId = Meteor.userId();
  const query = {
    ...filter, isRemoved: { $ne: true }, caseId,
  };

  if (!isAdmin(this.userId)) {
    const { organizationId } = Meteor.users.findOne({ _id: userId }, { fields: { organizationId: 1 } });
    query.organizationId = organizationId;
  }

  const countHandle = Needs.find(query)
    .observeChanges({
      added: (id) => {
        count += 1;
        if (!initializing) {
          this.changed('CollectionsMeta', 'Needs', { count });
        }
      },
      removed: (id) => {
        count -= 1;
        this.changed('CollectionsMeta', 'Needs', { count });
      },
    });
    // Instead, we'll send one `added` message right after `observeChanges` has
    // returned, and mark the subscription as ready.
  this.added('CollectionsMeta', 'Needs', { count });
  const docsHandle = Needs.find(query,
    {
      skip, limit, sort, fields: { services: 0, mergedServices: 0, onBoarding: 0 },
    }).observeChanges({
    added: (id, entry) => {
      this.added('needs', id, entry);
    },
    changed: (id, entry) => {
      this.changed('needs', id, entry);
    },
    removed: (id, entry) => {
      this.removed('needs', id, entry);
    },
  });

  initializing = false;
  this.ready();

  // Stop observing the cursor when the client unsubscribes. Stopping a
  // subscription automatically takes care of sending the client any `removed`
  // messages.
  this.onStop(() => {
    countHandle.stop();
    docsHandle.stop();
  });
});


Meteor.publish('cases.users', function (casee) {
  if (!isAdmin(this.userId)) return this.ready();
  return Meteor.users.find({ _id: { $in: [casee.responsible, casee.researcher, casee.beneficiary] } });
});

Meteor.publish('cases.all', function () {
  if (!isAdmin(this.userId)) return this.ready();
  return Cases.find({});
});

Meteor.publish('org.cases', function (orgId) {
  if (!isAdmin(this.userId)) return this.ready();
  return Cases.find({ organizationId: orgId });
});

Meteor.publish('cases.single', function (caseId) {
  if (!this.userId) return this.ready();
  const bCase = Cases.findOne({ _id: caseId });
  return [
    Cases.find({ _id: caseId }),
    beneficiaries.find({ _id: bCase.beneficiary }),
  ];
});

Meteor.publish('cases.feed', function () {
  if (!this.userId) return this.ready();
  return Cases.find();
});

Meteor.publish('cases.user', function (userId, caseIds) {
  if (!this.userId || this.userId !== userId) return this.ready();
  return Cases.find({ _id: { $in: caseIds } });
});
