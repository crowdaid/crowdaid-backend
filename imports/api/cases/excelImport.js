import { ValidatedMethod } from 'meteor/mdg:validated-method';
import SimpleSchema from 'simpl-schema';
import { Meteor } from 'meteor/meteor';
import { Random } from 'meteor/random';
import XLSX from 'xlsx';
import { generateCaseCode } from './methods';
import { createCategory } from '../categories/methods';
import Categories from '../categories/categories';
import Beneficiaries from '../beneficiaries/beneficiaries';
import Cases from './cases';
import Organizations from '../organizations/organizations';

const checkFiletype = (fileName) => {
  const ext = fileName.split('.')[1];
  if (ext !== 'xlsx') {
    throw new Meteor.Error(400, 'File must be excle file with \'xlsx\' extension ');
  }
};

const checkValidFiles = (wb) => {
  const sheetNames = wb.SheetNames.map(sh => sh.toLowerCase());
  if (sheetNames.length !== 2) {
    throw new Meteor.Error(400, 'Excel file must be having two sheets');
  } else if (!sheetNames.includes('beneficiaries')) {
    throw new Meteor.Error(400, 'Excel file must be having Beneficiaries sheet');
  } else if (!sheetNames.includes('cases')) {
    throw new Meteor.Error(400, 'Excel file must be having Cases sheet');
  }
  const beneficiaries = XLSX.utils.sheet_to_json(wb.Sheets[wb.SheetNames[sheetNames.indexOf('beneficiaries')]]);
  const cases = XLSX.utils.sheet_to_json(wb.Sheets[wb.SheetNames[sheetNames.indexOf('cases')]]);
  if (beneficiaries.length !== cases.length) {
    throw new Meteor.Error(400, 'The cases rows numbers not match beneficiaries number');
  }
  return sheetNames;
};

const createDemoCauses = (orgId) => {
  Categories.remove({ name: 'Demo' });
  const cause = createCategory.call({
    name: 'Demo',
    description: 'This is demo cause',
    aliases: 'demo',
    categoryType: 'cause',
    icon: {
      class: 'icon-heart',
      id: '67237f08ba6c85567ded98562d532282',
    },
  });
  if (orgId) {
    Organizations.update({ _id: orgId }, {
      $addToSet: {
        categories: cause,
      },
    });
  }
  return cause;
};

const formatAndInsertBensData = (wb, orgId) => {
  let bensData = [];
  const sheetNames = checkValidFiles(wb);
  try {
    // get beneficiaries data from excel sheet
    const beneficiaries = XLSX.utils.sheet_to_json(wb.Sheets[wb.SheetNames[sheetNames.indexOf('beneficiaries')]]);
    // delete header
    beneficiaries.splice(0, 1);
    if (beneficiaries.length === 0) throw new Meteor.Error(400, 'The beneficiaries sheet is empty');

    // get beneficiaries data from excel sheet
    const user = Meteor.user();
    // format case doc
    bensData = beneficiaries.map(ben => ({
      _id: Random.id(),
      name: {
        ar: ben['name.ar'],
        en: ben['name.en'] || ben['name.ar'],
      },
      about: ben.about,
      address: ben.address || 'Cairo, Egypt',
      nationalIdNumber: ben.nationalIdNumber,
      gender: ben.gender,
      type: 'family',
      areaType: ben.areaType,
      verificationStatus: 'verified',
      birthDate: new Date(ben.birthDate),
      verifiedBy: user && user._id,
      orgId: orgId || (user && user.organizationId) || '6HHjzL6Fn8rPoqMCn',
    }));
  } catch (error) {
    throw new Meteor.Error(400, 'The excel file data is not in the required format', { error });
  }
  return Beneficiaries.rawCollection().insertMany(bensData);
};

const formatAndInsertCasesData = (wb, beneficiaries, orgId) => {
  let casesData = [];
  const sheetNames = checkValidFiles(wb);
  try {
    // get cases data from excel sheet
    const cases = XLSX.utils.sheet_to_json(wb.Sheets[wb.SheetNames[sheetNames.indexOf('cases')]]);
    // delete header
    cases.splice(0, 1);
    if (cases.length === 0) throw new Meteor.Error(400, 'The cases sheet is empty');

    // get beneficiaries data from excel sheet
    const user = Meteor.user();
    const demoCause = createDemoCauses(orgId || (user && user.organizationId));
    // format case doc
    casesData = cases.map((c, i) => ({
      _id: Random.id(),
      title: c.title,
      description: c.description,
      status: 'approved',
      caseCode: generateCaseCode(),
      caseLocation: {
        address: c.address || 'Cairo, Egypt',
        location: {
          type: 'Point',
          coordinates: [30.33, 31.55],
        },
      },
      categories: [demoCause],
      dueDate: new Date(c.dueDate),
      responsible: user && user._id,
      researcher: user && user._id,
      beneficiary: beneficiaries[i],
      urgency: 'N/A',
      organizationId: orgId || (user && user.organizationId) || '6HHjzL6Fn8rPoqMCn',
    }));
  } catch (error) {
    throw new Meteor.Error(400, 'The excel file data is not in the required format', { error });
  }
  return Cases.rawCollection().insertMany(casesData);
};

export const generateFromExcelS = new ValidatedMethod({
  name: 'cases.methods.generateFromExcelS',
  validate: new SimpleSchema({
    orgId: {
      type: SimpleSchema.RegEx.Id,
      optional: true,
    },
    data: {
      type: String,
    },
    name: {
      type: String,
    },
  }).validator({ clean: true }),
  run({ orgId, data, name }) {
    checkFiletype(name);
    const wb = XLSX.read(data, { type: 'binary' });
    return formatAndInsertBensData(wb, orgId).then((bens) => {
      if (!bens.result.ok) {
        throw new Meteor.Error(500, 'Something went wrong in insert data');
      } else {
        const { insertedIds } = bens;
        return formatAndInsertCasesData(wb, insertedIds, orgId);
      }
    });
  },
});

export const generateFromExcelU = new ValidatedMethod({
  name: 'cases.methods.generateFromExcelU',
  validate: new SimpleSchema({
    orgId: {
      type: SimpleSchema.RegEx.Id,
      optional: true,
    },
    data: {
      type: Uint8Array,
    },
    name: {
      type: String,
    },
  }).validator({ clean: true }),
  run({ orgId, data, name }) {
    checkFiletype(name);
    const wb = XLSX.read(data, { type: 'array' });
    return formatAndInsertBensData(wb, orgId).then((bens) => {
      if (!bens.result.ok) {
        throw new Meteor.Error(500, 'Something went wrong in insert data');
      } else {
        const { insertedIds } = bens;
        return formatAndInsertCasesData(wb, insertedIds, orgId);
      }
    });
  },
});
