import { Meteor } from 'meteor/meteor';
import SimpleSchema from 'simpl-schema';
import { ValidatedMethod } from 'meteor/mdg:validated-method';
// import { Notifications } from 'meteor/meteorize:notifications';
import Cases from '/imports/api/cases/cases';
import Donations from '/imports/api/donations/donations';
import { Authorize } from '/imports/api/utils';
import { caseStatuses } from '/imports/api/commonValues';
import Needs from '../needs/needs';
import Beneficiaries from '../beneficiaries/beneficiaries';

const shortid = require('shortid');

// generate case unique code number
export const generateCaseCode = () => {
  shortid.characters('0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ#$');
  const caseCode = shortid.generate();
  return caseCode;
};

export const createCase = new ValidatedMethod({
  name: 'cases.methods.create',
  validate: Cases.schema.pick(
    'title',
    'description',
    'caseLocation',
    'categories',
    'urgency',
    'status',
    'beneficiary',
    'dueDate',
  ).validator({ clean: true }),
  run({
    title,
    description,
    caseLocation,
    categories,
    beneficiary,
    urgency,
    status,
    dueDate,
  }) {
    Authorize(['owner', 'employee']);

    const { organizationId } = Meteor.users.findOne({ _id: this.userId }, {
      fields: { organizationId: 1 },
    });
    // update beneficiary address
    Beneficiaries.update({ _id: beneficiary }, {
      $set: {
        address: caseLocation.address,
      },
    });
    // generate Case code number
    const caseCode = generateCaseCode();


    return Cases.insert({
      title,
      description,
      caseLocation,
      categories,
      caseCode,
      urgency,
      dueDate,
      status,
      beneficiary,
      organizationId,
      responsible: this.userId,
      researcher: this.userId,
    });
  },
});

export const updateCase = new ValidatedMethod({
  name: 'cases.methods.update',
  validate: Cases.schema.pick(
    '_id',
    'title',
    'description',
    'caseLocation',
    'categories',
    'beneficiary',
    'status',
    'urgency',
    'dueDate',
    'attachments',
  ).validator({ clean: true }),
  run({
    _id,
    title,
    description,
    caseLocation,
    categories,
    beneficiary,
    urgency,
    status,
    dueDate,
    attachments,
  }) {
    Authorize(['owner', 'employee']);

    const owner = Meteor.users.findOne({ _id: this.userId }, { fields: { organizationId: 1 } });
    const Case = Cases.findOne({ _id });

    if (owner.organizationId !== Case.organizationId) throw new Meteor.Error(401, 'You are not allowed to access this case.');
    // update beneficiary address
    Beneficiaries.update({ _id: beneficiary }, {
      $set: {
        address: caseLocation.address,
      },
    });
    return Cases.update(_id, {
      $set: {
        title,
        description,
        caseLocation,
        categories,
        status,
        beneficiary,
        urgency,
        dueDate,
        attachments,
      },
    });
  },
});
export const removeNeed = new ValidatedMethod({
  name: 'cases.needs.remove',
  validate: new SimpleSchema({
    caseId: { type: SimpleSchema.RegEx.Id },
    needId: { type: SimpleSchema.RegEx.Id },
  }).validator({ clean: true }),
  run({
    caseId,
    needId,
  }) {
    Authorize(['employee', 'owner']);
    Needs.update(needId, {
      $pull: {
        cases: caseId,
      },
    });
    Cases.update(caseId, {
      $pull: {
        needs: needId,
      },
    });
  },
});

// this won't actually remove it, it will only flip the isRemoved flag
export const removeCase = new ValidatedMethod({
  name: 'cases.methods.remove',
  validate: new SimpleSchema({
    caseId: {
      type: SimpleSchema.RegEx.Id,
    },
  }).validator({ clean: true }),
  run({ caseId }) {
    Authorize(['owner', 'employee']);
    Cases.update({ _id: caseId }, { $set: { isRemoved: true, removedAt: new Date() } });
    Needs.update({ caseId },
      { $set: { isRemoved: true, removedAt: new Date() } },
      { multi: true });
    Donations.update({ caseId },
      { $set: { isRemoved: true, removedAt: new Date() } },
      { multi: true });
  },
});

export const setCaseStatus = new ValidatedMethod({
  name: 'cases.methods.setStatus',
  validate: new SimpleSchema({
    status: {
      type: String,
      allowedValues: caseStatuses,
    },
    caseId: { type: SimpleSchema.RegEx.Id },
  }).validator({ clean: true }),
  run({ caseId, status }) {
    Authorize(['owner']);
    Cases.update(caseId, { $set: { status } });
  },
});

export const addAttachment = new ValidatedMethod({
  name: 'cases.methods.addAttachment',
  validate: new SimpleSchema({
    caseId: {
      type: SimpleSchema.RegEx.Id,
    },
    fileId: {
      type: SimpleSchema.RegEx.Id,
    },
  }).validator({ clean: true }),
  run({ caseId, fileId }) {
    Cases.update(caseId, { $addToSet: { attachments: fileId } });
  },
});

export const caseDetails = new ValidatedMethod({
  name: 'cases.methods.caseDetails',
  validate: new SimpleSchema({
    caseId: {
      type: SimpleSchema.RegEx.Id,
    },
  }).validator({ clean: true }),
  run({ caseId }) {
    // user grapher to prepare the case doc and return it
    const query = Cases.createQuery({
      $filters: {
        _id: caseId,
      },
      title: 1,
      description: 1,
      dueDate: 1,
      urgency: 1,
      organization: {
        name: 1,
        logo: 1,
        logoImge: 1,
      },
      urgentNeeds: {
        $filters: {
          status: { $ne: 'fulfilled' },
          isRemoved: { $ne: true },
        },
        $options: { sort: { dueDate: -1 }, limit: 3 },
        title: 1,
        quantityNeeded: 1,
        unitOfMeasure: 1,
        itemPrice: 1,
        quantityFulfilled: 1,
      },
      tags: {
        $filters: { isRemoved: { $ne: true } },
        name: 1,
        icon: 1,
      },
    });
    return query.fetchOne();
  },
});
