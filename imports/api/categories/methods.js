import SimpleSchema from 'simpl-schema';
import { ValidatedMethod } from 'meteor/mdg:validated-method';
import Organizations from '../organizations/organizations';
import Cases from '../cases/cases';
import Categories from '/imports/api/categories/categories';
import { Authorize } from '/imports/api/utils';
import Needs from '../needs/needs';
import Donations from '../donations/donations';

export const createCategory = new ValidatedMethod({
  name: 'categories.methods.create',
  validate: Categories.schema.pick('name', 'description',
    'aliases', 'categoryType',
    'icon', 'parentCategory').validator({ clean: true }),
  run({
    name, description, aliases, parentCategory, icon, categoryType,
  }) {
    Authorize(['admin', 'owner', 'employee', 'donor']);
    if (Categories.findOne({ name: name.trim() })) {
      throw new Meteor.Error(400, 'Category Name should be unique.');
    }
    // we should check for permessions
    return Categories.insert({
      name,
      description,
      aliases,
      parentCategory,
      categoryType,
      icon,
    });
  },
});

export const updateCategory = new ValidatedMethod({
  name: 'categories.methods.update',
  validate: Categories.schema.pick('_id', 'name',
    'categoryType', 'description',
    'aliases', 'icon', 'parentCategory').validator({ clean: true }),
  run({
    _id, name, description, aliases, parentCategory, icon, categoryType,
  }) {
    Authorize('admin');
    if (Categories.findOne({ _id: { $ne: _id }, name: name.trim() })) {
      throw new Meteor.Error(400, 'Category Name should be unique.');
    }
    // we should check for permessions
    Categories.update(_id, {
      $set: {
        name,
        description,
        aliases,
        categoryType,
        parentCategory,
        icon,
      },
    });
  },
});


// this won't actually remove it, it will only alter the isRemoved flag
export const removeCategory = new ValidatedMethod({
  name: 'categories.methods.remove',
  validate: Categories.schema.pick('_id').validator({ clean: true }),
  run({ _id }) {
    Authorize('admin');
    const category = Categories.findOne({ _id });

    // check if needs uses this category, as it's a requied  filed
    if (category.categoryType === 'item' || category.categoryType === 'needType') {
      const count = Needs.find({ $or: [{ needTypeId: _id }, { itemId: _id }] }).count();
      if (count) throw new Meteor.Error(400, 'This category is currently used by a need, use merge instead');
    }

    // remove the category from all who use it
    if (!category) return;
    Cases.update({}, {
      $pull: {
        categories: _id,
      },
    }, { multi: true });

    Organizations.update({ }, {
      $pull: {
        categories: _id,
      },
    }, { multi: true });

    // remove all it's sub categories
    const children = Categories.find({ parentCategory: _id }).fetch();
    Categories.remove(_id);
    if (!children.length) return;

    children.forEach((c, i) => {
      removeCategory.call({ _id: c._id });
    });
  },
});


export const searchCats = new ValidatedMethod({
  name: 'categories.methods.search',
  validate: new SimpleSchema({
    keyword: {
      type: String,
    },
    id: {
      type: String,
      optional: true,
      regEx: SimpleSchema.RegEx.Id,
    },
  }).validator({ clean: true }),
  run({ keyword, id }) {
    Authorize(['admin', 'owner', 'employee']);
    const idfilter = {};
    if (id) {
      idfilter._id = { $ne: id };
    }
    const regex = new RegExp(`${keyword}`);
    const match = Categories.find({
      ...idfilter,
      $or: [{ name: { $regex: regex, $options: 'igm' } },
        { aliases: { $regex: regex, $options: 'igm' } }],
    }, { fields: { name: 1, categoryType: 1 } });
    return match.fetch();
  },
});

export const mergeCats = new ValidatedMethod({
  name: 'categories.methods.merge_cats',
  validate: new SimpleSchema({
    mergedCatId: {
      type: String,
    },
    _id: {
      type: String,
      regEx: SimpleSchema.RegEx.Id,
    },
  }).validator({ clean: true }),
  run({ mergedCatId, _id }) {
    Authorize('admin');
    const category = Categories.findOne({ _id: mergedCatId });
    if (!category) {
      throw new Meteor.Error(500, 'Something went wrong try later');
    }
    if (!category.aliases) {
      category.aliases = [];
    }
    Categories.update({ _id }, {
      $addToSet: {
        aliases: { $each: [category.name, ...category.aliases] },
      },
    });
    if (category.categoryType === 'item') {
      Needs.update({ itemId: mergedCatId }, {
        $set: {
          itemId: _id,
        },
      }, { multi: true });
      Donations.update({ itemId: mergedCatId }, {
        $set: {
          itemId: _id,
        },
      }, { multi: true });
    } else if (category.categoryType === 'needType') {
      Needs.update({ needTypeId: mergedCatId }, {
        $set: {
          needTypeId: _id,
        },
      }, { multi: true });
    }
    removeCategory.call({ _id: mergedCatId });
  },
});
