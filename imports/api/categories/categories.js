// Meteor core imports
import { Mongo } from 'meteor/mongo';
import { Tracker } from 'meteor/tracker';

// node imports
import SimpleSchema from 'simpl-schema';
import slug from 'slug';

// internal imports
import commonData from '/imports/api/sharedSchemas';

const Categories = new Mongo.Collection('categories');

SimpleSchema.extendOptions(['autoform']);
Categories.schema = new SimpleSchema({
  _id: {
    type: String,
    regEx: SimpleSchema.RegEx.Id,
  },
  name: {
    type: String,
  },
  slug: {
    type: String,
    autoValue() {
      if (this.field('name').isSet) return slug(this.field('name').value, { lower: true });
    },
  },
  description: {
    type: String,
    optional: true,
  },
  parentCategory: {
    type: String,
    regEx: SimpleSchema.RegEx.Id,
    optional: true,
  },
  categoryType: {
    type: String,
    allowedValues: ['cause', 'needType', 'item'],
    defaultValue: 'cause',
  },
  aliases: {
    type: Array,
    optional: true,
  },
  'aliases.$': {
    type: String,
  },
  icon: {
    type: Object,
    optional: true,
  },
  'icon.id': {
    type: String,
  },
  'icon.class': {
    type: String,
  },
}, { trimStrings: true, tracker: Tracker });

Categories.schema.extend(commonData);

Categories.attachSchema(Categories.schema);


// Deny all client-side updates since we will be using methods to manage this collection
Categories.deny({
  insert() {
    return true;
  },
  update() {
    return true;
  },
  remove() {
    return true;
  },
});

export default Categories;
