import { Meteor } from 'meteor/meteor';
import Categories from '/imports/api/categories/categories';
import { isAdmin, Authorize } from '/imports/api/utils';
import Organizations from '../../organizations/organizations';

Meteor.publish('CategoriesTabular', function ({
  filter = {}, limit = 10, skip = 0, sort = { _id: -1 },
}) {
  if (!isAdmin(this.userId)) return this.ready();
  let count = 0;
  let initializing = true;

  const countHandle = Categories.find({ ...filter, isRemoved: { $ne: true } })
    .observeChanges({
      added: (id) => {
        count += 1;
        if (!initializing) {
          this.changed('CollectionsMeta', 'Categories', { count });
        }
      },
      removed: (id) => {
        count -= 1;
        this.changed('CollectionsMeta', 'Categories', { count });
      },
    });
  // Instead, we'll send one `added` message right after `observeChanges` has
  // returned, and mark the subscription as ready.
  this.added('CollectionsMeta', 'Categories', { count });
  const docsHandle = Categories.find({ ...filter, isRemoved: { $ne: true } },
    {
      skip, limit, sort, fields: { services: 0, mergedServices: 0, onBoarding: 0 },
    }).observeChanges({
    added: (id, entry) => {
      this.added('categories', id, entry);
    },
    changed: (id, entry) => {
      this.changed('categories', id, entry);
    },
    removed: (id, entry) => {
      this.removed('categories', id, entry);
    },
  });

  initializing = false;
  this.ready();

  // Stop observing the cursor when the client unsubscribes. Stopping a
  // subscription automatically takes care of sending the client any `removed`
  // messages.
  this.onStop(() => {
    countHandle.stop();
    docsHandle.stop();
  });
});


Meteor.publish('categories.all', function (qrys = {}, filters = {}) {
  if (!this.userId) return this.ready();
  return Categories.find({ ...qrys, isRemoved: { $ne: true } }, filters);
});

Meteor.publish('categories.org', function (orgId) {
  if (!this.userId) return this.ready();
  const { organizationId } = Meteor.user();

  const org = Organizations.findOne({ _id: orgId || organizationId });
  return Categories.find({ _id: { $in: org.categories } });
});

Meteor.publish('categories.case', function (casee) {
  if (!Authorize(['admin', `manage-org-${casee.owner.id}`], true)) return this.ready();
  return Categories.find({ _id: { $in: casee.categories } });
});

Meteor.publish('categories.single', function (id) {
  if (!this.userId) return this.ready();
  return Categories.find({ _id: id });
});
