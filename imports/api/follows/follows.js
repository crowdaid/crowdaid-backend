// Meteor core imports
import { Mongo } from 'meteor/mongo';
import { Tracker } from 'meteor/tracker';

// node imports
import SimpleSchema from 'simpl-schema';

// internal imports
import commonData from '/imports/api/sharedSchemas';
import { followeeTypes } from '/imports/api/commonValues';

const Follows = new Mongo.Collection('follows');

SimpleSchema.extendOptions(['autoform']);
Follows.schema = new SimpleSchema({
  followee: {
    type: Object,
  },
  'followee.type': {
    type: String,
    allowedValues: followeeTypes,
  },
  'followee.id': {
    type: SimpleSchema.RegEx.Id,
  },
}, { trimStrings: true, tracker: Tracker });

Follows.schema.extend(commonData);

Follows.attachSchema(Follows.schema);


// Deny all client-side updates since we will be using methods to manage this collection
Follows.deny({
  insert() {
    return true;
  },
  update() {
    return true;
  },
  remove() {
    return true;
  },
});

export default Follows;
