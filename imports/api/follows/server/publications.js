import { Meteor } from 'meteor/meteor';

import Follows from '/imports/api/follows/follows';

Meteor.publish('follows.all', function () {
  if (!this.userId) return this.ready();
  return Follows.find({});
});
