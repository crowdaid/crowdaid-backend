import SimpleSchema from 'simpl-schema';
import { ValidatedMethod } from 'meteor/mdg:validated-method';

import Needs from '/imports/api/needs/needs';
import { userCheck } from '/imports/api/utils';

export const createNeed = new ValidatedMethod({
  name: 'needs.methods.create',
  validate: Needs.schema.pick(
    'caseId',
    'description',
    'dueDate',
    'userId',
  ).validator({ clean: true }),
  run({
    caseId,
    description,
    dueDate,
    userId,
  }) {
    userCheck(userId, 'needs.methods.create');

    // we should check for permessions later
    Needs.insert({
      caseId,
      description,
      dueDate,
      userId,
    });
  },
});

// this won't actually remove it, it will only flip the isRemoved flag
export const removeNeed = new ValidatedMethod({
  name: 'needs.methods.remove',
  validate: new SimpleSchema({
    needId: {
      type: SimpleSchema.RegEx.Id,
    },
    userId: {
      type: SimpleSchema.RegEx.Id,
    },
  }).validator({ clean: true }),
  run({ needId, userId }) {
    userCheck(userId, 'needs.methods.remove');

    // we should check user ownership/admin to be able to let him do this

    // we should check for permessions later

    // we should check if the attribute already isActive: false

    Needs.update({ _id: needId }, { $set: { isRemoved: true, removedAt: new Date() } });
  },
});
