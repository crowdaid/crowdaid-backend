/* eslint-disable prefer-destructuring */
import { Email } from 'meteor/email';

// reinvite admin html
const adminReenrolledHtml = {
  subject: 'CrowdAid Admin enrolled',
  html: `
        <h1> CrowdAid Admin </h1>
        <h3> You are back to be an active admin in <b>CrowdAid</b> </h3>
        <p> Now you can login with your account to CrowdAid admin panel </p>
      `,
};
// removing admin html
const adminRemovingHtml = {
  subject: 'CrowdAid Admin removed',
  html: `
        <h1> CrowdAid </h1>
        <h3>  You have been removed from crowdAid as an admin  </h3>
        <p> currently you can't login to crowdAid as an admin</p>
      `,
};
// removing employee html
const employeeRemovingdHtml = org => ({
  subject: `CrowdAid ${org.name.ar} removed`,
  html: `
        <h4> CrowdAid ${org.name.ar} </h4>
        <h3> You have been removed from <b>${org.name.ar}</b> team  </h3>
        <p> currently you can't login to crowdAid <b>${org.name.ar}</b> </p>
      `,
});
// reinvite employee html
const employeeInvitationHtml = org => ({
  subject: `CrowdAid ${org.name.ar}`,
  html: `
        <h4> CrowdAid ${org.name.ar} owner: ${org.owner} </h4>
        <h5> You are currently part of <b>${org.name.ar}</b> team </h5>
        <p> Now you can login with your account to ${org.name.ar} dashboard </p>;
      `,
});
/**
 * send mails to user
 * @param {String} to the email of the user
 * @param {String: role} role the user role to send the email to
 * @param {Opject} org the org of the user if any
 * @param {Boolean} removing if it is removing email
 */
const sendEmail = (to, role, org, removing) => {
  const from = (org && org.owner) || 'CrowdAid <admin@nezam.io.com>';
  if (!role) { throw new Error('role is required to determined which type of users'); }
  if (!['employee', 'admin'].includes(role)) {
    throw new Error('role is should be employee or admin');
  }
  let subject = '';
  let html = '';
  if (role === 'admin') {
    if (removing) {
      subject = adminRemovingHtml.subject;
      html = adminRemovingHtml.html;
    } else {
      subject = adminReenrolledHtml.subject;
      html = adminReenrolledHtml.html;
    }
  } else if (role === 'employee' && org) {
    if (removing) {
      subject = employeeRemovingdHtml(org).subject;
      html = employeeRemovingdHtml(org).html;
    } else {
      subject = employeeInvitationHtml(org).subject;
      html = employeeInvitationHtml(org).html;
    }
  }
  return Email.send({
    to, from, subject, html,
  });
};
export default sendEmail;
