/**
 * Created by ahmad on 12/31/17.
 */

export const beneficiaryTypes = ['individual', 'animal', 'group', 'family', 'organization', 'place'];
export const caseOwnerTypes = ['user', 'organization'];
export const caseStatuses = ['draft', 'approved', 'pending', 'rejected', 'fulfilled', 'terminated'];
export const needStatuses = [
  'untouched', /* No one tried to help with this need yet */
  'touched', /* This need got one or more help offers */
  'fulfilled',
];
export const offerStatuses = [
  'offered',
  'collected',
  'flagged',
];
export const followeeTypes = [
  'user',
  'organization',
];
export const urgencyFlags = ['immediate', 'today', 'thisWeek', 'thisMonth', 'thisYear', 'N/A', 'setDate'];
export const verificationStatuses = ['pending', 'verified', 'moreInfoNeeded', 'rejected'];
export const familyRelations = [
  'son', 'daughter', 'mother', 'father', 'self',
  'aunt', 'uncle', 'grandFather', 'grandMother',
  'wife', 'husband', 'motherInLaw', 'fatherInLaw',
  'grandSon', 'grandDaughter',
];
export const Units = ['EGP', 'Item'];
export const needFrequencies = ['Once', 'Monthly', 'Yearly'];
export const autoDonationFrequencies = ['Once', 'Weekly', 'Monthly', 'Yearly'];
export const donationMethods = ['Cash on door', 'Credit card', 'Servise'];
export const donationStatuses = ['Pending', 'Collected', 'Failed', 'Cancelled', 'Offer', 'Unallocated'];
