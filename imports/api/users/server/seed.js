import { Meteor } from 'meteor/meteor';
import { Accounts } from 'meteor/accounts-base';
// seed if db is empty
Meteor.startup(() => {
  const { superadmin } = Meteor.settings;
  if (Meteor.users.findOne({ username: superadmin.username })) return;
  const superadminId = Accounts.createUser(Meteor.settings.superadmin);
  Meteor.users.update({ _id: superadminId }, {
    $set: {
      'emails.0.verified': true,
      registered_emails: [{
        address: superadmin.email,
        verified: true,
      }],
      roles: ['superadmin', 'admin'],
    },
  });
});
