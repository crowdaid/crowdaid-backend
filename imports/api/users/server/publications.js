/**
 * Created by ahmad on 6/23/17.
 */
import { Meteor } from 'meteor/meteor';
import {
  isAdmin, Authorize, isOwner,
} from '/imports/api/utils';
import Cases from '../../cases/cases';
import Organizations from '../../organizations/organizations';
import Donations from '../../donations/donations';
import Beneficiaries from '../../beneficiaries/beneficiaries';
import Categories from '../../categories/categories';
import Needs from '../../needs/needs';

Meteor.publish('Statistics', function () {
  if (!isAdmin(this.userId)) return this.ready();
  const adminsHandle = Meteor.users.find({ roles: { $in: ['admin', 'superadmin'] }, isRemoved: { $ne: true } })
    .observeChanges({
      added: (id, entry) => {
        this.added('Admins', id, entry);
      },
      changed: (id, entry) => {
        this.changed('Admins', id, entry);
      },
      removed: (id, entry) => {
        this.removed('Admins', id, entry);
      },
    });

  const endusersHandle = Meteor.users.find({ roles: { $in: ['donor'] }, isRemoved: { $ne: true } })
    .observeChanges({
      added: (id, entry) => {
        this.added('EndUsers', id, entry);
      },
      changed: (id, entry) => {
        this.changed('EndUsers', id, entry);
      },
      removed: (id, entry) => {
        this.removed('EndUsers', id, entry);
      },
    });

  const casesHandle = Cases.find({ isRemoved: { $ne: true } })
    .observeChanges({
      added: (id, entry) => {
        this.added('cases', id, entry);
      },
      changed: (id, entry) => {
        this.changed('cases', id, entry);
      },
      removed: (id, entry) => {
        this.removed('cases', id, entry);
      },
    });

  const organizationHandle = Organizations.find({ isRemoved: { $ne: true } })
    .observeChanges({
      added: (id, entry) => {
        this.added('organizations', id, entry);
      },
      changed: (id, entry) => {
        this.changed('organizations', id, entry);
      },
      removed: (id, entry) => {
        this.removed('organizations', id, entry);
      },
    });

  const donationsHandle = Donations.find({ isRemoved: { $ne: true } })
    .observeChanges({
      added: (id, entry) => {
        this.added('donations', id, entry);
      },
      changed: (id, entry) => {
        this.changed('donations', id, entry);
      },
      removed: (id, entry) => {
        this.removed('donations', id, entry);
      },
    });

  const benHandle = Beneficiaries.find({ isRemoved: { $ne: true } })
    .observeChanges({
      added: (id, entry) => {
        this.added('beneficiaries', id, entry);
      },
      changed: (id, entry) => {
        this.changed('beneficiaries', id, entry);
      },
      removed: (id, entry) => {
        this.removed('beneficiaries', id, entry);
      },
    });

  const categoriesHandle = Categories.find({ isRemoved: { $ne: true } })
    .observeChanges({
      added: (id, entry) => {
        this.added('categories', id, entry);
      },
      changed: (id, entry) => {
        this.changed('categories', id, entry);
      },
      removed: (id, entry) => {
        this.removed('categories', id, entry);
      },
    });

  const needsHandle = Needs.find({ isRemoved: { $ne: true } })
    .observeChanges({
      added: (id, entry) => {
        this.added('needs', id, entry);
      },
      changed: (id, entry) => {
        this.changed('needs', id, entry);
      },
      removed: (id, entry) => {
        this.removed('needs', id, entry);
      },
    });

  this.ready();

  this.onStop(() => {
    adminsHandle.stop();
    casesHandle.stop();
    organizationHandle.stop();
    benHandle.stop();
    categoriesHandle.stop();
    needsHandle.stop();
    donationsHandle.stop();
    endusersHandle.stop();
  });
});

Meteor.publish('loggedUser', function (userId) {
  if (userId) {
    return Meteor.users.find({ _id: userId }, { fields: { services: 0, mergedServices: 0 } });
  }
  if (!this.userId) return this.ready();
  return Meteor.users.find({ _id: this.userId }, { fields: { services: 0, mergedServices: 0 } });
});

Meteor.publish('usersList', function () {
  if (!isAdmin(this.userId)) return this.ready();
  return Meteor.users.find({}, { fields: { services: 0, mergedServices: 0 } });
});

Meteor.publish('AdminsTabular', function ({
  filter = {}, limit = 10, skip = 0, sort = { _id: -1 },
}) {
  if (!isAdmin(this.userId)) return this.ready();
  let count = 0;
  let initializing = true;
  const countHandle = Meteor.users.find({ ...filter, roles: { $in: ['admin', 'superadmin'] }, isRemoved: { $ne: true } })
    .observeChanges({
      added: (id) => {
        count += 1;
        if (!initializing) {
          this.changed('CollectionsMeta', 'Admins', { count });
        }
      },
      removed: (id) => {
        count -= 1;
        this.changed('CollectionsMeta', 'Admins', { count });
      },
    });
    // Instead, we'll send one `added` message right after `observeChanges` has
    // returned, and mark the subscription as ready.
  this.added('CollectionsMeta', 'Admins', { count });
  const docsHandle = Meteor.users.find({ ...filter, roles: { $in: ['admin', 'superadmin'] }, isRemoved: { $ne: true } },
    {
      skip, limit, sort, fields: { services: 0, mergedServices: 0, onBoarding: 0 },
    }).observeChanges({
    added: (id, entry) => {
      this.added('Admins', id, entry);
    },
    changed: (id, entry) => {
      this.changed('Admins', id, entry);
    },
    removed: (id, entry) => {
      this.removed('Admins', id, entry);
    },
  });

  initializing = false;
  this.ready();

  // Stop observing the cursor when the client unsubscribes. Stopping a
  // subscription automatically takes care of sending the client any `removed`
  // messages.
  this.onStop(() => {
    countHandle.stop();
    docsHandle.stop();
  });
});


Meteor.publish('OwnersTabular', function ({
  filter = {}, limit = 10, skip = 0, sort = { _id: -1 },
}) {
  if (!isAdmin(this.userId)) return this.ready();
  let count = 0;
  let initializing = true;
  const countHandle = Meteor.users.find({
    ...filter, roles: 'owner', organizationId: { $ne: null }, isRemoved: { $ne: true },
  })
    .observeChanges({
      added: (id) => {
        count += 1;
        if (!initializing) {
          this.changed('CollectionsMeta', 'Owners', { count });
        }
      },
      removed: (id) => {
        count -= 1;
        this.changed('CollectionsMeta', 'Owners', { count });
      },
    });
    // Instead, we'll send one `added` message right after `observeChanges` has
    // returned, and mark the subscription as ready.
  this.added('CollectionsMeta', 'Owners', { count });
  const docsHandle = Meteor.users.find({
    ...filter, roles: 'owner', organizationId: { $ne: null }, isRemoved: { $ne: true },
  },
  {
    skip, limit, sort, fields: { services: 0, mergedServices: 0, onBoarding: 0 },
  }).observeChanges({
    added: (id, entry) => {
      this.added('Owners', id, entry);
    },
    changed: (id, entry) => {
      this.changed('Owners', id, entry);
    },
    removed: (id, entry) => {
      this.removed('Owners', id, entry);
    },
  });

  initializing = false;
  this.ready();

  // Stop observing the cursor when the client unsubscribes. Stopping a
  // subscription automatically takes care of sending the client any `removed`
  // messages.
  this.onStop(() => {
    countHandle.stop();
    docsHandle.stop();
  });
});

Meteor.publish('StaffTabular', function ({
  filter = {}, limit = 10, skip = 0, sort = { _id: -1 }, orgId,
}) {
  if (!isOwner(this.userId) && !isAdmin(this.userId)) return this.ready();
  let count = 0;
  let initializing = true;

  const query = { ...filter, roles: { $in: ['employee'] }, isRemoved: { $ne: true } };
  const user = Meteor.users.findOne({ _id: this.userId }, { fields: { organizationId: 1 } });

  if (isOwner(this.userId)) query.organizationId = user.organizationId;
  else query.organizationId = orgId;

  const countHandle = Meteor.users.find(query).observeChanges({
    added: (id) => {
      count += 1;
      if (!initializing) {
        this.changed('CollectionsMeta', 'Employees', { count });
      }
    },
    removed: (id) => {
      count -= 1;
      this.changed('CollectionsMeta', 'Employees', { count });
    },
  });

  // Instead, we'll send one `added` message right after `observeChanges` has
  // returned, and mark the subscription as ready.
  this.added('CollectionsMeta', 'Employees', { count });
  const docsHandle = Meteor.users.find(query,
    {
      skip, limit, sort, fields: { services: 0, mergedServices: 0, onBoarding: 0 },
    }).observeChanges({
    added: (id, entry) => {
      this.added('employees', id, entry);
    },
    changed: (id, entry) => {
      this.changed('employees', id, entry);
    },
    removed: (id, entry) => {
      this.removed('employees', id, entry);
    },
  });

  initializing = false;
  this.ready();

  // Stop observing the cursor when the client unsubscribes. Stopping a
  // subscription automatically takes care of sending the client any `removed`
  // messages.
  this.onStop(() => {
    countHandle.stop();
    docsHandle.stop();
  });
});

Meteor.publish('org.users', function (org) {
  const authorized = Authorize(['admin', `manage-org-${org._id}`], true);
  if (!authorized) return this.ready();
  return Meteor.users.find({ _id: { $in: [org.claimedBy].concat(org.employees) } });
});

Meteor.publish('case.users', function (singleCase) {
  const authorized = Authorize(['admin', `manage-org-${singleCase.owner.id}`], true);
  if (!authorized) return this.ready();
  return Meteor.users.find({
    _id: {
      $in: [
        singleCase.responsible, singleCase.researcher, singleCase.owner.id,
      ],
    },
  });
});

Meteor.publish('users.single', function (userId) {
  if (!this.userId) return this.ready();
  return Meteor.users.find({ _id: userId }, {
    fields: {
      _id: 1, emails: 1, roles: 1, profile: 1,
    },
  });
});


Meteor.publish('EndUsersTabular', function ({
  filter = {}, limit = 10, skip = 0, sort = { _id: -1 },
}) {
  if (!isAdmin(this.userId)) return this.ready();
  let count = 0;
  let initializing = true;
  const roleQuery = { $or: [{ roles: { $exists: false } }, { roles: 'donor' }] };
  const countHandle = Meteor.users.find({ ...filter, ...roleQuery, isRemoved: { $ne: true } })
    .observeChanges({
      added: (id) => {
        count += 1;
        if (!initializing) {
          this.changed('CollectionsMeta', 'EndUsers', { count });
        }
      },
      removed: (id) => {
        count -= 1;
        this.changed('CollectionsMeta', 'EndUsers', { count });
      },
    });
    // Instead, we'll send one `added` message right after `observeChanges` has
    // returned, and mark the subscription as ready.
  this.added('CollectionsMeta', 'EndUsers', { count });
  const docsHandle = Meteor.users.find({ ...filter, ...roleQuery, isRemoved: { $ne: true } },
    {
      skip, limit, sort, fields: { services: 0, mergedServices: 0, onBoarding: 0 },
    }).observeChanges({
    added: (id, entry) => {
      this.added('EndUsers', id, entry);
    },
    changed: (id, entry) => {
      this.changed('EndUsers', id, entry);
    },
    removed: (id, entry) => {
      this.removed('EndUsers', id, entry);
    },
  });

  initializing = false;
  this.ready();

  // Stop observing the cursor when the client unsubscribes. Stopping a
  // subscription automatically takes care of sending the client any `removed`
  // messages.
  this.onStop(() => {
    countHandle.stop();
    docsHandle.stop();
  });
});

Meteor.publish('case.donors', function (donorIds) {
  if (!this.userId) return this.ready();
  return Meteor.users.find({ _id: { $in: donorIds } }, { fields: { _id: 1, emails: 1, roles: 1 } });
});

Meteor.publish('need.donors', function (donorIds) {
  if (!this.userId) return this.ready();
  return Meteor.users.find({ _id: { $in: donorIds } }, { fields: { _id: 1, emails: 1, roles: 1 } });
});
