import SimpleSchema from 'simpl-schema';
import { Meteor } from 'meteor/meteor';
import { Roles } from 'meteor/alanning:roles';
import { Accounts } from 'meteor/accounts-base';
import { ValidatedMethod } from 'meteor/mdg:validated-method';
import Organizations from '/imports/api/organizations/organizations';
import {
  userCheck, isAdmin, isSuperAdmin, validateEmail,
  isOwner, Authorize, inviteUser, isEmployee, isDonor,
} from '/imports/api/utils';
import sendEmail from '../EmailsUtils';
import Cases from '../cases/cases';
import Needs from '../needs/needs';
import Donations from '../donations/donations';

export const makeAdmin = new ValidatedMethod({
  name: 'users.methods.make_admin',
  validate: new SimpleSchema({
    userId: {
      type: String,
      regEx: SimpleSchema.RegEx.Id,
    },
  }).validator({ clean: true }),
  run({ userId }) {
    Authorize('admin');
    if (isAdmin(userId)) throw new Meteor.Error('unauthorized', "You don't have permission to enter this page");
    Roles.addUsersToRoles(userId, ['admin']);
  },
});

export const removeRoleFromUser = new ValidatedMethod({
  name: 'users.methods.remove_role',
  validate: new SimpleSchema({
    userId: {
      type: String,
      regEx: SimpleSchema.RegEx.Id,
    },
    role: {
      type: String,
      optional: true,
    },
  }).validator({ clean: true }),
  run({ userId, role }) {
    Authorize(['admin', 'owner']);
    if ((role === 'admin' || role === 'owner') && (!isAdmin(this.userId) && !isSuperAdmin(this.userId))) throw new Meteor.Error('unauthorized', "You don't have permission to enter this page");
    if (role === 'employee' && (!isOwner(this.userId) && !isSuperAdmin(userId) && !isAdmin(userId))) throw new Meteor.Error('unauthorized', "You don't have permission to enter this page");

    Roles.removeUsersFromRoles(userId, role);
    Meteor.users.update({ _id: userId }, {
      $unset: {
        organizationId: '',
      },
    });
  },
});


export const removeUser = new ValidatedMethod({
  name: 'users.methods.remove',
  validate: new SimpleSchema({
    userId: {
      type: String,
      regEx: SimpleSchema.RegEx.Id,
    },
  }).validator({ clean: true }),
  run({ userId }) {
    Authorize(['admin', 'owner']);
    const user = Meteor.users.findOne({ _id: userId });
    if (isAdmin(this.userId) || isSuperAdmin(this.userId)) {
      // send removing email to the admin
      sendEmail(user.emails[0].address, 'admin', null, true);
      Meteor.users.update({ _id: userId }, {
        $set: {
          isRemoved: true,
          'services.resume.loginTokens': [], // logout the user when removed from the system
        },
      });
    }

    // remove employee from the current organization
    // set the current organizationId to other organization the employee working in
    // set to organizationId to null if the employee not working at any
    const owner = Meteor.users.findOne({ _id: this.userId });
    const currentOrg = Organizations.findOne({ _id: user.organizationId },
      { fields: { _id: 1, name: 1 } });
    if (isEmployee(userId) && user.organizationId === owner.organizationId) {
      Organizations.update({ _id: user.organizationId }, {
        $pull: {
          employees: userId,
        },
      });
      const employeeOrgs = Organizations.find({ employees: userId, isRemoved: { $ne: true } },
        { fields: { _id: 1, name: 1 } }).fetch();
      Meteor.users.update({ _id: userId }, {
        $set: {
          organizationId: employeeOrgs[0] ? employeeOrgs[0]._id : null,
          'services.resume.loginTokens': [], // logout the user when removed from the system
        },
      });
      // send removing email to the employee
      sendEmail(user.emails[0].address, 'employee', currentOrg, true);
    }
  },
});

export const updateProfile = new ValidatedMethod({
  name: 'users.methods.update_profile',
  validate: new SimpleSchema({
    userId: {
      type: String,
      regEx: SimpleSchema.RegEx.Id,
      optional: true,
    },
    firstName: {
      type: String,
      optional: true,
    },
    lastName: {
      type: String,
      optional: true,
    },
    mobile: {
      type: String,
      optional: true,
    },
    idNumber: {
      type: String,
      optional: true,
    },
  }).validator({ clean: true }),
  run({
    userId, firstName, lastName, mobile, idNumber,
  }) {
    if (idNumber) {
    // find if the national number is already exists
      const duplicateNationalNum = Meteor.users.find({
        'profile.idNumber': idNumber,
        _id: { $ne: userId || Meteor.userId() },
      }).count();
      if (duplicateNationalNum) {
        throw new Meteor.Error(403, 'National ID number already exist');
      }
    }


    if (!userId) {
      Meteor.users.update(Meteor.userId(), {
        $set: {
          'profile.firstName': firstName,
          'profile.lastName': lastName,
          'profile.mobile': mobile,
          'profile.idNumber': idNumber,
        },
      });
      return;
    }

    const user = Meteor.users.findOne({ _id: userId });
    const owner = Meteor.users.findOne({ _id: this.userId });

    //  give permissions to the admin
    if (!isAdmin(this.userId) && (!isOwner(this.userId) || user.organizationId !== owner.organizationId)) {
      throw new Meteor.Error('unauthorized', "You don't have permission to edit this profile");
    }

    Meteor.users.update(userId, {
      $set: {
        'profile.firstName': firstName,
        'profile.lastName': lastName,
        'profile.mobile': mobile,
        'profile.idNumber': idNumber,
      },
    });
  },
});

export const updateAvatar = new ValidatedMethod({
  name: 'users.methods.update_avatar',
  validate: new SimpleSchema({
    fileId: {
      type: String,
      regEx: SimpleSchema.RegEx.Id,
    },
    userId: {
      type: String,
      regEx: SimpleSchema.RegEx.Id,
    },
  }).validator({ clean: true }),
  run({ fileId, userId }) {
    userCheck(userId, 'users.methods.update_avatar');
    Meteor.users.update(userId, {
      $set: {
        'profile.avatar': fileId,
      },
    });
  },
});

// TODO: fix profile address
export const updateDonorProfile = new ValidatedMethod({
  name: 'users.methods.update_donor_profile',
  validate: new SimpleSchema({
    entity: {
      type: String,
    },
    value: {
      type: String,
      optional: true,
    },
    action: {
      type: String,
      allowedValues: ['add', 'remove'],
    },
  }).validator({ clean: true }),
  run({ entity, value, action }) {
    if (!isDonor(Meteor.userId())) {
      throw new Meteor.Error(503, 'You not allowed to do the action');
    }

    if (!['address', 'phone'].includes(entity)) {
      throw new Meteor.Error(400, `${entity} not allowed value to edit`);
    }
    if (action === 'add' && !value) {
      throw new Meteor.Error(400, 'Value required in add action');
    }
    // add address or phone
    if (action === 'add') {
      Meteor.users.update(Meteor.userId(), {
        $set: {
          [`profile.${entity}`]: value,
        },
      });
    } else {
      // remove address or phone
      Meteor.users.update(Meteor.userId(), {
        $unset: {
          [`profile.${entity}`]: '',
        },
      });
    }
  },
});

export const addEmail = new ValidatedMethod({
  name: 'users.methods.add_email',
  validate: new SimpleSchema({
    email: {
      type: String,
      regEx: SimpleSchema.RegEx.email,
    },
    userId: {
      type: String,
      regEx: SimpleSchema.RegEx.Id,
      optional: true,
    },
  }).validator({ clean: true }),
  run({ userId, email }) {
    userCheck(userId || Meteor.userId(), 'users.methods.add_email');
    if (Meteor.isServer) {
      Accounts.addEmail(userId || Meteor.userId(), email);
      Accounts.sendVerificationEmail(userId || Meteor.userId(), email);
    }
  },
});

export const removeEmail = new ValidatedMethod({
  name: 'users.methods.remove_email',
  validate: new SimpleSchema({
    email: {
      type: String,
      regEx: SimpleSchema.RegEx.email,
    },
    userId: {
      type: String,
      regEx: SimpleSchema.RegEx.Id,
      optional: true,
    },
  }).validator({ clean: true }),
  run({ userId, email }) {
    userCheck(userId || Meteor.userId(), 'users.methods.remove_email');
    if (!Meteor.isServer) return;
    const user = Meteor.users.findOne({ _id: userId || Meteor.userId() });
    const userEmails = user.emails;
    if (userEmails[0].address === email) {
      throw new Meteor.Error(400, 'Can not remove the primary email');
    }
    Accounts.removeEmail(userId || Meteor.userId(), email);
  },
});

export const sendVerification = new ValidatedMethod({
  name: 'users.methods.send_verification',
  validate: new SimpleSchema({
    email: {
      type: String,
      regEx: SimpleSchema.RegEx.email,
    },
    userId: {
      type: String,
      regEx: SimpleSchema.RegEx.Id,
      optional: true,
    },
  }).validator({ clean: true }),
  run({ userId, email }) {
    userCheck(userId || Meteor.userId(), 'users.methods.send_verification');
    if (!Meteor.isServer) return;
    Accounts.sendVerificationEmail(userId || Meteor.userId(), email);
  },
});

export const forgetPassword = new ValidatedMethod({
  name: 'users.methods.forget_password',
  validate: new SimpleSchema({
    email: {
      type: String,
      regEx: SimpleSchema.RegEx.email,
    },
  }).validator({ clean: true }),
  run({ email }) {
    userCheck(Meteor.userId(), 'users.methods.send_verification');
    if (!Meteor.isServer) return;
    Accounts.sendResetPasswordEmail(Meteor.userId(), email);
  },
});

/**
 * change password of users by the admin or the superadmin
 * accept the admin current password and the new password
 * @param {String} oldPassword
 * @param {String} newPassword
 * @param {String} userId
 */
export const changePassword = new ValidatedMethod({
  name: 'users.methods.change_password',
  validate: new SimpleSchema({
    oldPassword: {
      type: String,
    },
    newPassword: {
      type: String,
    },
    userId: {
      type: String,
    },
  }).validator({ clean: true }),
  run({ userId, oldPassword, newPassword }) {
    // check if the logged user have the admin or super admin role
    if (!isAdmin(this.userId) || !isSuperAdmin(this.userId)) {
      throw new Meteor.Error('unauthorized', 'You not have the permissions to change this user password');
    }
    // admin can't change other admin password
    if (isAdmin(userId) || isSuperAdmin(userId)) {
      throw new Meteor.Error(400, "You can't change other admin's password");
    }
    if (!newPassword || !oldPassword) {
      throw new Meteor.Error(400, 'Password may not be empty');
    }
    const admin = Meteor.user();
    // check the admin old password
    const oldRes = Accounts._checkPassword(admin, oldPassword);
    if (oldRes.error) {
      throw new Meteor.Error(403, oldRes.error.reason);
    }
    if (Meteor.isServer) {
      Accounts.setPassword(userId, newPassword);
    }
  },
});

// invite new user
export const inviteWithEmail = new ValidatedMethod({
  name: 'users.methods.send_invitation',
  validate: new SimpleSchema({
    email: {
      type: String,
      regEx: SimpleSchema.RegEx.email,
    },
    role: {
      type: String,
    },
  }).validator({ clean: true }),
  run({ email, role }) {
    // check if user how send the invitation is an admin or owner
    Authorize(['admin', 'owner']);
    // create admin user
    if (Meteor.isServer) {
      const user = inviteUser(role, email);

      if (user.invited) return `A new account created, and invitation email sent successfully to ${email}!`;

      // check if the user already in the system his password
      if (user.inSystem) {
        if (user.role === 'admin') { return `${email} currently is an active admin`; }
        if (user.role === 'employee') { return `${email} is already part of your organization`; }
      }
      // send email and make the admin not removed if it is
      // send email ans make the employee part of the organization if not
      if (user.enrolled) {
        if (user.role === 'admin') { return `${email} is back, and now he is an active admin`; }
        if (user.role === 'employee') { return `${email} is now part of your organization`; }
      }

      // if does exist resend the enrollment email
      if (user.resent) return `You already sent an invitation to ${email}, invitation email is resent.`;

      throw new Meteor.Error(400, 'Bad Request');
    }
  },
});

// set the new admin username
export const registerAdmin = new ValidatedMethod({
  name: 'users.methods.register_admin',
  validate: new SimpleSchema({
    firstName: {
      type: String,
    },
    lastName: {
      type: String,
    },
    mobile: {
      type: String,
    },
    idNumber: {
      type: String,
    },
    token: {
      type: String,
    },
  }).validator({ clean: true }),
  run({
    token, firstName, lastName, mobile, idNumber,
  }) {
    // find the user by token
    const user = Meteor.users.findOne({ 'services.password.reset.token': token });

    if (!user) { // never existed or already been used
      throw new Meteor.Error(403, 'Token expired, contact the administrator to resend the invitation');
    }

    if (!isAdmin(user._id)) throw new Meteor.Error('unauthorized', "You don't have permission to enter this page");
    // find if the national number is already exists
    const duplicateNationalNum = Meteor.users.find({ 'profile.idNumber': idNumber }).count();
    if (duplicateNationalNum) {
      throw new Meteor.Error(403, 'National ID number already exist');
    }

    Meteor.users.update(user._id, {
      $set: {
        'profile.firstName': firstName,
        'profile.lastName': lastName,
        'profile.idNumber': idNumber,
        'profile.mobile': mobile,
      },
    });
  },
});

export const registerEmployee = new ValidatedMethod({
  name: 'users.methods.register_via_organization',
  validate: new SimpleSchema({
    firstName: {
      type: String,
    },
    lastName: {
      type: String,
    },
    mobile: {
      type: String,
    },
    idNumber: {
      type: String,
    },
    token: {
      type: String,
    },
  }).validator({ clean: true }),
  run({
    token, firstName, lastName, mobile, idNumber,
  }) {
    // find the user by token
    const user = Meteor.users.findOne({ 'services.password.reset.token': token });

    // never existed or already been used
    if (!user) throw new Meteor.Error(403, 'Token expired, contact the administrator to resend the invitation');

    if (!isEmployee(user._id) && !isOwner(user._id)) throw new Meteor.Error('unauthorized', "You don't have permission to enter this page");

    // find if the national number is already exists
    const duplicateNationalNum = Meteor.users.find({ 'profile.idNumber': idNumber }).count();
    if (duplicateNationalNum) {
      throw new Meteor.Error(403, 'National ID number already exist');
    }

    Meteor.users.update(user._id, {
      $set: {
        'profile.firstName': firstName,
        'profile.lastName': lastName,
        'profile.idNumber': idNumber,
        'profile.mobile': mobile,
      },
    });
  },
});


export const getUserToken = new ValidatedMethod({
  name: 'users.methods.get_user_token',
  validate: new SimpleSchema({
    token: {
      type: String,
    },
  }).validator({ clean: true }),
  run({ token }) {
    return Meteor.users.findOne({
      'services.password.reset.token': token,
      isRemoved: { $ne: true },
    });
  },
});

export const getAdminDashboardStats = new ValidatedMethod({
  name: 'users.methods.get_admin_dahsboard_stats',
  validate: new SimpleSchema({
    type: {
      type: String,
    },
  }).validator({ clean: true }),
  async run({ type }) {
    if (!isAdmin(Meteor.userId())) return Meteor.Error(500, 'Not allowd to do this action');
    if (type !== 'admin') return Meteor.Error(500, 'Not allowd to do this action');
    const needsStats = Needs.rawCollection().aggregate(
      [
        { $match: { isRemoved: { $ne: true } } },
        { $sort: { createdAt: -1 } },
        {
          $group: {
            _id: { month: { $month: '$createdAt' }, year: { $year: '$createdAt' } },
            count: { $sum: 1 },
            date: { $first: '$createdAt' },
          },
        }, {
          $project: {
            date: '$date',
            count: 1,
            _id: 0,
          },
        }],
    ).toArray();
    const casesStats = Cases.rawCollection().aggregate(
      [
        { $match: { isRemoved: { $ne: true } } },
        { $sort: { createdAt: -1 } },
        {
          $group: {
            _id: { month: { $month: '$createdAt' }, year: { $year: '$createdAt' } },
            count: { $sum: 1 },
            date: { $first: '$createdAt' },
          },
        }, {
          $project: {
            date: '$date',
            count: 1,
            _id: 0,
          },
        }],
    ).toArray();
    const donationsStats = Donations.rawCollection().aggregate(
      [
        { $match: { isRemoved: { $ne: true } } },
        { $sort: { createdAt: -1 } },
        {
          $group: {
            _id: { month: { $month: '$createdAt' }, year: { $year: '$createdAt' } },
            count: { $sum: 1 },
            date: { $first: '$createdAt' },
          },
        }, {
          $project: {
            date: '$date',
            count: 1,
            _id: 0,
          },
        }],
    ).toArray();
    const donoersStats = Meteor.users.rawCollection().aggregate(
      [
        { $match: { roles: 'donor' } },
        { $sort: { createdAt: -1 } },
        {
          $group: {
            _id: { month: { $month: '$createdAt' }, year: { $year: '$createdAt' } },
            count: { $sum: 1 },
            date: { $first: '$createdAt' },
          },
        }, {
          $project: {
            date: '$date',
            count: 1,
            _id: 0,
          },
        }],
    ).toArray();
    return Promise.all([casesStats, needsStats, donoersStats, donationsStats]);
  },
});

// Donor methods

export const registerDonorUser = new ValidatedMethod({
  name: 'users.methods.register_donor_user',
  validate: new SimpleSchema({
    fromOrg: {
      type: Boolean,
      defaultValue: false,
    },
    username: {
      type: String,
    },
    email: {
      type: String,
    },
    password: {
      type: String,
      optional: true,
    },
    profile: {
      type: Object,
      optional: true,
      defaultValue: {},
    },
    'profile.address': {
      type: String,
      optional: true,
    },
    'profile.phone': {
      type: String,
      optional: true,
    },
  }).validator({ clean: true }),
  run({
    username, email, password, profile, fromOrg,
  }) {
    // password not required if the account comes  from organization
    if (!fromOrg && !password) {
      throw new Meteor.Error(400, 'Please enter a password');
    }
    if (!validateEmail(email)) {
      throw new Meteor.Error(400, 'Email is invalid ');
    }

    const userId = Accounts.createUser({
      username, email, password: fromOrg ? '' : password,
    });
    // set the user profile
    Meteor.users.update({ _id: userId }, {
      $set: {
        profile,
      },
    });
    Roles.addUsersToRoles(userId, ['donor']);
    return userId;
  },
});

export const searchDonors = new ValidatedMethod({
  name: 'users.methods.search_donors',
  validate: new SimpleSchema({
    keyword: {
      type: String,
    },
  }).validator({ clean: true }),
  run({ keyword }) {
    Authorize(['owner', 'employee']);
    const regex = new RegExp(keyword, 'ig');

    const users = Meteor.users.createQuery({
      $filters: {
        $or: [
          { username: { $regex: regex } },
          { 'profile.phone': { $regex: regex } },
          { 'emails.address': { $regex: regex } },
        ],
        isRemoved: { $ne: true },
        roles: 'donor',
      },
      username: 1,
      emails: 1,
      profile: 1,
      userImage: 1,
    }).fetch();

    return users;
  },
});
