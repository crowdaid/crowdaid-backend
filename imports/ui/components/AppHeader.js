import { Meteor } from 'meteor/meteor';
import { withTracker } from 'meteor/react-meteor-data';
import { ReactiveVar } from 'meteor/reactive-var';
import { moment } from 'meteor/momentjs:moment';
import React from 'react';
import { Link } from 'react-router-dom';
import Layout from 'antd/lib/layout';
import Button from 'antd/lib/button';
import Menu from 'antd/lib/menu';
import Avatar from 'antd/lib/avatar';
import Icon from 'antd/lib/icon';
import Badge from 'antd/lib/badge';
import { AccountsReact } from 'meteor/meteoreact:accounts';
import { isAdmin } from '/imports/api/utils';
import { Notifications } from 'meteor/meteorize:notifications';

const SubMenu = Menu.SubMenu;
const perPage = 7;
const limit = new ReactiveVar(perPage);

class AppHeader extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      numUnread: 0,
      showNumUnread: false,
    };
  }

  componentDidMount() {
  }

  componentWillReceiveProps(props) {
    if (props.numUnread != this.numUnread) {
      this.setState({ showNumUnread: true });
    }
    this.setState({ numUnread: props.numUnread });
  }

  loadMoreNots(e) {
    e.stopPropagation();
    limit.set(limit.get() + perPage);
  }

  getNots() {
    const nots = this.props.nots;
    return nots.map((not) => {
      let title; let content; let
        url;
      switch (not.type) {
        case 'CaseAssigned':
          url = `/profile/org/${not.data.owner.id}/cases/${not.data.caseId}`;
          title = 'New Case';
          content = 'New case assigned to you';
          break;
        case 'CaseUpdated':
          url = `/profile/org/${not.data.owner.id}/cases/${not.data.caseId}`;
          title = 'Case Updated';
          content = `Case #${not.data.caseId} has been updated`;
          break;
        default:
          url = '/profile';
          title = 'New Notification';
          content = 'You have a new notification';
      }

      return (
        <Menu.Item key={`not-${not._id}`} className={not.read ? 'read' : 'unread'}>
          <Link to={url}>
            <Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />
            <span className="not-content">{content}</span>
            <span className="not-date">
              {moment(not.createdDate).fromNow()}
            </span>
          </Link>
        </Menu.Item>
      );
    });
  }

  handleNotClick({ item, key, keyPath }) {
    if (!key) return;
    const parts = key.split('-');
    if (parts && parts[0] === 'not') {
      Notifications.markRead(parts[1], this.props.userId);
    }
  }

  notLinkContent() {
    const { showNumUnread, numUnread } = this.state;
    return (
      <div className="posrel">
        <Icon type="bell" />
        <Badge style={{ position: 'absolute', top: '-15px' }} count={showNumUnread ? numUnread : 0}>
          <a href="javascript:void(0)" />
        </Badge>
      </div>
    );
  }

  render() {
    return (
      <Layout.Header className="AppHeader">
        <Link to="/">
          <Button type="primary" size="large" icon="dingding">Home</Button>
        </Link>
        {/* <div className="logo" /> */}
        {this.props.user ? (
          <Menu mode="horizontal" selectable={false} onClick={this.handleNotClick.bind(this)} onOpenChange={() => { this.setState({ showNumUnread: false }); }}>
            <SubMenu title={this.notLinkContent()} className="nots-menu">
              <Menu.Item disabled style={{ padding: '0 20px', margin: '0', cursor: 'default' }}>
                <Icon type="bell" />
                {' '}
&nbsp; Notifications
              </Menu.Item>
              <Menu.Divider />
              <Menu.ItemGroup>
                {this.getNots()}
              </Menu.ItemGroup>
              <Menu.Divider />
              <Menu.Item style={{ margin: 0, textAlign: 'center' }}>
                <a onClick={this.loadMoreNots.bind(this)} href="javascript:void(0);">Load more</a>
              </Menu.Item>
            </SubMenu>
            {isAdmin(this.props.user._id)
              ? (
                <Menu.Item>
                  <Link to="/admin">
                    <Icon type="dashboard" />
                    {' '}
Admin
                  </Link>
                </Menu.Item>
              ) : ''
            }
            <Menu.Item>
              <Link to="/profile">
                <Icon type="user" />
                {' '}
                {this.props.user.username || 'Profile'}
              </Link>
            </Menu.Item>
            <Menu.Item>
              <Link to="/" onClick={() => { AccountsReact.logout(); }}>
                <Icon type="logout" />
                {' '}
Logout
              </Link>
            </Menu.Item>
          </Menu>
        ) : (
          <Menu mode="horizontal">
            <Menu.Item>
              <Link
                to={{
                  pathname: '/login',
                  state: { from: '/' },
                }}
              >
                <Icon type="login" />
                {' '}
Login
              </Link>
            </Menu.Item>
            <Menu.Item>
              <Link
                to={{
                  pathname: '/join',
                  state: { from: '/' },
                }}
              >
                <Icon type="link" />
                {' '}
Join
              </Link>
            </Menu.Item>
          </Menu>
        )}
      </Layout.Header>
    );
  }
}

export default withTracker((props) => {
  Meteor.subscribe('notifications');
  return {
    nots: Notifications.get(limit.get()),
    numUnread: Notifications.unreadCount(),
    userId: Meteor.userId(),
  };
})(AppHeader);
