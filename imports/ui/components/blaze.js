import React from 'react';
import ReactDOM from 'react-dom';
import { Blaze as MeteorBlaze } from 'meteor/blaze';
import { Template } from 'meteor/templating';
import PropTypes from 'prop-types';

class Blaze extends React.Component {
  componentDidMount() {
    const componentRoot = ReactDOM.findDOMNode(this);
    const parentNode = componentRoot.parentNode;
    // Render the Blaze template on this node
    this.view = MeteorBlaze.renderWithData(Template[this.props.template], this.props.data, parentNode, componentRoot);
    parentNode.removeChild(componentRoot);
  }

  componentWillUnmount() {
    // Clean up Blaze view
    MeteorBlaze.remove(this.view);
  }

  render() {
    return <div />;
  }
}

Blaze.propTypes = {
  template: PropTypes.string.isRequired,
};

export default Blaze;
