import React from 'react';
import PropTypes from 'prop-types';
import { Switch, Route } from 'react-router-dom';
import LocaleProvider from 'antd/lib/locale-provider';
import enUS from 'antd/lib/locale-provider/en_US';
import Layout from 'antd/lib/layout';
import AppHeader from '../components/AppHeader';


const { Footer, Content } = Layout;

import Admin from '../pages/Admin';

import Login from '../pages/Login';
import Join from '../pages/Join';
import Enroll from '../pages/Enroll';
import VerifyEmail from '../pages/VerifyEmail';

import Home from '../pages/Home';
import UserProfile from '../pages/UserProfile';
import Cases from '../public/cases/index';
import JustGive from '../public/donations/justGive';

import NoMatch from '../pages/NoMatch';

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      user: props.user,
      connected: props.connected,
    };
  }

  render() {
    return (
      <div>
        <LocaleProvider locale={enUS}>
          <div className="outerContainer">
            <Layout className="layout">
              <AppHeader {...this.props} />
              <Content style={{ padding: '0 50px', flex: 1 }}>
                <div className="flexContainer">
                  <div className="flexItem flexArea" style={{ background: '#fff', padding: 24 }}>
                    <Switch>
                      <Route path="/admin" render={props => (<Admin {...this.props} {...props} />)} />

                      <Route path="/login" render={props => (<Login {...this.props} {...props} />)} />
                      <Route path="/join" render={props => (<Join {...this.props} {...props} />)} />
                      <Route path="/verify-email/:token" render={props => (<VerifyEmail {...this.props} {...props} />)} />
                      <Route path="/enroll-account/:token" render={props => (<Enroll {...this.props} {...props} />)} />

                      <Route exact path="/" render={props => (<Home {...this.props} {...props} />)} />
                      <Route path="/profile" render={props => (<UserProfile {...this.props} {...props} />)} />
                      <Route path="/cases" render={props => (<Cases {...this.props} {...props} />)} />
                      <Route path="/just-give" render={props => <JustGive {...this.props} {...props} />} />

                      <Route component={NoMatch} />
                    </Switch>
                  </div>
                </div>
              </Content>
              <Footer style={{ textAlign: 'center' }}>
                Footer
              </Footer>
            </Layout>
          </div>
        </LocaleProvider>
      </div>
    );
  }
}

App.propTypes = {
  user: PropTypes.object, // current meteor user
  connected: PropTypes.bool, // server connection status
};

App.contextTypes = {
  router: PropTypes.object,
};
