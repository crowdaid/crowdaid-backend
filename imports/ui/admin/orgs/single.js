import React from 'react';
import { Link } from 'react-router-dom';
import { Meteor } from 'meteor/meteor';
import { withTracker } from 'meteor/react-meteor-data';
import PropTypes from 'prop-types';
import Organizations from '/imports/api/organizations/organizations';
import Causes from '/imports/api/causes/causes';
import Files from '/imports/api/files/files';
import { setManager } from '/imports/api/organizations/methods';
import {
  Form, Input, Button, Tag, Icon, message,
} from 'antd/lib';
import { isAdmin } from '/imports/api/utils';

class Single extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      ready: false,
      settingManager: false,
      loading: false,
    };
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      ready: !nextProps.loading,
	  });
  }

  renderLegals() {
    const files = this.props.files;
    return files.map((file) => {
      const url = Files.link(file);
      return (
        <Tag key={file._id}>
          <a href={url} target="_blank">{file.name}</a>
        </Tag>
      );
    });
  }

  renderCauses() {
    const causes = this.props.causes;
    return causes.map(cause => <Tag key={cause._id} color="orange">{cause.name}</Tag>);
  }

  getOrgManager() {
    if (isAdmin(this.props.manager)) {
      const settingManager = this.state.settingManager;
      const loading = this.state.loading;
      return (
        <tr>
          <td colSpan="2">
            {settingManager
              ? (
                <Form layout="inline" onSubmit={this.setManager.bind(this)}>
                  <Form.Item>
                    <Input prefix={<Icon type="user" />} type="email" id="managerEmail" placeholder="Enter manager email address" />
                  </Form.Item>
                  <Form.Item>
									&nbsp; &nbsp;
                    <Button type="primary" htmlType="submit" loading={loading}>
										Set Manager

                    </Button>
									&nbsp; &nbsp;
                    <Button type="default" htmlType="button" onClick={() => { this.setState({ settingManager: false }); }}>
										Cancel

                    </Button>
                  </Form.Item>
                </Form>
              ) : (
                <div>
								Manager not set &nbsp;
                  <Button icon="user" onClick={() => { this.setState({ settingManager: true }); }}>Set Manager</Button>
                </div>
              )}
          </td>
        </tr>
      );
    }
    return (
      <tr>
        <td>Manager:</td>
        <td>
          {this.props.manager.emails[0].address}
          {' '}
&nbsp;
          <Link to={`/admin/users/${this.props.manager._id}`}>Profile</Link>
        </td>
      </tr>
    );
  }

  setManager(e) {
    e.preventDefault();
    this.setState({ loading: true });
    const form = e.target;
    const email = form.managerEmail.value;
    setManager.call({
      orgId: this.props.org._id,
      email,
    }, (err, res) => {
      if (err) {
        this.setState({ loading: false });
        alert(err);
      } else {
        this.setState({ loading: false, settingManager: false });
        message.success(res);
      }
    });
  }

  render() {
    const ready = this.state.ready;
    return (
      <div className="single-view org-view posrel">
        { !ready ? <div>Loading</div>
          : (
            <div>
              <div className="single-view-actions">
                <Link to={`/admin/orgs/${this.props.org._id}/edit`}>
                  <Button type="default" icon="edit">
								Edit

                  </Button>
                </Link>
						&nbsp;
                <Button type="danger" icon="delete" onClick={() => { this.props.removeOrg(this.props.org); }}>
							Delete

                </Button>
              </div>
              <table>
                <thead>
                  <tr>
                    <td colSpan="2">
                      <h2> Organization details </h2>
                    </td>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>English Name</td>
                    <td>{this.props.org.name.en}</td>
                  </tr>
                  <tr>
                    <td>Arabic Name</td>
                    <td>{this.props.org.name.ar}</td>
                  </tr>
                  <tr>
                    <td>About</td>
                    <td>{this.props.org.about}</td>
                  </tr>
                  <tr>
                    <td>Registration Number</td>
                    <td>{this.props.org.registrationCode}</td>
                  </tr>
                  <tr>
                    <td>Registration Date</td>
                    <td>{this.props.org.registrationDate.toString()}</td>
                  </tr>
                  <tr>
                    <td>Legal Files</td>
                    <td>
                      {this.renderLegals()}
                    </td>
                  </tr>
                  <tr>
                    <td>Headquarters</td>
                    <td>{this.props.org.HQLocation}</td>
                  </tr>
                  <tr>
                    <td>Causes</td>
                    <td>{this.renderCauses()}</td>
                  </tr>
                </tbody>
                <thead>
                  <tr>
                    <td colSpan="2">
                      <br />
                      <h2> Organization Manager </h2>
                    </td>
                  </tr>
                </thead>
                <tbody>
                  {this.getOrgManager()}
                </tbody>
              </table>
            </div>
          )
			}
      </div>
    );
  }
}

Single.propTypes = {
  user: PropTypes.object,
  org: PropTypes.object,
  causes: PropTypes.array,
  files: PropTypes.array,
  loading: PropTypes.bool,
  manager: PropTypes.object,
};

export default withTracker((props) => {
  const orgId = props.match.params.id;
  const sub = Meteor.subscribe('organizations.single', orgId);
  const org = Organizations.findOne({ _id: orgId });
  const managerId = org ? org.claimedBy : null;
  const legals = org ? org.legalFiles : [];
  const sub1 = org ? Meteor.subscribe('causes.org', org) : {};
  const sub2 = org ? Meteor.subscribe('files.legal.org', org) : {};
  const sub3 = org ? Meteor.subscribe('users.single', managerId) : {};
  return {
    user: Meteor.user(),
    org,
    causes: Causes.find().fetch(),
    files: Files.find({ _id: { $in: legals } }).fetch(),
    loading: !(!!org && sub1.ready() && sub2.ready() && sub3.ready()),
    manager: Meteor.users.findOne({ _id: managerId }),
  };
})(Single);
