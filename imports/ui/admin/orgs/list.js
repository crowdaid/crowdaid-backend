import React from 'react';
import { Link } from 'react-router-dom';
import { Meteor } from 'meteor/meteor';
import { ReactiveVar } from 'meteor/reactive-var';
import { withTracker } from 'meteor/react-meteor-data';
import PropTypes from 'prop-types';
import Organizations from '/imports/api/organizations/organizations';
import {
  Table, Pagination, Divider, Icon, Button,
} from 'antd/lib';

const PerPage = 10;
const PageNumber = new ReactiveVar(1);

class List extends React.Component {
  constructor(props) {
    super(props);
    this.state = {

    };
  }

  componentDidMount() {
    this.props.setActiveList('orgs');
  }

  render() {
    const dataSource = this.props.orgs.map((org) => {
      org.key = org._id;
      return org;
    });

    const columns = [
      {
			  title: 'Name',
			  dataIndex: 'name.en',
			  key: 'name',
      },
      {
			  title: 'Code',
			  dataIndex: 'registrationCode',
			  key: 'code',
      },
      {
			  title: 'Location',
			  dataIndex: 'HQLocation',
			  key: 'location',
      },
      {
			  title: 'Verification',
			  dataIndex: 'VerificationStatus',
			  key: 'Verification',
      },
      {
        title: 'Actions',
        key: 'actions',
        render: (text, record) => (
          <div>
            <Link to={`/admin/orgs/${record._id}`}>View</Link>
            <Divider type="vertical" />
            <Link to={`/admin/orgs/${record._id}/edit`}>Edit</Link>
            <Divider type="vertical" />
            <a href="javascript:void(0);" onClick={() => this.props.removeOrg(record)}>Delete</a>
          </div>
			  ),
      },
    ];

    return (
      <div className="adminListContainer" style={{ position: 'relative' }}>
        <Link to="/admin/orgs/create">
          <Button type="primary" style={{ position: 'absolute', right: 0, top: '-40px' }} icon="plus-circle-o">
  					New Organization

          </Button>
        </Link>
        <Table pagination={this.props.pagination} dataSource={dataSource} columns={columns} />
      </div>
    );
  }
}

List.propTypes = {
  orgs: PropTypes.array,
  loading: PropTypes.bool,
  orgsReady: PropTypes.bool,
  total: PropTypes.number,
  pagination: PropTypes.object,
};

export default withTracker((props) => {
  const orgsHandle = Meteor.subscribe('organizations.all');
  const orgs = Organizations.find({}, {
  	limit: PerPage, skip: (PageNumber.get() - 1) * PerPage, sort: { createdAt: 'DESC' },
  });

  const totalOrgs = Organizations.find().count();
  const orgsExist = totalOrgs > 0;
  const loading = !orgsHandle.ready();

  return {
    loading,
    orgsReady: orgsHandle.ready(),
    orgs: orgsExist ? orgs.fetch() : [],
    total: totalOrgs,
    pagination: {
  		position: 'bottom',
  		total: totalOrgs,
  		defaultCurrent: 1,
  		pageSize: PerPage,
  		onChange(page, perPage) {
  			PageNumber.set(page);
  		},
  	},
  };
})(List);
