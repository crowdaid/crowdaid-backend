import React from 'react';
import { Meteor } from 'meteor/meteor';
import { withTracker } from 'meteor/react-meteor-data';
import PropTypes from 'prop-types';
import {
  Form, Input, Select, DatePicker, Button, Upload, Icon, Progress,
} from 'antd/lib';
import message from 'antd/lib/message';
import Causes from '/imports/api/causes/causes';
import { createOrganization } from '/imports/api/organizations/methods';
import Files from '/imports/api/files/files';
import { removeFile } from '/imports/api/files/methods';

const FormItem = Form.Item;
const Option = Select.Option;

class Create extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      uploading: false,
      progress: 0,
      loading: false,
      fileList: [],
    };

    this.causes = [];
    this.regDate = '';
    this.fileIds = [];
  }

  handleRegDateChange(moment, dateString) {
    this.regDate = dateString;
  }

  handleSubmit() {
    this.setState({ loading: true });
    const form = document.getElementById('createOrgForm');
    const causes = this.causes;
    const regDate = this.regDate;
    createOrganization.call({
      name: {
		        ar: form.arName.value,
		        en: form.enName.value,
		    },
      about: form.about.value,
      claimedBy: Meteor.userId(),
      registrationCode: form.regCode.value,
      registrationDate: regDate,
      HQLocation: form.HQLocation.value,
      legalFiles: this.fileIds,
      causes,
      userId: form.claimedBy.value,
    }, (err, res) => {
      if (err) {
        // An error occurred
        alert(err);
        this.setState({ loading: false });
      } else {
        this.setState({ loading: false });
        message.success('Organization added successfully');
        this.props.history.push(`/admin/orgs/${res}`);
      }
    });
  }

  uploadLegalFile(event) {
	    const self = this;
	    if (event.file && event.file.status != 'removed') {
      self.setState({ uploading: true, progress: 0 });
	      const file = event.file;
      const uploadInstance = Files.insert({
        file,
        meta: {
          // locator: self.props.fileLocator,
          userId: Meteor.userId(), // Optional, used to check on server for file tampering
          type: 'legal',
        },
        streams: 'dynamic',
        chunkSize: 'dynamic',
        allowWebWorkers: true, // If you see issues with uploads, change this to false
      }, false);

      // Track upload process
      uploadInstance.on('start', () => {
        // Upload started

      });

      uploadInstance.on('progress', (progress, fileObj) => {
        // Upload in progress
        self.setState({
          progress,
        });
      });

      uploadInstance.on('uploaded', (error, fileObj) => {
        // Upload complete
        fileObj.url = Files.link(fileObj);
        fileObj.uid = fileObj._id;
        self.state.fileList.push(fileObj);
        setTimeout(() => {
          self.setState({
	            uploading: false,
            fileList: self.state.fileList,
	          });
        }, 700);
      });

      uploadInstance.on('error', (error, fileObj) => {
        // An error occurred
        console.log(`Error during upload: ${error}`);
      });

      uploadInstance.on('end', (error, fileObj) => {
        // Upload process finished
        self.fileIds.push(fileObj._id);
      });

      // Start the upload
      uploadInstance.start();
	    }
  }

  removeLegalFile(file) {
    removeFile.call({
      fileId: file._id,
      userId: file.userId,
    }, (err, res) => {
      if (err) {
        // An error occurred
        alert(err);
        this.setState({ loading: false });
      } else {
        // message.success("Image deleted");
      }
    });

    this.fileIds.splice(this.fileIds.indexOf(file._id), 1);
    this.setState({
      fileList: $.grep(this.state.fileList, f => f._id != file._id),
    });
  }

  render() {
    const {
      uploading, progress, loading, fileList,
    } = this.state;
    const causes = this.props.causes.map(cause => <Option key={cause._id} defaultValue={cause._id}>{cause.name}</Option>);
    return (
      <div>
        <h2>Create New Organization</h2>
        <Form layout="vertical" id="createOrgForm" style={{ width: '70%' }}>
          <FormItem label="Arabic Name">
            <Input
              id="arName"
              type="text"
              placeholder="Organization Arabic name"
            />
          </FormItem>
          <FormItem label="English Name">
            <Input
              id="enName"
              type="text"
              placeholder="Organization English name"
            />
          </FormItem>
          <FormItem label="Commercial Registration Number">
            <Input
              id="regCode"
              type="text"
              placeholder="Commercial Registration Number"
            />
          </FormItem>
          <FormItem label="Organization Registration Date">
            <DatePicker format="YYYY-MM-DD" id="regDate" onChange={this.handleRegDateChange.bind(this)} />
          </FormItem>
          <FormItem label="Upload Legal Files" extra="Select files to upload">
            {uploading ? <Progress percent={progress} status="active" /> : ''}
            <Upload name="legalFiles" id="legalFiles" fileList={fileList} onChange={this.uploadLegalFile.bind(this)} onRemove={this.removeLegalFile.bind(this)} listType="picture" beforeUpload={() => false}>
              <Button>
                <Icon type="upload" />
                {' '}
Click to upload

              </Button>
            </Upload>
          </FormItem>
          <FormItem label="Head Quarters">
            <Input
              id="HQLocation"
              type="text"
              placeholder="Organization Location"
            />
          </FormItem>
          <FormItem label="Organization About">
            <Input.TextArea
              id="about"
              type="textarea"
              rows={4}
              placeholder="Organization About"
            />
          </FormItem>
          <FormItem label="Causes">
            <Select
              mode="multiple"
              onChange={(value) => { this.causes = value; }}
              placeholder="Please select causes"
              id="causes"
            >
              {causes}
            </Select>
          </FormItem>
          <FormItem>
            <Button key="submit" type="primary" onClick={this.handleSubmit.bind(this)} loading={loading}>Create Organization</Button>
          </FormItem>
          <input
            type="text"
            id="claimedBy"
            defaultValue={this.props.user._id}
            readOnly
            style={{ display: 'none' }}
            placeholder="Claimer Id"
          />
        </Form>
      </div>
    );
  }
}

Create.propTypes = {
  user: PropTypes.object,
  causes: PropTypes.array,
};

export default withTracker((props) => {
  Meteor.subscribe('causes.all');
  return {
    user: Meteor.user(),
    causes: Causes.find().fetch(),
  };
})(Create);
