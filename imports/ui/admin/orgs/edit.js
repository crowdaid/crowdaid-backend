import React from 'react';
import { Meteor } from 'meteor/meteor';
import { moment } from 'meteor/momentjs:moment';
import { withTracker } from 'meteor/react-meteor-data';
import PropTypes from 'prop-types';
import {
  Form, Input, Select, DatePicker, Button, Upload, Modal, Icon, Progress,
} from 'antd/lib';
import message from 'antd/lib/message';
import Organizations from '/imports/api/organizations/organizations';
import { updateOrganization } from '/imports/api/organizations/methods';
import Causes from '/imports/api/causes/causes';
import Files from '/imports/api/files/files';
import { removeFile } from '/imports/api/files/methods';

const FormItem = Form.Item;
const Option = Select.Option;

class Edit extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      uploading: false,
      progress: 0,
      loading: false,
      fileList: props.files,
      ready: false,
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.org && !this.causes) {
      this.causes = nextProps.org.causes;
      this.regDate = nextProps.org.registrationDate;
      this.fileIds = nextProps.org.legalFiles;
    }

    this.setState({
      ready: !nextProps.loading,
      fileList: nextProps.files,
	  });
    console.log('Meteor data received');
  }

  handleRegDateChange(date, dateString) {
    this.regDate = dateString;
  }

  handleSubmit() {
    this.setState({ loading: true });
    const form = document.getElementById('editOrgForm');
    const orgId = this.props.org._id;
    const causes = this.causes;
    const regDate = this.regDate;
    const fileIds = this.fileIds;
    updateOrganization.call({
      _id: orgId,
      name: {
		        ar: form.arName.value,
		        en: form.enName.value,
		    },
      about: form.about.value,
      claimedBy: Meteor.userId(),
      registrationCode: form.regCode.value,
      registrationDate: regDate,
      HQLocation: form.HQLocation.value,
      legalFiles: fileIds,
      causes,
      userId: form.claimedBy.value,
    }, (err, res) => {
      if (err) {
        // An error occurred
        alert(err);
        this.setState({ loading: false });
      } else {
        this.setState({ loading: false });
        message.success('Organization updated successfully');
        if (this.props.from && this.props.from == 'profile') {
          this.props.history.push(`/profile/org/${this.props.org._id}`);
        } else {
          this.props.history.push(`/admin/orgs/${this.props.org._id}`);
        }
      }
    });
  }

  uploadLegalFile(event) {
    const self = this;
    if (event.file && event.file.status != 'removed') {
      self.setState({ uploading: true, progress: 0 });
      const file = event.file;
      const uploadInstance = Files.insert({
        file,
        meta: {
          // locator: self.props.fileLocator,
          userId: Meteor.userId(), // Optional, used to check on server for file tampering
          type: 'legal',
        },
        streams: 'dynamic',
        chunkSize: 'dynamic',
        allowWebWorkers: true, // If you see issues with uploads, change this to false
      }, false);

      // Track upload process
      uploadInstance.on('start', () => {
        // Upload started

      });

      uploadInstance.on('progress', (progress, fileObj) => {
        // Upload in progress
        self.setState({
          progress,
        });
      });

      uploadInstance.on('uploaded', (error, fileObj) => {
        // Upload complete
        fileObj.url = Files.link(fileObj);
        fileObj.uid = fileObj._id;
        self.state.fileList.push(fileObj);
        setTimeout(() => {
          self.setState({
            uploading: false,
            fileList: self.state.fileList,
          });
        }, 700);
      });

      uploadInstance.on('error', (error, fileObj) => {
        // An error occurred
        console.log(`Error during upload: ${error}`);
      });

      uploadInstance.on('end', (error, fileObj) => {
        // Upload process finished
        self.fileIds.push(fileObj._id);
      });

      // Start the upload
      uploadInstance.start();
    }
  }

  removeLegalFile(file) {
    removeFile.call({
      fileId: file._id,
      userId: file.userId,
      orgId: this.props.org._id,
    }, (err, res) => {
      if (err) {
        // An error occurred
        alert(err);
      } else {
        // message.success("Image deleted");
      }
    });

    this.fileIds.splice(this.fileIds.indexOf(file._id), 1);
    this.setState({
      fileList: $.grep(this.state.fileList, f => f._id != file._id),
    });
  }

  render() {
    const {
      uploading, progress, loading, fileList, ready,
    } = this.state;
    const causes = this.props.causes.map(cause => <Option key={cause._id} defaultValue={cause._id}>{cause.name}</Option>);
    return (
      <div>
        { !ready ? 'Loading'
          : (
            <Form layout="vertical" id="editOrgForm" style={{ width: '70%' }}>
              <h2>{`Edit Organization: ${this.props.org.name.en}`}</h2>
              <FormItem label="Arabic Name">
                <Input
                  id="arName"
                  type="text"
                  defaultValue={this.props.org.name.ar}
                  placeholder="Organization Arabic name"
                />
              </FormItem>
              <FormItem label="English Name">
                <Input
                  id="enName"
                  type="text"
                  defaultValue={this.props.org.name.en}
                  placeholder="Organization English name"
                />
              </FormItem>
              <FormItem label="Registration Code">
                <Input
                  id="regCode"
                  type="text"
                  defaultValue={this.props.org.registrationCode}
                  placeholder="Organization Registration Code"
                />
              </FormItem>
              <FormItem label="Organization Registration Date">
                <DatePicker format="YYYY-MM-DD" id="regDate" defaultValue={moment(this.regDate)} onChange={this.handleRegDateChange.bind(this)} />
              </FormItem>
              <FormItem label="Upload Legal Files" extra="Select files to upload">
                {uploading ? <Progress percent={progress} status="active" /> : ''}
                <Upload name="legalFiles" id="legalFiles" fileList={fileList} onChange={this.uploadLegalFile.bind(this)} onRemove={this.removeLegalFile.bind(this)} listType="picture" beforeUpload={() => false}>
                  <Button>
                    <Icon type="upload" />
                    {' '}
Click to upload

                  </Button>
                </Upload>
              </FormItem>
              <FormItem label="Head Quarters">
                <Input
                  id="HQLocation"
                  type="text"
                  defaultValue={this.props.org.HQLocation}
                  placeholder="Organization Location"
                />
              </FormItem>
              <FormItem label="Organization About">
                <Input.TextArea
                  id="about"
                  type="textarea"
                  defaultValue={this.props.org.about}
                  rows={4}
                  placeholder="Organization About"
                />
              </FormItem>
              <FormItem label="Causes">
                <Select
                  mode="multiple"
                  defaultValue={this.causes}
                  onChange={(value) => { this.causes = value; }}
                  placeholder="Please select causes"
                  id="causes"
                >
                  {causes}
                </Select>
              </FormItem>
              <FormItem>
                <Button key="submit" type="primary" onClick={this.handleSubmit.bind(this)} loading={loading}>Save Changes</Button>
              </FormItem>
              <input
                type="text"
                id="claimedBy"
                defaultValue={this.props.org.claimedBy}
                readOnly
                style={{ display: 'none' }}
                placeholder="Claimer Id"
              />
            </Form>
          )
				}
      </div>
    );
  }
}

Edit.propTypes = {
  user: PropTypes.object,
  org: PropTypes.object,
  causes: PropTypes.array,
  files: PropTypes.array,
  loading: PropTypes.bool,
};

export default withTracker((props) => {
  const orgId = props.match.params.id;
  const sub = Meteor.subscribe('organizations.single', orgId);
  const sub1 = Meteor.subscribe('causes.all');
  const org = Organizations.findOne({ _id: orgId });
  const legals = org ? org.legalFiles : [];
  const sub2 = org ? Meteor.subscribe('files.legal.org', org) : {};
  return {
    user: Meteor.user(),
    org,
    causes: Causes.find().fetch(),
    files: Files.find({ _id: { $in: legals } }).map((file) => {
      const obj = file;
      obj.url = Files.link(file);
      obj.uid = file._id;
      return obj;
    }),
    loading: !(!!org && sub1.ready() && sub2.ready()),
  };
})(Edit);
