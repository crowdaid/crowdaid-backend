import React from 'react';
import { Switch, Route } from 'react-router-dom';
import { Modal } from 'antd/lib';
import List from './list';
import Single from './single';
import Create from './create';
import Edit from './edit';
import { removeOrganization } from '/imports/api/organizations/methods';

export default class OrgsList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {

    };
  }

  componentDidMount() {
    this.props.setActiveList('orgs');
  }

  removeOrg(org) {
    const history = this.history;
    Modal.confirm({
      title: `Delete organization ${org.name.en}`,
      description: 'Are your sure you want to delete this organization?',
      okText: 'Delete',
      okType: 'danger',
      onOk() {
        removeOrganization.call({
          organizationId: org._id,
          userId: org.userId,
        }, (err, result) => {
          if (err) {
            alert(err);
          } else {
            history.push('/admin/orgs');
          }
        });
      },
      onCancel() {

      },
    });
  }

  render() {
    const base = '/admin/orgs';
    return (
      <Switch>
        <Route exact path={base} render={() => (<List {...this.props} removeOrg={this.removeOrg} />)} />
        <Route path="/admin/orgs/create" render={props => (<Create {...props} />)} />
        <Route path="/admin/orgs/:id/edit" render={props => (<Edit {...props} />)} />
        <Route path="/admin/orgs/:id" render={props => (<Single {...props} removeOrg={this.removeOrg} />)} />
        <Route render={() => (<div>No Match</div>)} />
      </Switch>
    );
  }
}
