import React from 'react';
import { Meteor } from 'meteor/meteor';
import { withTracker } from 'meteor/react-meteor-data';
import PropTypes from 'prop-types';
import {
  Form, Input, Button, Icon, message,
} from 'antd/lib';
import Causes from '/imports/api/causes/causes';
import { updateCause } from '/imports/api/causes/methods';

const FormItem = Form.Item;

class Edit extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      ready: false,
    };
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      ready: !nextProps.loading,
    });
  }

  handleSubmit() {
    this.setState({ loading: true });
    const form = document.getElementById('editCauseForm');
    updateCause.call({
      _id: this.props.cause._id,
      name: form.name.value,
      description: form.description.value,
      userId: this.props.user._id,
    }, (err, res) => {
      if (err) {
        // An error occurred
        this.setState({ loading: false });
        alert(err);
      } else {
        this.setState({ loading: false });
        message.success('Cause updated successfully');
        this.props.history.push(`/admin/causes/${this.props.cause._id}`);
      }
    });
  }

  render() {
    const { loading } = this.state;
    return (
      <Form layout="vertical" id="editCauseForm">
        <FormItem label="Cause Title">
          <Input
            id="name"
            type="text"
            defaultValue={this.props.cause.name}
            placeholder="Cause Title"
          />
        </FormItem>
        <FormItem label="Cause Description">
          <Input.TextArea
            id="description"
            type="textarea"
            defaultValue={this.props.cause.description}
            rows={4}
            placeholder="Cause Description"
          />
        </FormItem>
        <FormItem>
          <Button key="submit" type="primary" onClick={this.handleSubmit.bind(this)} loading={loading}>Save Changes</Button>
        </FormItem>
      </Form>
    );
  }
}

Edit.propTypes = {
  user: PropTypes.object,
  cause: PropTypes.object,
  loading: PropTypes.bool,
};

export default withTracker((props) => {
  const id = props.match.params.id;
  const sub = Meteor.subscribe('causes.single', id);
  return {
    user: Meteor.user(),
    cause: Causes.findOne({ _id: id }),
    loading: !sub.ready(),
  };
})(Edit);
