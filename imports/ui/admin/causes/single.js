import React from 'react';
import { Link } from 'react-router-dom';
import { Meteor } from 'meteor/meteor';
import { withTracker } from 'meteor/react-meteor-data';
import PropTypes from 'prop-types';
import Button from 'antd/lib/button';
import Causes from '/imports/api/causes/causes';

class Single extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      ready: props.ready,
    };
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      ready: nextProps.ready,
	  });
  }

  render() {
    const ready = this.state.ready;
    return (
      <div className="single-view cause-view posrel">
        { !ready ? <div>Loading</div>
          : (
            <div>
              <div className="single-view-actions">
                <Link to={`/admin/causes/${this.props.cause._id}/edit`}>
                  <Button type="default" icon="edit">
								Edit

                  </Button>
                </Link>
						&nbsp;
                <Button type="danger" icon="delete" onClick={() => { this.props.removeCause(this.props.cause); }}>
							Delete

                </Button>
              </div>
              <table>
                <thead>
                  <tr>
                    <td colSpan="2">
                      <h2> Cause details </h2>
                    </td>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>Cause Title</td>
                    <td>{this.props.cause.name}</td>
                  </tr>
                  <tr>
                    <td>Cause Description</td>
                    <td>{this.props.cause.description}</td>
                  </tr>
                </tbody>
              </table>
            </div>
          )
			}
      </div>
    );
  }
}

Single.propTypes = {
  user: PropTypes.object,
  cause: PropTypes.object,
  ready: PropTypes.bool,
};

export default withTracker((props) => {
  const id = props.match.params.id;
  const sub = Meteor.subscribe('causes.single', id);
  const cause = Causes.findOne({ _id: id });
  return {
    user: Meteor.user(),
    cause,
    ready: sub.ready(),
  };
})(Single);
