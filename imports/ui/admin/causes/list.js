import React from 'react';
import { Link } from 'react-router-dom';
import { Meteor } from 'meteor/meteor';
import { ReactiveVar } from 'meteor/reactive-var';
import { withTracker } from 'meteor/react-meteor-data';
import PropTypes from 'prop-types';
import Causes from '/imports/api/causes/causes';
import {
  Table, Pagination, Divider, Icon, Button,
} from 'antd/lib';

const PerPage = 10;
const PageNumber = new ReactiveVar(1);

class List extends React.Component {
  constructor(props) {
    super(props);
    this.state = {

    };
  }

  componentDidMount() {
    this.props.setActiveList('causes');
  }

  render() {
    const dataSource = this.props.causes.map((cause) => {
      cause.key = cause._id;
      return cause;
    });

    const columns = [
      {
			  title: 'Name',
			  dataIndex: 'name',
			  key: 'name',
      },
      {
			  title: 'Description',
			  dataIndex: 'description',
			  key: 'description',
      },
      {
        title: 'Actions',
        key: 'actions',
        render: (text, record) => (
          <div>
            <Link to={`/admin/causes/${record._id}`}>View</Link>
            <Divider type="vertical" />
            <Link to={`/admin/causes/${record._id}/edit`}>Edit</Link>
            <Divider type="vertical" />
            <a href="javascript:void(0);" onClick={() => this.props.removeCause(record)}>Delete</a>
          </div>
			  ),
      },
    ];

    return (
      <div className="adminListContainer" style={{ position: 'relative' }}>
        <Link to="/admin/causes/create">
          <Button type="primary" style={{ position: 'absolute', right: 0, top: '-40px' }} icon="plus-circle-o">
            New Cause
          </Button>
        </Link>
        <Table pagination={this.props.pagination} dataSource={dataSource} columns={columns} />
      </div>
    );
  }
}

List.propTypes = {
  causes: PropTypes.array,
  loading: PropTypes.bool,
  causesReady: PropTypes.bool,
  total: PropTypes.number,
  pagination: PropTypes.object,
};

export default withTracker((props) => {
  const causesHandle = Meteor.subscribe('causes.all');
  const causes = Causes.find({}, {
  	limit: PerPage, skip: (PageNumber.get() - 1) * PerPage, sort: { createdAt: 'DESC' },
  });

  const totalCauses = Causes.find().count();
  const causesExist = totalCauses > 0;
  const loading = !causesHandle.ready();

  return {
    loading,
    causesReady: causesHandle.ready(),
    causes: causesExist ? causes.fetch() : [],
    total: totalCauses,
    pagination: {
  		position: 'bottom',
  		total: totalCauses,
  		defaultCurrent: 1,
  		pageSize: PerPage,
  		onChange(page, perPage) {
  			PageNumber.set(page);
  		},
  	},
  };
})(List);
