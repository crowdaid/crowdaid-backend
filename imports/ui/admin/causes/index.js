import React from 'react';
import { Switch, Route } from 'react-router-dom';
import { Modal, message } from 'antd/lib';
import List from './list';
import Single from './single';
import Create from './create';
import Edit from './edit';
import { removeCause } from '/imports/api/causes/methods';

export default class CausesList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {

    };
  }

  componentDidMount() {
    this.props.setActiveList('causes');
  }

  removeCause(cause) {
    const history = this.history;
    Modal.confirm({
      title: `Delete cause ${cause.name}`,
      description: 'Are your sure you want to delete this cause?',
      okText: 'Delete',
      okType: 'danger',
      onOk() {
        removeCause.call({
          _id: cause._id,
          userId: cause.userId,
        }, (err, result) => {
          if (err) {
            console.log(err);
          } else {
            message.success('Cause deleted successfully');
            history.push('/admin/causes');
          }
        });
      },
      onCancel() {

      },
    });
  }

  render() {
    const base = '/admin/causes';
    return (
      <Switch>
        <Route exact path={base} render={() => (<List {...this.props} removeCause={this.removeCause} />)} />
        <Route path={`${base}/create`} render={props => (<Create {...props} />)} />
        <Route path={`${base}/:id/edit`} render={props => (<Edit {...props} />)} />
        <Route path={`${base}/:id`} render={props => (<Single {...props} removeCause={this.removeCause} />)} />
        <Route render={() => (<div>No Match</div>)} />
      </Switch>
    );
  }
}
