import React from 'react';
import { Meteor } from 'meteor/meteor';
import PropTypes from 'prop-types';
import {
  Form, Input, Button, message,
} from 'antd/lib';
import { createCause } from '/imports/api/causes/methods';

const FormItem = Form.Item;

export default class Create extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
    };
  }

  handleSubmit() {
    this.setState({ loading: true });
    const form = document.getElementById('createCauseForm');
    createCause.call({
      name: form.name.value,
      description: form.description.value,
      userId: Meteor.userId(),
    }, (err, res) => {
      if (err) {
        // An error occurred
        this.setState({ loading: false });
        alert(err);
      } else {
        this.setState({ loading: false });
        message.success('Cause added successfully');
        this.props.history.push(`/admin/causes/${res}`);
      }
    });
  }

  render() {
    // const causes = [];
    const { loading } = this.state;
    return (
      <Form layout="vertical" id="createCauseForm">
        <FormItem label="Cause Title">
          <Input
            id="name"
            type="text"
            placeholder="Cause Title"
          />
        </FormItem>
        <FormItem label="Cause Description">
          <Input.TextArea
            id="description"
            type="textarea"
            rows={4}
            placeholder="Cause Description"
          />
        </FormItem>
        <FormItem>
          <Button key="submit" type="primary" onClick={this.handleSubmit.bind(this)} loading={loading}>Create Cause</Button>
        </FormItem>
      </Form>
    );
  }
}
