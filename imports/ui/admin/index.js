import React from 'react';
import LocaleProvider from 'antd/lib/locale-provider';
import enUS from 'antd/lib/locale-provider/en_US';
import Layout from 'antd/lib/layout';
import Menu from 'antd/lib/menu';
import Breadcrumb from 'antd/lib/breadcrumb';
import Icon from 'antd/lib/icon';
import Button from 'antd/lib/button';
import { Switch, Route } from 'react-router-dom';
import { AccountsReact } from 'meteor/meteoreact:accounts';
import UsersList from './users/index';
import OrgsList from './orgs/index';
import CausesList from './causes/index';

const { SubMenu } = Menu;
const { Header, Content, Sider } = Layout;

export default class AdminLayout extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      activeList: 'users',
    };
  }

  setActiveList(list) {
    if (list == this.state.activeList) return;
    this.setState({ activeList: list });
  }

  linkTo(item) {
    this.props.history.push(`/admin/${item.key}`);
    // this.setState({activeList: item.key});
  }

  render() {
    const { activeList } = this.state;
    return (
      <div>
        <LocaleProvider locale={enUS}>
          <div className="admin-container">
            <Layout>
              <div id="modalContainer" />
              <Header className="header admin-header">
                <div className="logo admin-logo">Crowd Aid</div>
                <Menu
                  theme="dark"
                  mode="horizontal"
                  defaultSelectedKeys={['1']}
                  style={{ lineHeight: '64px', textAlign: 'right' }}
                >
                  <Menu.Item key="1">nav 1</Menu.Item>
                  <Menu.Item key="2">nav 2</Menu.Item>
                  <Menu.Item key="3">nav 3</Menu.Item>
                  <Menu.Item key="logout">
                    <Button type="primary" size="large" icon="logout" onClick={() => { AccountsReact.logout(); }}>
                      Logout
                    </Button>
                  </Menu.Item>
                </Menu>
              </Header>
              <Layout>
                <Sider width={200} style={{ background: '#fff' }}>
                  <Menu
                    mode="inline"
                    defaultSelectedKeys={['/amdin/users']}
                    defaultOpenKeys={['/admin/users']}
                    selectedKeys={[activeList]}
                    onClick={this.linkTo.bind(this)}
                    style={{ height: '100%', borderRight: 0 }}
                  >
                    <Menu.Item key="users">
                      <span>
                        <Icon type="user" />
Users
                      </span>
                    </Menu.Item>
                    <Menu.Item key="orgs">
                      <span>
                        <Icon type="user" />
Charities
                      </span>
                    </Menu.Item>
                    <Menu.Item key="causes">
                      <span>
                        <Icon type="user" />
Causes
                      </span>
                    </Menu.Item>
                    <SubMenu
                      key="sub1"
                      title={(
                        <span>
                          <Icon type="user" />
subnav 1
                        </span>
)}
                    >
                      <Menu.Item key="1">option1</Menu.Item>
                      <Menu.Item key="2">option2</Menu.Item>
                      <Menu.Item key="3">option3</Menu.Item>
                      <Menu.Item key="4">option4</Menu.Item>
                    </SubMenu>
                    <SubMenu
                      key="sub2"
                      title={(
                        <span>
                          <Icon type="laptop" />
subnav 2
                        </span>
)}
                    >
                      <Menu.Item key="5">option5</Menu.Item>
                      <Menu.Item key="6">option6</Menu.Item>
                      <Menu.Item key="7">option7</Menu.Item>
                      <Menu.Item key="8">option8</Menu.Item>
                    </SubMenu>
                    <SubMenu
                      key="sub3"
                      title={(
                        <span>
                          <Icon type="notification" />
subnav 3
                        </span>
)}
                    >
                      <Menu.Item key="9">option9</Menu.Item>
                      <Menu.Item key="10">option10</Menu.Item>
                      <Menu.Item key="11">option11</Menu.Item>
                      <Menu.Item key="12">option12</Menu.Item>
                    </SubMenu>
                  </Menu>
                </Sider>
                <Layout style={{ padding: '0 24px 24px' }}>
                  <Breadcrumb style={{ margin: '16px 0' }}>
                    <Breadcrumb.Item>Home</Breadcrumb.Item>
                    <Breadcrumb.Item>List</Breadcrumb.Item>
                    <Breadcrumb.Item>App</Breadcrumb.Item>
                  </Breadcrumb>
                  <Content style={{
                    background: '#fff', padding: 24, margin: 0, minHeight: 280,
                  }}
                  >
                    <Switch>
                      <Route path="/admin/users" render={() => (<UsersList {...this.props} setActiveList={this.setActiveList.bind(this)} />)} />
                      <Route path="/admin/orgs" render={() => (<OrgsList {...this.props} setActiveList={this.setActiveList.bind(this)} />)} />
                      <Route path="/admin/causes" render={() => (<CausesList {...this.props} setActiveList={this.setActiveList.bind(this)} />)} />
                    </Switch>
                  </Content>
                </Layout>
              </Layout>
            </Layout>
          </div>
        </LocaleProvider>
      </div>
    );
  }
}
