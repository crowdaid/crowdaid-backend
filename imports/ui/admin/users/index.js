import React from 'react';
import ReactDOM from 'react-dom';
import { Meteor } from 'meteor/meteor';
import { ReactiveVar } from 'meteor/reactive-var';
import { withTracker } from 'meteor/react-meteor-data';
import PropTypes from 'prop-types';
import {
  Table, Pagination, Modal, message,
} from 'antd/lib';
import { isAdmin } from '/imports/api/utils';
import { makeAdmin } from '/imports/api/users/methods';
import { removeAdmin } from '/imports/api/users/methods';

const PerPage = 1;
const PageNumber = new ReactiveVar(1);

class UsersList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {

    };
  }

  componentDidMount() {
    this.props.setActiveList('users');
  }

  makeAdmin(user) {
    Modal.confirm({
      title: `Make ${user.username} an admin?`,
      content: 'This user will have all admin privileges.',
      onOk() {
        makeAdmin.call({
          userId: user._id,
        }, (err, res) => {
          if (err) {
            alert(err);
          } else {
            message.success('Admin created successfully');
          }
        });
      },
      onCancel() {

      },
    });
  }

  removeAdmin(user) {
    Modal.confirm({
	    title: `Remove admin role for ${user.username} ?`,
	    content: 'This user will no longer have admin privileges.',
	    onOk() {
        removeAdmin.call({
		      userId: user._id,
		    }, (err, res) => {
		      if (err) {
		        alert(err);
		      } else {
		        message.success('Admin removed successfully');
		      }
		    });
	    },
	    onCancel() {

	    },
	  });
  }

  render() {
    const dataSource = this.props.users.map((user) => {
      user.key = user._id;
      return user;
    });

    const columns = [
      {
			  title: 'Username',
			  dataIndex: 'username',
			  key: 'username',
      },
      {
			  title: 'Email Address',
			  dataIndex: 'emails[0].address',
			  key: 'email',
      },
      {
        title: 'Actions',
        key: 'actions',
        render: (text, record) => (
          <span>
            {isAdmin(record)
              ? <a href="javascript:void(0);" onClick={() => this.removeAdmin(record)}>Remove Admin</a>
              : <a href="javascript:void(0);" onClick={() => this.makeAdmin(record)}>Make Admin</a>
						}
          </span>
        ),
      },
    ];

    return (
      <Table pagination={this.props.pagination} dataSource={dataSource} columns={columns} />
    );
  }
}

UsersList.propTypes = {
  users: PropTypes.array,
  usersReady: PropTypes.bool,
  loading: PropTypes.bool,
  total: PropTypes.number,
  pagination: PropTypes.object,
};

export default withTracker((props) => {
  const usersHandle = Meteor.subscribe('usersList');
  const users = Meteor.users.find({}, {
  	limit: PerPage, skip: (PageNumber.get() - 1) * PerPage, sort: { _id: 'DESC' },
  });

  const totalUsers = Meteor.users.find().count();
  const usersExist = totalUsers > 0;
  const loading = !usersHandle.ready();
  return {
    loading,
    usersReady: usersHandle.ready(),
    users: usersExist ? users.fetch() : [],
    total: totalUsers,
    pagination: {
      position: 'bottom',
      total: totalUsers,
      defaultCurrent: 1,
      pageSize: PerPage,
      onChange(page, perPage) {
        PageNumber.set(page);
      },
    },
  };
})(UsersList);
