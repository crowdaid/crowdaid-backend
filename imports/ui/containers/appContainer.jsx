/**
 * Created by ahmad on 6/14/17.
 */

import { Meteor } from 'meteor/meteor';
import { withTracker } from 'meteor/react-meteor-data';

import App from '../layouts/App.jsx';

export default withTracker(props => {
  const loggedUser = Meteor.subscribe('loggedUser');
  console.log('sub', loggedUser);
  return {
    user: Meteor.user(),
    currentUserReady: loggedUser.ready(),
    connected: Meteor.status().connected,
  };
})(App);
