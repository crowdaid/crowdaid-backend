/**
 * Created by ahmad on 6/23/17.
 */
import React from 'react';
import { Redirect } from 'react-router-dom';
import { AccountsReactComponent } from 'meteor/meteoreact:accounts';

const Join = (props) => {
  const { from } = props.location.state || { from: { pathname: '/' } };
  if (props.user) {
    return (
      <Redirect to="/" />
    );
  }

  return (
    <div>
      <span>Content</span>
      <h1>Join Page </h1>
      <div>
        <AccountsReactComponent
          history={props.history}
          state="signUp"
        />
      </div>
    </div>
  );
};

export default Join;
