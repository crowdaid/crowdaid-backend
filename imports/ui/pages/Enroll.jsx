/**
 * Created by ahmad on 6/23/17.
 */
import React from 'react';
import Meteor from 'meteor/meteor';
import { Redirect } from 'react-router-dom';
import { AccountsReactComponent } from 'meteor/meteoreact:accounts';

const Enroll = (props) => {
  return (
    <div>
      <span>Content</span>
      <h1>Set Account Password </h1>
      <div>
        <AccountsReactComponent
          history={props.history}
          state="enrollAccount"
          token={props.match.params.token}
        />
      </div>
    </div>
  );
};

export default Enroll;
