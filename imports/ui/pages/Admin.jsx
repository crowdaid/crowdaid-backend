/**
 * Created by ahmad on 6/23/17.
 */
import React from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import { Redirect } from 'react-router-dom';
import { withTracker } from 'meteor/react-meteor-data';
import { isAdmin } from '/imports/api/utils';
import Spin from 'antd/lib/spin';
import AdminLayout from '../admin/index';

class Admin extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: !this.props.currentUserReady,
    };
  }

  removeModal() {
    setTimeout(function() {
      ReactDOM.unmountComponentAtNode(document.getElementById('modalContainer'));
    }, 200);
  }

  render() {

    if (this.props.userReady) {
      console.log(this.props.user);
      if (this.props.user && isAdmin(this.props.user._id)) {
        return (
          <AdminLayout {...this.props} removeModal={this.removeModal} />
        );
      }

      return (
        <Redirect to="/" />
      );
    }

    return (
      <Spin size="large" />
    );
  }
}

Admin.propTypes = {
  user: PropTypes.object,
  userReady: PropTypes.bool,
}

export default withTracker(props => {
  const userHandle = Meteor.subscribe('loggedUser');
  return {
    user: Meteor.user(),
    userReady: userHandle.ready(),
  }
})(Admin);
