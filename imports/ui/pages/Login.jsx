/**
 * Created by ahmad on 6/23/17.
 */
import React from 'react';
import Meteor from 'meteor/meteor';
import { Redirect } from 'react-router-dom';
import { AccountsReactComponent } from 'meteor/meteoreact:accounts';

const Login = (props) => {
  const { from } = props.location.state || { from: { pathname: '/' } };
  if (props.user) {
    return (
      <Redirect to="/" />
    );
  }

  return (
    <div>
      <span>Content</span>
      <h1>Login Page </h1>
      <div>
        <AccountsReactComponent
          history={props.history}
          state="signIn"
        />
      </div>
    </div>
  );
};

export default Login;
