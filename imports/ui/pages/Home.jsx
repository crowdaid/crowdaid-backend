/**
 * Created by ahmad on 6/23/17.
 */
import React from 'react';
import Spin from 'antd/lib/spin';
import GuestHome from './GuestHome';
import Feed from '../public/cases/feed';

const Home = (props) => {
  if (props.currentUserReady) {
    if (props.user) {
      return (
        <Feed {...props} />
      );
    }
    return (
      <GuestHome {...props} />
    );
  }

  return (
    <Spin size="large" />
  );
};

export default Home;
