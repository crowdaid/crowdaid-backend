/**
 * Created by ahmad on 6/23/17.
 */
import React from 'react';
import { withTracker } from 'meteor/react-meteor-data';
import PropTypes from 'prop-types';
import Organizations from '/imports/api/organizations/organizations';
import LocaleProvider from 'antd/lib/locale-provider';
import enUS from 'antd/lib/locale-provider/en_US';
import Layout from 'antd/lib/layout';
import Menu from 'antd/lib/menu';
import Icon from 'antd/lib/icon';
import Button from 'antd/lib/button';
import Spin from 'antd/lib/spin';
import { Switch, Route, Redirect } from 'react-router-dom';

import AppHeader from '../components/AppHeader';
import Dashboard from '../profile/index';

import MyCases from '../profile/user/cases';
import MyDonations from '../profile/user/donations/list';
import EditDonation from '../profile/user/donations/edit';
import DonationDetails from '../profile/user/donations/details';
import DonationCheckout from '../profile/user/donations/checkout';

import Organization from '../profile/org/index';

import ProfileInfo from '../profile/user/details';
import EditProfile from '../profile/user/edit';
import UserEmails from '../profile/user/emails';
import Password from '../profile/user/password';

const { SubMenu } = Menu;
const { Header, Content, Sider } = Layout;

class UserProfile extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      activeList: 'dashboard',
    }
  }

  setActiveList(list) {
    if (list == this.state.activeList) return;
    this.setState({activeList: list});
  }

  linkTo(item) {
    this.props.history.push("/profile/" + item.key);
    // this.setState({activeList: item.key});
  }

  renderOrgs() {
    return this.props.orgs.map(org => <Menu.Item key={'org/' + org._id}>{org.name.en}</Menu.Item>);
  }

  render() {
    if (this.props.userReady) {
      if ( ! this.props.user) {
        return <Redirect to="/" />
      }
    } else {
      return <Spin size="large" />
    }

    const { activeList } = this.state;
    return (
      <Layout>
        <Sider width={200} style={{ background: '#fff' }}>
          <Menu
            mode="inline"
            defaultSelectedKeys={['dashboard']}
            defaultOpenKeys={['']}
            selectedKeys={[activeList]}
            onClick={this.linkTo.bind(this)}
            style={{ height: '100%' }}
          >
            <Menu.Item key="dashboard">
              <span><Icon type="dashboard" />Dashboard</span>
            </Menu.Item>
            <Menu.Item key="cases">
              <span><Icon type="solution" />My Cases</span>
            </Menu.Item>
            <Menu.Item key="donations">
              <span><Icon type="pay-circle" />My Donations</span>
            </Menu.Item>
            <SubMenu key="orgs" title={<span><Icon type="team" />Organizations</span>}>
              {this.renderOrgs()}
            </SubMenu>
            <Menu.Item key="info">
              <span><Icon type="profile" />Profile info</span>
            </Menu.Item>
            <Menu.Item key="edit">
              <span><Icon type="edit" />Edit profile</span>
            </Menu.Item>
            <Menu.Item key="emails">
              <span><Icon type="mail" />Email addresses</span>
            </Menu.Item>
            <Menu.Item key="password">
              <span><Icon type="lock" />Change password</span>
            </Menu.Item>
            <Menu.Item key="settings">
              <span><Icon type="setting" />Account settings</span>
            </Menu.Item>
          </Menu>
        </Sider>
        <Layout>
          <Content style={{ background: '#fff', paddingLeft: '20px', margin: 0 }}>
            <Switch>
              <Route exact path="/profile" render={() => (<Dashboard />)} />
              <Route path="/profile/dashboard" render={() => (<Dashboard />)} />
              <Route path="/profile/cases" render={(props) => (<MyCases {...props} />)} />
              <Route exact path="/profile/donations" render={(props) => (<MyDonations {...props} />)} />
              <Route exact path="/profile/donations/:did" render={(props) => (<DonationDetails {...props} />)} />
              <Route exact path="/profile/donations/:did/edit" render={(props) => (<EditDonation {...props} />)} />
              <Route exact path="/profile/donations/:did/checkout" render={(props) => (<DonationCheckout {...props} />)} />
              <Route path="/profile/info" render={(props) => (<ProfileInfo {...props} />)} />
              <Route path="/profile/edit" render={(props) => (<EditProfile {...props} />)} />
              <Route path="/profile/emails" render={(props) => (<UserEmails {...props} />)} />
              <Route path="/profile/password" render={(props) => (<Password {...props} />)} />
              <Route path="/profile/org/:id" render={(props) => (<Organization {...props} />)} />
            </Switch>
          </Content>
        </Layout>
      </Layout>
    );
  }
}

UserProfile.propTypes = {
  user: PropTypes.object,
  userReady: PropTypes.bool,
  orgsReady: PropTypes.bool,
  orgs: PropTypes.array,
}

export default withTracker(props => {
  const userId = Meteor.userId();
  const userHandle = Meteor.subscribe('loggedUser');
  const orgsHandle = Meteor.subscribe('user.organizations', userId);
  const orgs = Organizations.find({$or: [{claimedBy: userId}, {employees: userId}]});
  return {
    user: Meteor.user(),
    userReady: userHandle.ready(),
    orgsReady: orgsHandle.ready(),
    orgs: orgs.fetch(),
  }
})(UserProfile);
