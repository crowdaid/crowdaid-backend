/**
 * Created by ahmad on 6/23/17.
 */
import React from 'react';
import Spin from 'antd/lib/spin';
import Alert from 'antd/lib/alert';
import { Accounts } from 'meteor/accounts-base';

export default class VerifyEmail extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      verifying: true,
      error: false,
      errMessage: "",
    }
  }

  componentDidMount() {
    Accounts.verifyEmail(this.props.match.params.token, (error) => {
      if (error) {
        console.log('email verification error', error);
        this.setState({error: true, errMessage: error.reason, verifying: false});
      } else {
        console.log('email verification success');
        this.setState({error: false, verifying: false});
        // this.props.history.push('/profile/emails');
      }
    });
  }

  render() {
    const { verifying, error, errMessage } = this.state;
    return (
      <div style={{position: 'absolute', top: '50%', left: '50%', textAlign: 'center'}}>
        {verifying ?
          <div>
            <Spin size="large" />
            <h3>Verifying Email</h3>
          </div>: ''
        }
        {error ? <Alert message={errMessage} type="error" showIcon />: ''}
        {!verifying && !error ?
          <Alert message="Email address verified" type="success" showIcon /> : ''
        }
      </div>
    );
  }
}
