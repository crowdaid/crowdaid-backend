import React from 'react'
import SingleCase from '/imports/ui/profile/org/cases/single'

const CaseDonors = (props) => {
  return <SingleCase mode="feed" target="donors" {...props} />
}

export default CaseDonors
