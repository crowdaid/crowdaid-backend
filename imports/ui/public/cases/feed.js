import React from 'react';
import { Link } from 'react-router-dom';
import { Meteor } from 'meteor/meteor';
import { withTracker } from 'meteor/react-meteor-data';
import { ReactiveVar } from 'meteor/reactive-var';
import Cases from '/imports/api/cases/cases';
import Causes from '/imports/api/causes/causes';
import { Card, Row, Col, Icon, Select, Button, Divider } from 'antd/lib';

const Option = Select.Option;
const Cause = new ReactiveVar();

class Feed extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      ready: false,
    }
  }

  componentWillReceiveProps(props) {
    this.setState({ ready: !props.loading });
  }

  renderCauses(causes) {
    return causes.toString();
  }

  caseActions(casee) {
    return [
      <Link to={"/cases/" + casee._id + "/needs"}><Icon type="profile" /> View needs</Link>,
      <Link to={"/cases/" + casee._id + "/donors"}><Icon type="credit-card" /> View donors</Link>,
      <Link to={"/cases/" + casee._id}><Icon type="edit" /> More details</Link>,
    ]
  }

  renderCases() {
    return this.props.cases.map(casee => {
      return (
        <Col key={casee._id} span="12" style={{padding: '0 12px 12px 0'}}>
          <Card title={casee.title + ' - #' + casee._id} loading={!this.state.ready} actions={this.caseActions(casee)}>
            <div className="case-location posrel">
              <Icon type="environment" /> &nbsp;{casee.location}
              <span className="case-urgency" style={{float: 'right'}}>
                <Icon type="calendar" /> &nbsp;{casee.urgency}
                <br />
                {casee.createdAt.toDateString()}
              </span>
            </div>
            <div className="case-description">
              {casee.description}
            </div>
            <div className="case-causes">
              {this.renderCauses(casee.causeIds)}
            </div>
          </Card>
        </Col>
      )
    });
  }

  render() {
    const { ready } = this.state;
    const causes = this.props.causes.map(cause => <Option key={cause._id} defaultValue={cause._id}>{cause.name}</Option>);
    return (
      <div className="feed-container">
        <Row className="feed-top">
          <Col span="4">
            Browse causes:
          </Col>
          <Col span="6">
            <Select defaultValue="all" style={{width: '100%'}} onChange = {(value) => {Cause.set(value)}} placeholder="Select cause">
              {[<Option key="all" defaultValue="all">All Causes</Option>].concat(causes)}
            </Select>
          </Col>
          <Col span="14" className="posrel">
            <Link to="/just-give" style={{position: 'absolute', top: 0, right: 0}}>
              <Button type="primary" icon="pay-circle">
                Just Give
              </Button>
            </Link>
          </Col>
        </Row>
        <Divider />
        <Row>
          {this.renderCases()}
        </Row>
      </div>
    )
  }
}

export default withTracker(props => {
  const casesHandle = Meteor.subscribe('cases.feed');
  const causesHandle = Meteor.subscribe('causes.all');
  const cause = Cause.get();
  const constraint = cause && cause != "all" ? {causeIds: cause} : {};
  const cases = Cases.find(constraint);
  return {
    loading: !casesHandle.ready(),
    cases: cases.fetch(),
    causes: Causes.find().fetch(),
  }
})(Feed)
