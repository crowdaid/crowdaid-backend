import React from 'react'
import { Switch, Route } from 'react-router-dom'
import CaseDetails from './details'
import CaseDonors from './donors'
import CaseNeeds from './needs'
import NeedDetails from '../needs/details'
import Donate from '../donations/donate'
import DonationDetails from '../donations/details'

export default class Cases extends React.Component {
  constructor(props) {
    super(props)
  }

  render() {
    return (
      <Switch>
        <Route exact path="/cases/:cid" render={props => (<CaseDetails {...this.props} {...props} />)} />
        <Route exact path="/cases/:cid/needs" render={props => (<CaseDetails target="needs" {...this.props} {...props} />)} />
        <Route exact path="/cases/:cid/donors" render={props => (<CaseDetails target="donors" {...this.props} {...props} />)} />
        <Route exact path="/cases/:cid/needs/:nid" render={props => (<NeedDetails {...this.props} {...props} />)} />
        <Route exact path="/cases/:cid/needs/:nid/donate" render={props => (<Donate {...this.props} {...props} />)} />
        <Route exact path="/cases/:cid/needs/:nid/donations/:did" render={props => (<DonationDetails {...this.props} {...props} />)} />
      </Switch>
    )
  }
}
