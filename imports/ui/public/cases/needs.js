import React from 'react';
import PropTypes from 'prop-types';
import { Meteor } from 'meteor/meteor';
import { withTracker } from 'meteor/react-meteor-data';
import Icon from 'antd/lib/icon';
import Needs from '/imports/api/needs/needs';

class CaseNeeds extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      ready: false,
    }
  }

  componentWillReceiveProps(props) {
    this.setState({ ready: !props.loading })
  }

  renderNeeds() {
    return this.props.needs.map(need => {
      return <h1 key={need._id}>{need.title}</h1>
    })
  }

  render() {
    const { ready } = this.state;
    if ( ! ready) return <Icon type="loading" />
    return (
      <div>
        {this.renderNeeds()}
      </div>
    )
  }
}

CaseNeeds.propTypes = {
  loading: PropTypes.bool,
  needs: PropTypes.array,
}

export default withTracker(props => {
  const caseId = props.match.params.cid;
  const NeedsHandle = Meteor.subscribe('needs.case.feed', caseId);

  return {
    loading: !NeedsHandle.ready(),
    needs: Needs.find({caseId: caseId}).fetch(),
  }
})(CaseNeeds)
