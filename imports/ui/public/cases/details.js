import React from 'react'
import SingleCase from '/imports/ui/profile/org/cases/single'

const CaseDetails = (props) => {
  return <SingleCase mode="feed" {...props} />
}

export default CaseDetails
