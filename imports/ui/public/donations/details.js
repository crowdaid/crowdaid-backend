import React from 'react'
import DonationDetails from '../../profile/user/donations/details'

const PublicDetails = (props) => {
  return <DonationDetails mode="feed" {...props} />
}

export default PublicDetails
