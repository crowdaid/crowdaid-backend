import React from 'react'
import PropTypes from 'prop-types'
import { Meteor } from 'meteor/meteor'
import { withTracker } from 'meteor/react-meteor-data'
import AutoDonations from '/imports/api/donations/autoDonations'
import Organizations from '/imports/api/organizations/organizations'
import { autoDonate } from '/imports/api/donations/methods'
import { donationMethods, autoDonationFrequencies } from '/imports/api/commonValues'

import { Card, Form, Select, Input, DatePicker, Switch, Button, Row, Col, Divider, Icon } from 'antd/lib'
import message from 'antd/lib/message'

const FormItem = Form.Item
const Option = Select.Option

class JustGive extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      quantifiable: true,
      donationType: null,
      donationMethod: null,
      donating: false,
      selectingOrg: false,
      searching: false,
      searchval: null,
      orgs: [],
      selectedOrgs: [],
      selective: false,
      loading: false,
    }

    this.collectionTime = null
    this.frequency = null
    this.selectedOrgId = null
  }

  setQuantifiability(isQuantifiable) {
    this.setState({quantifiable: isQuantifiable})
  }

  handleQuantityChange(e) {
    const quantity = e.target.value
  }

  handleOrgSearch(value) {
    this.setState({orgs: Organizations.find()})
  }

  handleOrgChange(value) {
    this.selectedOrgId = value
    this.setState({searching: false, orgs: []})
  }

  addOrg() {
    const { selectedOrgs } = this.state
    const alreadySelected = selectedOrgs.find(org => org.id === this.selectedOrgId)
    if (alreadySelected) {
      message.error('Organization already selected')
      return
    }

    const quantity = document.getElementById('quantity').value
    selectedOrgs.push({
      id: this.selectedOrgId,
      quantity,
    })

    this.setState({ selectedOrgs, selectingOrg: false, })
  }

  removeSelectedOrg(org) {
    this.setState({selectedOrgs: $.grep(this.state.selectedOrgs, function(o) {
      return o.id != org.id
    })})
  }

  handleSubmit() {
    const form = document.getElementById('justGiveForm');
    const { quantifiable, donationMethod, donationType, selective, selectedOrgs } = this.state
    autoDonate.call({
      quantity: quantifiable && !selective ? form.quantity.value : 1,
      notes: form.notes.value,
      donationMethod,
      type: donationType,
      collectionAddress: donationMethod != 'Credit card' ? form.address.value: 'NoAddress',
      nextCollectionTime: this.collectionTime,
      frequency: this.frequency,
      isSelective: selective,
      orgs: selective ? selectedOrgs : [],
      userId: Meteor.userId(),
    }, (err, res) => {
      if (err) {
        message.error(err.toString());
      } else {
        message.success('Autodonation created successfully');
      }
    })
  }

  render() {
    const { quantifiable, donationType, donationMethod, donating, selectingOrg,
            searching, searchval, orgs, selectedOrgs, selective, loading } = this.state
		const frequencies = autoDonationFrequencies.map(f => <Option key={f} defaultValue={f}>{f}</Option>)
    const methods = donationMethods.map(method => <Option key={method} defaultValue={method}>{method}</Option>)
    const orgOptions = orgs.map(org => <Option key={org._id} defaultValue={org._id}>{org.name.en}</Option>)
    const orgsList = selectedOrgs.map(org => <div key={'org_' + org._id}>{org.id + " --- " + org.quantity + " --- "} <a href="javascript:void(0);" onClick={() => {this.removeSelectedOrg(org).bind(this)}}>Remove</a></div>)
		return (
			<div>
				{donating ?
          <Form layout="vertical" id="justGiveForm" style={{width: '70%'}}>
            <FormItem className="posrel">
              <h2>Enter donation details</h2>
              <div className="posrel">
                <Button style={{position: 'absolute', right: 0, top: 0}} icon="left" type="default" onClick={() => this.setState({donating: false})}>Back</Button>
              </div>
            </FormItem>
            <FormItem>
              Selective donation  <Switch checkedChildren="Yes" unCheckedChildren="No" onChange={(value) => this.setState({selective: value})} />
            </FormItem>
            {selective ?
              <div>
                {selectingOrg || selectedOrgs.length == 0 ?
                  <Row>
                    <Col span="12">
                      <FormItem label="Select organization to donate for">
                        <Select
                          onSearch={this.handleOrgSearch.bind(this)}
                          onChange={this.handleOrgChange.bind(this)}
                          showSearch={true}
                          filterOption={false}
                          notFoundContent={searchval == null ? 'Type to start search' : 'No organizations match your search'}
                          id="organization"
                          placeholder="Select organization to donate for"
                        >
                          {orgOptions}
                        </Select>
                      </FormItem>
                    </Col>
                    <Col span="8">
                      {quantifiable ?
                        <FormItem>
                          <Input
                            type="number"
                            min={1}
                            style={{width:'95%'}}
                            onChange={this.handleQuantityChange.bind(this)}
                            id='quantity'
                            placeholder={donationType == 'money' ? 'Amount (EGP)' : 'Items number'}
                            addonAfter={donationType == 'money' ? 'EGP' : 'Item(s)'}/>
                        </FormItem>: ''
                      }
                    </Col>
                    <Col span="4">
                      <Button icon="check" onClick={this.addOrg.bind(this)}>Done</Button>
                    </Col>
                  </Row>: ''
                }
                <div className="selectedOrgs">
                  {orgsList}
                </div>
              </div>:
              <div>
                {quantifiable ?
    	  	        <FormItem>
    	  						<Input
    									type="number"
    									min={1}
    	                onChange={this.handleQuantityChange.bind(this)}
    	  							id="quantity"
    	  							placeholder={donationType == 'money' ? 'Enter total donation amount' : 'Enter total number of items'}
    									addonAfter={donationType == 'money' ? 'EGP' : 'Item(s)'}/>
    	  					</FormItem>: ''
    	          }
              </div>
            }
            <Row>
              {donationType == 'money' ?
                <Col span={8}>
                  <FormItem label="Donation method">
                    <Select
                      onChange = {(value) => {this.setState({donationMethod: value})}}
                      placeholder="Select payment method"
                      id="method"
                      style={{width:'95%'}}
                    >
                      {methods}
                    </Select>
                  </FormItem>
                </Col>: ''
              }
  						<Col span={7}>
  							<FormItem label="Donation collection time">
  								<DatePicker style={{width:'95%'}} format="YYYY-MM-DD HH:mm:ss" showTime={true} id="collectionTime" onChange={(momentObj, string) => { this.collectionTime = momentObj.unix() }} />
  							</FormItem>
  						</Col>
							<Col span={9}>
								<FormItem label="Donation Frequency">
			            <Select
			              onChange = {(value) => {this.frequency = value}}
			              placeholder="Select donation frequency"
			              id="frequency">
			              {frequencies}
			            </Select>
			          </FormItem>
							</Col>
  					</Row>
            {donationMethod != 'Credit card' ?
              <FormItem label="Collection Address">
                <Input id="address" placeholder="Enter donation collection address" />
              </FormItem>: ''
            }
            <FormItem label="Donation notes">
              <Input.TextArea
                id="notes"
                rows={4}
                placeholder="Add notes on your donation" />
            </FormItem>
            <Divider />
            <FormItem>
              <Button key="submit" type="primary" onClick={this.handleSubmit.bind(this)} loading={loading}>Submit donation</Button>
            </FormItem>
  	      </Form>:
          <Card title="What do you want to donate?">
            <Card.Grid style={{width:'33%', textAlign: 'center', cursor: 'pointer'}} onClick={() => {this.setState({donationType: 'money', donationMethod: null, donating: true})}}>
              <Icon style={{ fontSize: 24 }} type="pay-circle-o" />
              <br />
              <a style={{fontSize: '24px', color: '#777777'}} href="javascript:void(0);">Money</a>
            </Card.Grid>
            <Card.Grid style={{width:'33%', textAlign: 'center', cursor: 'pointer'}} onClick={() => {this.setState({donationType: 'items', donationMethod: null, donating: true})}}>
              <Icon style={{ fontSize: 24 }} type="appstore-o" />
              <br />
              <a style={{fontSize: '24px', color: '#777777'}} href="javascript:void(0);">Items</a>
            </Card.Grid>
            <Card.Grid style={{width:'33%', textAlign: 'center', cursor: 'pointer'}} onClick={() => {this.setState({donationType: 'service', donationMethod: null, donating: true})}}>
              <Icon style={{ fontSize: 24 }} type="global" />
              <br />
              <a style={{fontSize: '24px', color: '#777777'}} href="javascript:void(0);">Service</a>
            </Card.Grid>
          </Card>
        }
			</div>
		)
  }
}

JustGive.propTypes = {

}

export default withTracker(props => {
  const orgsSub = Meteor.subscribe('organizations.justgive');
  const orgs = Organizations.find({}).fetch();
  return {
    orgs,
  }
})(JustGive)
