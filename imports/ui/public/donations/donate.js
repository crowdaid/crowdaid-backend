import React from 'react'
import PropTypes from 'prop-types'
import { Meteor } from 'meteor/meteor'
import { withTracker } from 'meteor/react-meteor-data'
import Needs from '/imports/api/needs/needs'
import Donations from '/imports/api/donations/donations'
import { donate } from '/imports/api/donations/methods'
import { donationMethods } from '/imports/api/commonValues'

import { Form, Input, Select, DatePicker, Radio, Icon, Spin, Divider, Button, Alert } from 'antd/lib'
import message from 'antd/lib/message'

const FormItem = Form.Item
const Option = Select.Option

class Donate extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      ready: false,
      loading: false,
      donationType: 'money', // money or items
      quantity: 0, // Donation quantity
      required: 0, // Unfulfilled quantity
      fulfilled: false, // Is this need fulfilled
      donationMethod: null,
    }

    this.collectionTime = null
  }

  componentWillReceiveProps(props) {
    let requiredQuantity
    if (props.need && props.need.isQuantifiable) {
      requiredQuantity = Math.round(100*(props.need.quantityNeeded - props.need.quantityFulfilled))/100
    } else {
      requiredQuantity = 0
    }

    this.setState({
      ready: !props.loading,
      required: requiredQuantity,
      fulfilled: props.need ? props.need.status === 'fulfilled' : false,
      donationType: props.need && !props.need.isQuantifiable ? 'noquantity' : this.state.donationType,
    })
  }

  handleDonationTypeChange(e) {
    this.setState({donationType: e.target.value})
    document.getElementById('quantity').value = ''
  }

  handleQuantityChange(e) {
    let donationQuantity
    // Get original value
    const val = e.target.value
    // Get the value parsed as number
		let value = Math.round(Number(val))
    // Make sure value is greater than 0
    value = value <= 0 ? 1 : value
    // Make sure value is less than or equal to required quantity
    if (this.props.need.unitOfMeasure == 'EGP' || (this.props.need.unitOfMeasure == 'Item' && this.state.donationType == 'items')) {
      if (this.props.need.unitOfMeasure == 'Item') {
        // If 1.5 items are required allow for values of 1
        // And the other 0.5 should be donated as money in another donation
        value = value > Math.floor(this.state.required) ? Math.floor(this.state.required) : value
      } else {
        value = value > this.state.required ? this.state.required : value
      }
      // Donation quantity is in EGP Or Item and is consistent with donation type
      // Field value is the same as the number before the slash e.g, 2/5items and 1000/5000EGP
      donationQuantity = value
    } else if (this.props.need.unitOfMeasure == 'Item' && this.state.donationType == 'money') {
      const itemPrice = this.props.need.itemPrice
      const requiredMoney = Number(itemPrice) * Number(this.state.required)
      value = value > requiredMoney ? requiredMoney : value
      // The money value must be converted to number of items
      // To show the number of items the user will donate
      donationQuantity = Math.round(value*100/itemPrice)/100
    }

    // Update state
		this.setState({quantity: val == '' ? 0 : donationQuantity})
    // Update field value
		document.getElementById('quantity').value = val == '' ? '' : value
	}

  handleSubmit() {
    this.setState({loading: true})
    const form = document.getElementById('donateForm')
    const needId = this.props.need._id
    const caseId = this.props.need.caseId
    const moneyValue = this.props.need.unitOfMeasure == 'Item' && this.state.donationType == 'money' ? form.quantity.value : 0
    const newDonation = {
      caseId,
      needId,
      quantity: this.state.quantity,
      moneyValue: moneyValue,
      type: this.state.donationType,
      notes: form.notes.value,
      donationMethod: this.state.donationType == 'money' ? this.state.donationMethod : 'NoMoney',
      collectionAddress: this.state.donationMethod == 'Cash on door' ? form.address.value : 'NoAddress',
      collectionTime: this.state.donationMethod == 'Cash on door' ? this.collectionTime : 'NoTime',
      wholeCase: false,
      wholeNeed: this.state.quantity === this.props.need.quantityNeeded,
      isRecurring: false,
      isActive: false,
      userId: Meteor.userId(),
    }

    donate.call(newDonation, (err, res) => {
      if (err) {
        message.error(err.toString())
        this.setState({loading: false})
      } else {
        message.success("Donation complete")
        this.setState({loading: false})
        this.props.history.push('/cases/' + caseId + '/needs/' + needId)
      }
    })
  }

  render() {
    const { ready, loading, donationType, quantity, required, fulfilled, donationMethod } = this.state
    if ( ! ready) return <Spin size="large" />
    const methods = donationMethods.map(method => <Option key={method} defaultValue={method}>{method}</Option>)
    return (
      <div>
        <h2>Donate need: {this.props.need.title}</h2>
        {fulfilled ? <Alert message="This need is fulfilled" type="success" /> :
          <Form layout="vertical" id="donateForm" style={{width: '70%'}}>
            <FormItem label="What do you want to donate?">
              <Radio.Group onChange={this.handleDonationTypeChange.bind(this)} defaultValue="money">
                <Radio.Button disabled={!this.props.need.isQuantifiable} value="money">Donate money</Radio.Button>
                <Radio.Button disabled={!this.props.need.isQuantifiable || this.props.need.unitOfMeasure === 'EGP' || required < 1} value="items">Donate items</Radio.Button>
                <Radio.Button disabled={this.props.need.isQuantifiable} value="noquantity">Non quantifiable</Radio.Button>
              </Radio.Group>
            </FormItem>
            {this.props.need.isQuantifiable ?
              <FormItem>
                <Input
                  type="number"
                  min={1}
                  onChange={this.handleQuantityChange.bind(this)}
                  id="quantity"
                  placeholder={donationType == 'money' ? 'Amount to donate (EGP)' : 'Number of items to donate'}
                  addonAfter={this.props.need.unitOfMeasure == 'EGP' ? quantity + '/' + required + 'EGP' : quantity + '/' + required + ' Item(s) | ' + this.props.need.itemPrice + 'EGP/Item'} />
              </FormItem>
              : ''
            }
            {donationType == 'money' ?
              <FormItem label="Donation method">
                <Select
                  onChange = {(value) => {this.setState({donationMethod: value})}}
                  placeholder="Select payment method"
                  id="method">
                  {methods}
                </Select>
              </FormItem> : ''
            }
            {donationMethod == 'Cash on door' || donationType != 'money' ?
              <div>
                <FormItem label="Collection Address">
                  <Input
                    id="address"
                    placeholder="Enter donation collection address" />
                </FormItem>
                <FormItem label="Collection time">
                  <DatePicker format="YYYY-MM-DD HH:mm:ss" showTime={true} id="collectionTime" onChange={(momentObj, string) => { this.collectionTime = momentObj.unix() }} />
    					  </FormItem>
              </div>: ''
            }
            <FormItem label="Donation notes">
              <Input.TextArea
                id="notes"
                rows={4}
                placeholder="Add notes on your donation" />
            </FormItem>
            <Divider />
            <FormItem>
              <Button key="submit" type="primary" onClick={this.handleSubmit.bind(this)} loading={loading}>Donate Now</Button>
            </FormItem>
          </Form>
        }
      </div>
    )
  }
}

Donate.propTypes = {
  need: PropTypes.object,
  donations: PropTypes.array,
  loading: PropTypes.bool,
}

export default withTracker(props => {
  const params = props.match.params
  const caseId = params.cid
  const needId = params.nid
  const orgId = null
  const needHandle = Meteor.subscribe('needs.single', needId)
  const donationsHandle = Meteor.subscribe('donations.need', needId)

  return {
    need: Needs.findOne(needId),
    donations: Donations.find({needId: needId}).fetch(),
    loading: !needHandle.ready(),
  }
})(Donate)
