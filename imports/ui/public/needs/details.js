import React from 'react'
import SingleNeed from '/imports/ui/profile/org/cases/singleNeed'

const NeedDetails = (props) => {
  return <SingleNeed mode="feed" {...props} />
}

export default NeedDetails
