import React from 'react';
import { Link } from 'react-router-dom';
import { Meteor } from 'meteor/meteor';
import { withTracker } from 'meteor/react-meteor-data';
import PropTypes from 'prop-types';
import { addEmail, removeEmail, sendVerification } from '/imports/api/users/methods';
import {
  Table, Modal, Divider, Spin, Tag, Button, Form, Input, Icon,
} from 'antd/lib';
import message from 'antd/lib/message';

class UserEmails extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      ready: !props.loading,
      addingEmail: false,
      loading: false,
    };
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      ready: !nextProps.loading,
	  });
  }

  resendVerification(email) {
    sendVerification.call({
      userId: this.props.user._id,
      email: email.address,
    }, (err, res) => {
      if (err) {
        message.error(err.toString());
      } else {
        message.success('Verification email sent successfully');
      }
    });
  }

  removeEmail(email) {
    const history = this.props.history;
    const user = this.props.user;
    Modal.confirm({
      title: `Remove Email address ${email.address} ?`,
      description: 'Are your sure you want to remove this email address?',
      okText: 'Remove',
      okType: 'danger',
      onOk() {
        removeEmail.call({
          userId: user._id, email: email.address,
        }, (err, res) => {
          if (err) {
            message.error(err.toString());
          } else {
            message.success('Email address removed successfully');
          }
        });
      },
    });
  }

  addEmail(e) {
    e.preventDefault();
    this.setState({ loading: true });
    const form = e.target;
    addEmail.call({
      userId: this.props.user._id,
      email: form.email.value,
    }, (err, res) => {
      if (err) {
        message.error(err.toString());
        this.setState({ loading: false });
      } else {
        message.success('Email added successfully');
        this.setState({ loading: false, addingEmail: false });
      }
    });
  }

  addEmailForm() {
    const loading = this.state.loading;
    return (
      <Form layout="inline" onSubmit={this.addEmail.bind(this)}>
        <Form.Item>
          <Input prefix={<Icon type="mail" />} type="email" id="email" placeholder="Enter email address" />
        </Form.Item>
        <Form.Item>
          &nbsp; &nbsp;
          <Button type="primary" htmlType="submit" loading={loading}>
            Add Email
          </Button>
          &nbsp; &nbsp;
          <Button type="default" htmlType="button" onClick={() => { this.setState({ addingEmail: false }); }}>
            Cancel
          </Button>
        </Form.Item>
      </Form>
    );
  }

  render() {
    const { ready, addingEmail, loading } = this.state;
    if (!ready) { return <Spin size="large" />; }
    const user = this.props.user;
    const dataSource = this.props.user.emails.map((email, index) => {
      email.key = `email-${index}`;
      return email;
    });

    const columns = [
      {
			  title: 'Email Address',
			  dataIndex: 'address',
			  key: 'address',
      },
      {
			  title: 'Email Status',
			  key: 'status',
        render: (text, record) => (
          <div>
            {record.verified
              ? <Tag color="green">Verified</Tag>
              : <Tag color="red">Not verified</Tag>
            }
          </div>
        ),
      },
      {
        title: 'Actions',
        key: 'actions',
        render: (text, record) => (
          <div>
            <a href="javascript:void(0);" onClick={() => this.removeEmail(record)}>Remove</a>
            {!record.verified
              ? (
                <span>
                  <Divider type="vertical" />
                  <a href="javascript:void(0);" onClick={() => this.resendVerification(record)}>Resend verification</a>
                </span>
              )
              : ''
            }
          </div>
        ),
      },
    ];

    return (
      <div className="posrel">
        <Button type="default" icon="plus" onClick={() => { this.setState({ addingEmail: true }); }} style={{ position: 'absolute', right: 0 }}>Add Email</Button>
        <h2>Email Addresses</h2>
        {addingEmail ? this.addEmailForm() : ''}
        <Table dataSource={dataSource} columns={columns} />
      </div>
    );
  }
}

UserEmails.propTypes = {
  user: PropTypes.object,
  loading: PropTypes.bool,
};

export default withTracker((props) => {
  const userId = Meteor.userId();
  const sub = Meteor.subscribe('loggedUser');
  const ready = sub.ready();
  return {
    user: Meteor.user(),
    loading: !ready,
  };
})(UserEmails);
