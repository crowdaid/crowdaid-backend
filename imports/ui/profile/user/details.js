import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { Meteor } from 'meteor/meteor';
import { withTracker } from 'meteor/react-meteor-data';
import Files from '/imports/api/files/files';
import { removeFile } from '/imports/api/files/methods';
import { updateAvatar } from '/imports/api/users/methods';
import {
  Button, Upload, Icon, Spin,
} from 'antd/lib';
import message from 'antd/lib/message';

class ProfileInfo extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      ready: !props.loading,
      uploading: false,
      updatingAvatar: false,
    };
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      ready: !nextProps.loading,
	  });
  }

  uploadAvatar(event) {
	    const self = this;
	    if (event.file && event.file.status != 'removed') {
      self.setState({ uploading: true });
	      const file = event.file;
      console.log(file);
      if (!/png|jpg|jpeg/i.test(file.extension)) {
        message.error('File extension is not allowed');
        return;
      }
      const uploadInstance = Files.insert({
        file,
        meta: {
          // locator: self.props.fileLocator,
          userId: Meteor.userId(), // Optional, used to check on server for file tampering
          type: 'avatar',
        },
        streams: 'dynamic',
        chunkSize: 'dynamic',
        allowWebWorkers: true, // If you see issues with uploads, change this to false
      }, false);

      uploadInstance.on('uploaded', (error, fileObj) => {
        // Upload complete
        self.setState({
          uploading: false,
          updatingAvatar: true,
        });

        updateAvatar.call({
          fileId: fileObj._id,
          userId: fileObj.userId,
        }, (err, res) => {
          if (err) {
            message.error('Error updating profile picture');
            self.setState({ updatingAvatar: false });
          } else {
            message.success('Avatar updated successfully');
            self.setState({ updatingAvatar: false });
          }
        });
      });

      uploadInstance.on('error', (error, fileObj) => {
        // An error occurred
        message.error(error.toString());
        self.setState({ uploading: false });
      });

      // Start the upload
      uploadInstance.start();
	    }
  }

  removeAvatar(file) {
    removeFile.call({
      fileId: file._id,
      userId: file.userId,
    }, (err, res) => {
      if (err) {
        // An error occurred
        message.error(err.toString());
        this.setState({ loading: false });
      } else {
        message.success('Avatar removed');
      }
    });
  }

  render() {
    const { ready, uploading, updatingAvatar } = this.state;
    const avatar = this.props.avatar;
    return (
      <div className="single-view user-view posrel">
        { !ready ? <Spin size="large" />
          : (
            <div>
              <div className="single-view-actions">
                <Link to="/profile/edit">
                  <Button type="default" icon="edit">
                Edit profile
                  </Button>
                </Link>
              </div>
              <table>
                <thead>
                  <tr>
                    <td colSpan="2">
                      <h2> Profile information </h2>
                    </td>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>Username</td>
                    <td>
                      <div>
                        {this.props.user.profile.username}
                      </div>
                      <div style={{ position: 'absolute', right: 0, top: 0 }}>
                        <Upload
                          name="avatar"
                          id="avatar"
                          listType="picture-card"
                          className="avatar-uploader"
                          showUploadList={false}
                          onChange={this.uploadAvatar.bind(this)}
                          onRemove={this.removeAvatar.bind(this)}
                          beforeUpload={() => false}
                        >
                          {!avatar
                ? (
                          <div className="posrel">
                            <Icon type={uploading ? 'loading' : 'user'} />
                            <div>{uploading ? 'Uploading' : 'Upload avatar'}</div>
                          </div>
                ) : (
                          <div className="posrel">
                            {uploading ? <Icon type="loading" /> : <img src={avatar} width="110" height="120" alt="Avatar" />}
                          </div>
                )}
                        </Upload>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <td>Primary Email</td>
                    <td>{this.props.user.emails[0].address}</td>
                  </tr>
                  <tr>
                    <td>Full Name</td>
                    <td>{this.props.user.profile.fullname}</td>
                  </tr>
                  <tr>
                    <td>Phone Number</td>
                    <td>{this.props.user.profile.phone}</td>
                  </tr>
                  <tr>
                    <td>Member since</td>
                    <td>{this.props.user.createdAt.toDateString()}</td>
                  </tr>
                </tbody>
              </table>
            </div>
          )
      }
      </div>
    );
  }
}

ProfileInfo.propTypes = {
  user: PropTypes.object,
  loading: PropTypes.bool,
};

export default withTracker((props) => {
  const userId = Meteor.userId();
  const userSub = Meteor.subscribe('loggedUser');
  const userReady = userSub.ready();
  const user = Meteor.user();
  let avatarSub; let
    avatar;
  if (userReady) {
    avatarSub = Meteor.subscribe('files.avatar', user.profile.avatar, user._id);
    avatar = Files.findOne(user.profile.avatar);
  }

  return {
    user,
    avatar: avatar ? avatar.link() : null,
    loading: !userReady,
  };
})(ProfileInfo);
