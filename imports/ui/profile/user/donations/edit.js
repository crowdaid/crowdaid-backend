import React from 'react';
import PropTypes from 'prop-types';
import { Meteor } from 'meteor/meteor';
import { withTracker } from 'meteor/react-meteor-data';
import { moment } from 'meteor/momentjs:moment';
import Needs from '/imports/api/needs/needs';
import Donations from '/imports/api/donations/donations';
import { updateDonation } from '/imports/api/donations/methods';
import { donationMethods } from '/imports/api/commonValues';

import {
  Form, Input, Switch, Select, DatePicker, Radio, Icon, Spin, Divider, Button, Alert,
} from 'antd/lib';
import message from 'antd/lib/message';

const FormItem = Form.Item;
const Option = Select.Option;

class EditDonation extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      ready: false,
      loading: false,
      donationType: 'money', // money or items
      quantity: 0, // Donation quantity
      required: 0, // Unfulfilled quantity
      fulfilled: false, // Is this need fulfilled
      donationMethod: null,
    };

    this.collectionTime = null;
    this.quantityFieldValue = 0;
  }

  componentWillReceiveProps(props) {
    let requiredQuantity;
    if (props.need && props.need.isQuantifiable) {
      requiredQuantity = Math.round(100 * (props.need.quantityNeeded - props.need.quantityFulfilled)) / 100;
      if (props.need.unitOfMeasure == 'EGP') {
        this.quantityFieldValue = this.props.donation.quantity;
      } else if (this.props.donation.type == 'items') {
        this.quantityFieldValue = this.props.donation.quantity;
      } else {
        this.quantityFieldValue = this.props.donation.moneyValue;
      }
    } else {
      requiredQuantity = 0;
    }

    this.setState({
      ready: !props.loading,
      donationType: props.donation ? props.donation.type : 'money',
      quantity: props.donation ? props.donation.quantity : 0,
      required: requiredQuantity,
      fulfilled: props.need ? props.need.status === 'fulfilled' : false,
      donationMethod: props.donation ? props.donation.donationMethod : null,
    });
  }

  handleDonationTypeChange(e) {
    this.setState({ donationType: e.target.value });
    document.getElementById('quantity').value = '';
  }

  handleQuantityChange(e) {
    let donationQuantity;
    // Get original value
    const val = e.target.value;
    // Get the value parsed as number
    let value = Math.round(Number(val));
    // Make sure value is greater than 0
    value = value <= 0 ? 1 : value;
    // Make sure value is less than or equal to required quantity
    if (this.props.need.unitOfMeasure == 'EGP' || (this.props.need.unitOfMeasure == 'Item' && this.state.donationType == 'items')) {
      if (this.props.need.unitOfMeasure == 'Item') {
        // If 1.5 items are required allow for values of 1
        // And the other 0.5 should be donated as money in another donation
        value = value > Math.floor(this.state.required) ? Math.floor(this.state.required) : value;
      } else {
        value = value > this.state.required ? this.state.required : value;
      }
      // Donation quantity is in EGP Or Item and is consistent with donation type
      // Field value is the same as the number before the slash e.g, 2/5items and 1000/5000EGP
      donationQuantity = value;
    } else if (this.props.need.unitOfMeasure == 'Item' && this.state.donationType == 'money') {
      const itemPrice = this.props.need.itemPrice;
      const requiredMoney = Number(itemPrice) * Number(this.state.required);
      value = value > requiredMoney ? requiredMoney : value;
      // The money value must be converted to number of items
      // To show the number of items the user will donate
      donationQuantity = Math.round(value * 100 / itemPrice) / 100;
    }

    // Update state
    this.setState({ quantity: val == '' ? 0 : donationQuantity });
    // Update field value
    document.getElementById('quantity').value = val == '' ? '' : value;
  }

  handleSubmit() {
    this.setState({ loading: true });
    const form = document.getElementById('updateDonationForm');
    const needId = this.props.donation.needId;
    const caseId = this.props.donation.caseId;
    const moneyValue = this.props.need.unitOfMeasure == 'Item' && this.state.donationType == 'money' ? form.quantity.value : 0;
    const donationData = {
      _id: this.props.donation._id,
      caseId,
      needId,
      quantity: this.state.quantity,
      moneyValue,
      type: this.state.donationType,
      notes: form.notes.value,
      donationMethod: this.state.donationType == 'money' ? this.state.donationMethod : 'NoMoney',
      collectionAddress: this.state.donationMethod == 'Cash on door' ? form.address.value : 'NoAddress',
      collectionTime: this.state.donationMethod == 'Cash on door' ? this.collectionTime : 'NoTime',
      wholeCase: this.props.donation.wholeCase,
      wholeNeed: this.state.quantity === this.props.need.quantityNeeded,
      isRecurring: this.props.donation.isRecurring,
      isActive: this.props.donation.isActive,
      userId: Meteor.userId(),
    };

    updateDonation.call(donationData, (err, res) => {
      if (err) {
        message.error(err.toString());
        this.setState({ loading: false });
      } else {
        message.success('Donation updated successfully');
        this.setState({ loading: false });
        this.props.history.push(`/profile/donations/${this.props.donation._id}`);
      }
    });
  }

  render() {
    const {
      ready, loading, donationType, quantity, required, fulfilled, donationMethod,
    } = this.state;
    if (!ready) return <Spin size="large" />;
    const methods = donationMethods.map(method => <Option key={method} defaultValue={method}>{method}</Option>);
    const donatedMoney = this.props.donation.moneyValue > 0 ? this.props.donation.moneyValue : this.props.donation.quantity;
    return (
      <div>
        <h2>
Edit donation to need:
          {this.props.need.title}
        </h2>
        {fulfilled ? <Alert message="This need is fulfilled" type="success" />
          : (
            <Form layout="vertical" id="updateDonationForm" style={{ width: '70%' }}>
              <FormItem label="What do you want to donate?">
                <Radio.Group onChange={this.handleDonationTypeChange.bind(this)} defaultValue={this.props.donation.type}>
                  <Radio.Button disabled={!this.props.need.isQuantifiable} value="money">Donate money</Radio.Button>
                  <Radio.Button disabled={!this.props.need.isQuantifiable || this.props.need.unitOfMeasure === 'EGP' || required < 1} value="items">Donate items</Radio.Button>
                  <Radio.Button disabled={this.props.need.isQuantifiable} value="noquantity">Non quantifiable</Radio.Button>
                </Radio.Group>
              </FormItem>
              {this.props.need.isQuantifiable
                ? (
                  <FormItem>
                    <Input
                      type="number"
                      min={1}
                      onChange={this.handleQuantityChange.bind(this)}
                      id="quantity"
                      defaultValue={this.quantityFieldValue}
                      placeholder={donationType == 'money' ? 'Amount to donate (EGP)' : 'Number of items to donate'}
                      addonAfter={this.props.need.unitOfMeasure == 'EGP' ? `${quantity}/${required}EGP` : `${quantity}/${required} Item(s) | ${this.props.need.itemPrice}EGP/Item`}
                    />
                  </FormItem>
                )
                : ''
            }
              {donationType == 'money'
                ? (
                  <FormItem label="Donation method">
                    <Select
                      onChange={(value) => { this.setState({ donationMethod: value }); }}
                      placeholder="Select payment method"
                      defaultValue={this.props.donation.donationMethod}
                      id="method"
                    >
                      {methods}
                    </Select>
                  </FormItem>
                ) : ''
            }
              {donationMethod == 'Cash on door' || donationType != 'money'
                ? (
                  <div>
                    <FormItem label="Collection Address">
                      <Input
                        id="address"
                        defaultValue={this.props.donation.collectionAddress}
                        placeholder="Enter donation collection address"
                      />
                    </FormItem>
                    <FormItem label="Collection time">
                      <DatePicker format="YYYY-MM-DD HH:mm:ss" showTime id="collectionTime" defaultValue={moment(this.props.donation.collectionTime)} onChange={(momentObj, string) => { this.collectionTime = momentObj.unix(); }} />
                    </FormItem>
                  </div>
                ) : ''
            }
              <FormItem label="Donation notes">
                <Input.TextArea
                  id="notes"
                  rows={4}
                  defaultValue={this.props.donation.notes}
                  placeholder="Add notes on your donation"
                />
              </FormItem>
              <Divider />
              <FormItem>
                <Button key="submit" type="primary" onClick={this.handleSubmit.bind(this)} loading={loading}>Save Changes</Button>
              </FormItem>
            </Form>
          )
        }
      </div>
    );
  }
}

EditDonation.propTypes = {
  need: PropTypes.object,
  donation: PropTypes.object,
  loading: PropTypes.bool,
};

export default withTracker((props) => {
  const donId = props.match.params.did;
  const donationHandle = Meteor.subscribe('donations.single', donId);
  const donation = Donations.findOne(donId);
  let need; let
    needHandle;
  if (donationHandle.ready()) {
    needHandle = Meteor.subscribe('needs.single', donation.needId);
    need = Needs.findOne(donation.needId);
  }

  return {
    donation,
    need,
    loading: !donation || !need,
  };
})(EditDonation);
