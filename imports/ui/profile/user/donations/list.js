import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { Meteor } from 'meteor/meteor';
import { ReactiveVar } from 'meteor/reactive-var';
import { withTracker } from 'meteor/react-meteor-data';
import { moment } from 'meteor/momentjs:moment';
import Donations from '/imports/api/donations/donations';
import { cancelDonation } from '/imports/api/donations/methods';
import {
  Table, Modal, Divider, Tag, Form, Row, Col,
} from 'antd/lib';
import message from 'antd/lib/message';

const FormItem = Form.Item;
const PerPage = 10;
const PageNumber = new ReactiveVar(1);
const myImpactStartDate = new ReactiveVar(moment().subtract('1', 'months'));
const myImpactEndDate = new ReactiveVar(moment());
const showMyImpact = new ReactiveVar(false);

class MyDonations extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      ready: false,
    };
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      ready: !nextProps.loading,
	  });
  }

  cancelDonation(donation) {
    cancelDonation.call({ donation }, (err, res) => {
      if (err) {
        message.error(err.toString());
      } else {
        message.success('Donation cancelled successfully');
      }
    });
  }

  donationColumns() {
    return [
      {
			  title: 'ID',
			  dataIndex: '_id',
			  key: 'id',
      },
      {
			  title: 'Quantity',
			  key: 'quantity',
        render: (text, record) => {
          const unit = (record.moneyValue == 0 || !record.moneyValue) && record.type == 'money' ? 'EGP' : 'Items';
          return (
            <span>
              {`${record.quantity} ${unit}`}
            </span>
          );
        },
      },
      {
			  title: 'Status',
			  key: 'status',
        render: (text, record) => {
          let color;
          switch (record.status) {
            case 'Pending':
              color = 'gold';
              break;
            case 'Cancelled':
              color = 'blue';
              break;
            case 'Failed':
              color = 'red';
              break;
            case 'Collected':
              'green';
              break;
            default:
              color = '';
          }
          return (
            <Tag color={color}>{record.status}</Tag>
          );
        },
      },
      {
        title: 'Actions',
        key: 'actions',
        render: (text, record) => (
          <div>
            <Link to={`/profile/donations/${record._id}`}>Details</Link>
            <Divider type="vertical" />
            <Link to={`/cases/${record.caseId}`}>View case</Link>
            <Divider type="vertical" />
            <Link to={`/cases/${record.caseId}/needs/${record.needId}`}>View Need</Link>
            {record.status == 'Pending'
              ? (
                <span>
                  <Divider type="vertical" />
                  <Link to={`/profile/donations/${record._id}/edit`}>Edit</Link>
                </span>
              ) : ''
            }
            {record.status == 'Pending' && record.donationMethod == 'Credit card'
              ? (
                <span>
                  <Divider type="vertical" />
                  <Link to={`/profile/donations/${record._id}/checkout`}>Checkout</Link>
                </span>
              ) : ''
            }
            {record.status == 'Pending' && ((record.donationMethod == 'Cash on door' && moment().diff(new Date(record.collectionTime), 'days', true) < -1) || record.donationMethod == 'Credit card')
              ? (
                <span>
                  <Divider type="vertical" />
                  <a href="javascript:void(0);" onClick={() => this.cancelDonation(record)}>Cancel</a>
                </span>
              ) : ''
            }
          </div>
        ),
      },
    ];
  }

  render() {
    const donationsSource = this.props.donations.map((donation) => {
      donation.key = donation._id;
      return donation;
    });
    return (
      <div>
        <div className="table-operations">
          <Row>
            <Col span="8">
              Show my impact:
              {' '}
              <Switch checkedChildren="Yes" unCheckedChildren="No" onChange={value => showMyImpact.set(value)} />
            </Col>
            <Col span="16">
              <Form layout="vertical" id="myImpactForm">
                <Row>
                  <Col span="12">
                    <FormItem label="Start date">
                      <DatePicker format="YYYY-MM-DD" id="startDate" defaultValue={myImpactStartDate.get()} onChange={(momentObj, string) => { myImpactStartDate.set(momentObj); }} />
                    </FormItem>
                  </Col>
                  <Col span="12">
                    <FormItem label="End date">
                      <DatePicker format="YYYY-MM-DD" id="endDate" defaultValue={myImpactEndDate.get()} onChange={(momentObj, string) => { myImpactEndDate.set(momentObj); }} />
                    </FormItem>
                  </Col>
                </Row>
              </Form>
            </Col>
          </Row>
        </div>
        <Table pagination={this.props.pagination} dataSource={donationsSource} columns={this.donationColumns()} />
      </div>
    );
  }
}

MyDonations.propTypes = {
  user: PropTypes.object,
  pagination: PropTypes.object,
  loading: PropTypes.bool,
  donations: PropTypes.array,
};

export default withTracker((props) => {
  const userId = Meteor.userId();
  const donationsHandle = Meteor.subscribe('donations.user', userId);
  const showImpact = showMyImpact.get();
  let dateRange;
  if (showImpact) {
    const startDate = myImpactStartDate.get();
    const endDate = myImpactEndDate.get();
    const min = moment.min(startDate, endDate).toDate();
    const max = moment.max(startDate, endDate).toDate();
    dateRange = {
      $lt: max,
      $gt: min,
    };
  } else {
    dateRange = {};
  }

  const totalDonations = Donations.find({ userId }).count();
  const donations = Donations.find({ userId, createdAt: dateRange }, {
  	limit: PerPage, skip: (PageNumber.get() - 1) * PerPage,
  });

  return {
    user: Meteor.user(),
    loading: !donationsHandle.ready(),
    donations: donations.fetch(),
    pagination: {
      position: 'bottom',
      total: totalDonations,
      defaultCurrent: 1,
      pageSize: PerPage,
      onChange(page, perPage) {
        PageNumber.set(page);
      },
    },
  };
})(MyDonations);
