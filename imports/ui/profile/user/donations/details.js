import React from 'react';
import { Link } from 'react-router-dom';
import { Meteor } from 'meteor/meteor';
import { withTracker } from 'meteor/react-meteor-data';
import PropTypes from 'prop-types';
import Donations from '/imports/api/donations/donations';
import Needs from '/imports/api/needs/needs';
import {
  Button, Tag, Icon, Spin,
} from 'antd/lib';

class DonationDetails extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      ready: false,
    };
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      ready: !nextProps.loading,
	  });
  }

  render() {
    const ready = this.state.ready;
    if (!ready) return <Spin size="large" />;
    const fromFeed = this.props.mode === 'feed';
    const donation = this.props.donation;
    const donor = this.props.donor;
    const need = this.props.need;
    const donationUnit = (donation.moneyValue == 0 || !donation.moneyValue) && donation.type == 'money' ? 'EGP' : 'Items';
    return (
      <div className="single-view donation-view posrel">
        <div>
          <div className="single-view-actions">
            <Link to="/profile/donations">
              <Button type="default" icon="left">
                Back to donations
              </Button>
            </Link>
            &nbsp; &nbsp;
            {this.props.mine
              ? (
                <Link to={`/profile/donations/${donation._id}/edit`}>
                  <Button type="default" icon="edit">
                  Edit
                  </Button>
                </Link>
              ) : ''
            }
          </div>
          <table>
            <thead>
              <tr>
                <td colSpan="2">
                  <h2> Donation details </h2>
                </td>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>Need</td>
                <td>{need ? need.title : 'Loading...'}</td>
              </tr>
              <tr>
                <td>Donor</td>
                <td>
                  {donor ? donor.username || donor.emails[0].address : 'Loading...'}
                   &nbsp;
                  {donor ? <Link to={`/profile/${donor._id}`}>Profile</Link> : ''}
                </td>
              </tr>
              <tr>
                <td>Donation type</td>
                <td>{donation.type}</td>
              </tr>
              <tr>
                <td>Donation quantity</td>
                <td>
                  {`${donation.quantity} ${donationUnit}`}
                </td>
              </tr>
              <tr>
                <td>Donation status</td>
                <td>{donation.status}</td>
              </tr>
              <tr>
                <td>Donation method</td>
                <td>{donation.donationMethod}</td>
              </tr>
              <tr>
                <td>Donation notes</td>
                <td>{donation.notes}</td>
              </tr>
              {this.props.mine
                ? (
                  <tr>
                    <td>Donation collection address</td>
                    <td>{donation.collectionAddress}</td>
                  </tr>
                ) : ''
              }
              {this.props.mine
                ? (
                  <tr>
                    <td>Donation collection time</td>
                    <td>{(new Date(donation.collectionTime)).toDateString()}</td>
                  </tr>
                ) : ''
              }
              {donation.status == 'Collected'
                ? (
                  <tr>
                    <td>CollectedAt</td>
                    <td>{(new Date(donation.collectedTime)).toDateString()}</td>
                  </tr>
                ) : ''
              }
            </tbody>
          </table>
        </div>
      </div>
    );
  }
}

DonationDetails.propTypes = {
  donation: PropTypes.object,
  donor: PropTypes.object,
  need: PropTypes.object,
  mine: PropTypes.bool,
  loading: PropTypes.bool,
};

export default withTracker((props) => {
  const donId = props.match.params.did;
  const donationHandle = Meteor.subscribe('donations.single', donId);
  const donationReady = donationHandle.ready();
  const donation = Donations.findOne(donId);
  let donor; let
    need;
  if (donationHandle.ready()) {
    Meteor.subscribe('users.single', donation.userId);
    Meteor.subscribe('needs.single', donation.needId);
    donor = Meteor.users.findOne(donation.userId);
    need = Needs.findOne(donation.needId);
  }

  return {
    donation,
    donor,
    need,
    mine: donationReady && donation.userId === Meteor.userId(),
    loading: !donationReady,
  };
})(DonationDetails);
