import React from 'react';
import { Link } from 'react-router-dom';
import { Meteor } from 'meteor/meteor';
import { ReactiveVar } from 'meteor/reactive-var';
import { withTracker } from 'meteor/react-meteor-data';
import PropTypes from 'prop-types';
import Cases from '/imports/api/cases/cases';
import Donations from '/imports/api/donations/donations';
import { Table, Divider } from 'antd/lib';

const PerPage = 10;
const PageNumber = new ReactiveVar(1);

class MyCases extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      ready: false,
    };
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      ready: !nextProps.loading,
	  });
  }

  render() {
    const dataSource = this.props.cases.map((casee) => {
      casee.key = casee._id;
      return casee;
    });

    const columns = [
      {
			  title: 'Title',
			  dataIndex: 'title',
			  key: 'title',
      },
      {
			  title: 'Location',
			  dataIndex: 'location',
			  key: 'location',
      },
      {
			  title: 'Urgency',
			  dataIndex: 'urgency',
			  key: 'urgency',
      },
      {
			  title: 'Status',
			  dataIndex: 'status',
			  key: 'status',
      },
      {
        title: 'Actions',
        key: 'actions',
        render: (text, record) => (
          <div>
            <Link to={`/cases/${record._id}`}>View</Link>
            <Divider type="vertical" />
            <Link to={`/cases/${record._id}/needs`}>Case Needs</Link>
            <Divider type="vertical" />
            <Link to={`/cases/${record._id}/donors`}>Case Donors</Link>
          </div>
        ),
      },
    ];

    return (
      <Table pagination={this.props.pagination} dataSource={dataSource} columns={columns} />
    );
  }
}

MyCases.propTypes = {
  userId: PropTypes.string,
  pagination: PropTypes.object,
  loading: PropTypes.bool,
  cases: PropTypes.array,
};

export default withTracker((props) => {
  const userId = Meteor.userId();
  const donationsSub = Meteor.subscribe('donations.user', userId);
  const myDonations = Donations.find({ userId }).fetch();
  let userCases = []; let totalCases = 0; let
    casesReady = false;
  if (donationsSub.ready()) {
    // Get ids of cases this user donated to
    const caseIds = _.pluck(myDonations, 'caseId');
    const casesSub = Meteor.subscribe('cases.user', userId, caseIds);
    casesReady = casesSub.ready();
    userCases = Cases.find({ _id: { $in: caseIds } }, {
    	limit: PerPage, skip: (PageNumber.get() - 1) * PerPage,
    }).fetch();
    totalCases = Cases.find({ _id: { $in: caseIds } }).count();
  }
  return {
    userId,
    cases: userCases,
    loading: !casesReady,
    pagination: {
      position: 'bottom',
      total: totalCases,
      defaultCurrent: 1,
      pageSize: PerPage,
      onChange(page, perPage) {
        PageNumber.set(page);
      },
    },
  };
})(MyCases);
