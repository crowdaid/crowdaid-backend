import React from 'react';
import { AccountsReactComponent } from 'meteor/meteoreact:accounts';

export default class Password extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
        <AccountsReactComponent history={this.props.history} state="changePwd" />
      </div>
    );
  }
}
