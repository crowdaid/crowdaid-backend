import React from 'react';
import { Meteor } from 'meteor/meteor';
import { withTracker } from 'meteor/react-meteor-data';
import PropTypes from 'prop-types';
import {
  Form, Input, Select, Button, Icon, Spin,
} from 'antd/lib';
import message from 'antd/lib/message';
import { updateProfile } from '/imports/api/users/methods';

const FormItem = Form.Item;
const Option = Select.Option;

class EditProfile extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      ready: false,
    };
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      ready: !nextProps.loading,
	  });
  }

  handleSubmit() {
    this.setState({ loading: true });
    const form = document.getElementById('editProfileForm');
    updateProfile.call({
      userId: this.props.user._id,
      username: form.username.value,
      fullname: form.fullname.value,
      phone: form.phone.value,
    }, (err, res) => {
      if (err) {
        // An error occurred
        alert(err);
        this.setState({ loading: false });
      } else {
        this.setState({ loading: false });
        message.success('Profile updated successfully');
        this.props.history.push('/profile/info');
      }
    });
  }

  render() {
    const { loading, ready } = this.state;
    if (!ready) { return <Spin size="large" />; }
    const user = this.props.user;
    return (
      <div>
        <h2>Edit Profile: </h2>
        <Form layout="vertical" id="editProfileForm" style={{ width: '70%' }}>
          <FormItem label="Username">
            <Input
              id="username"
              type="text"
              defaultValue={user.username}
              placeholder="Username"
            />
          </FormItem>
          <FormItem label="Full Name">
            <Input
              id="fullname"
              type="text"
              defaultValue={user.profile.fullname}
              placeholder="Full Name"
            />
          </FormItem>
          <FormItem label="Phone Number">
            <Input
              id="phone"
              type="text"
              defaultValue={user.profile.phone}
              placeholder="Phone Number"
            />
          </FormItem>
          <FormItem>
            <Button key="submit" type="primary" onClick={this.handleSubmit.bind(this)} loading={loading}>Save Profile</Button>
          </FormItem>
        </Form>
      </div>
    );
  }
}

EditProfile.propTypes = {
  user: PropTypes.object,
  loading: PropTypes.bool,
};

export default withTracker((props) => {
  const sub = Meteor.subscribe('loggedUser');
  const ready = sub.ready();
  return {
    user: Meteor.user(),
    loading: !ready,
  };
})(EditProfile);
