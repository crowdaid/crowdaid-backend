import React from 'react';
import { Link } from 'react-router-dom';
import { Meteor } from 'meteor/meteor';
import { withTracker } from 'meteor/react-meteor-data';
import PropTypes from 'prop-types';
import Organizations from '/imports/api/organizations/organizations';
import Causes from '/imports/api/causes/causes';
import Files from '/imports/api/files/files';
import { Button, Tag, Icon } from 'antd/lib';

class Details extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      ready: false,
    };
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      ready: !nextProps.loading,
	  });
  }

  renderLegals() {
    const files = this.props.files;
    return files.map((file) => {
      const url = Files.link(file);
      return (
        <Tag key={file._id}>
          <a href={url} target="_blank">{file.name}</a>
        </Tag>
      );
    });
  }

  renderCauses() {
    const causes = this.props.causes;
    return causes.map(cause => <Tag key={cause._id} color="orange">{cause.name}</Tag>);
  }

  render() {
    const ready = this.state.ready;
    return (
      <div className="single-view org-view posrel">
        { !ready ? <div>Loading</div>
          : (
            <div>
              <div className="single-view-actions">
                <Link to={`/profile/org/${this.props.org._id}/edit`}>
                  <Button type="default" icon="edit">
                Edit
                  </Button>
                </Link>
              </div>
              <table>
                <thead>
                  <tr>
                    <td colSpan="2">
                      <h2> Organization details </h2>
                    </td>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>English Name</td>
                    <td>{this.props.org.name.en}</td>
                  </tr>
                  <tr>
                    <td>Arabic Name</td>
                    <td>{this.props.org.name.ar}</td>
                  </tr>
                  <tr>
                    <td>About</td>
                    <td>{this.props.org.about}</td>
                  </tr>
                  <tr>
                    <td>Registration Number</td>
                    <td>{this.props.org.registrationCode}</td>
                  </tr>
                  <tr>
                    <td>Registration Date</td>
                    <td>{this.props.org.registrationDate.toString()}</td>
                  </tr>
                  <tr>
                    <td>Legal Files</td>
                    <td>
                      {this.renderLegals()}
                    </td>
                  </tr>
                  <tr>
                    <td>Headquarters</td>
                    <td>{this.props.org.HQLocation}</td>
                  </tr>
                  <tr>
                    <td>Causes</td>
                    <td>
                      {this.renderCauses()}
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          )
      }
      </div>
    );
  }
}

Details.propTypes = {
  user: PropTypes.object,
  org: PropTypes.object,
  causes: PropTypes.array,
  files: PropTypes.array,
  loading: PropTypes.bool,
};

export default withTracker((props) => {
  const orgId = props.match.params.id;
  const sub = Meteor.subscribe('organizations.single', orgId);
  const org = Organizations.findOne({ _id: orgId });
  const sub1 = org ? Meteor.subscribe('causes.org', org) : {};
  const sub2 = org ? Meteor.subscribe('files.legal.org', org) : {};
  return {
    user: Meteor.user(),
    org,
    causes: Causes.find().fetch(),
    files: Files.find({ _id: { $in: org.legalFiles } }).fetch(),
    loading: !(!!org && sub1.ready() && sub2.ready()),
  };
})(Details);
