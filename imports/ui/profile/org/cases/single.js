import React from 'react';
import ReactDOM from 'react-dom';
import { Link } from 'react-router-dom';
import { Meteor } from 'meteor/meteor';
import { withTracker } from 'meteor/react-meteor-data';
import PropTypes from 'prop-types';

import Organizations from '/imports/api/organizations/organizations';
import Causes from '/imports/api/causes/causes';
import Beneficiaries from '/imports/api/beneficiaries/beneficiaries';
import Cases from '/imports/api/cases/cases';
import Needs from '/imports/api/needs/needs';
import Donations from '/imports/api/donations/donations';
import { removeCase } from '/imports/api/cases/methods';
import { removeNeed } from '/imports/api/needs/methods';
import { markCollected } from '/imports/api/donations/methods';

import { caseStatuses, urgencyFlags, caseOwnerTypes } from '/imports/api/commonValues.js';

import { Authorize } from '/imports/api/utils';

import {
  Table, Modal, Button, Icon, Tag, Spin, Divider,
} from 'antd/lib';
import message from 'antd/lib/message';

class Single extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      ready: !props.loading,
    };

    this.scrolledToTarget = false;
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      ready: !nextProps.loading,
	  });

    if (!this.scrolledToTarget) {
      const target = this.props.target;
      if (target && this.refs[target]) {
        const elem = ReactDOM.findDOMNode(this.refs[target]);
        window.scrollTo(0, elem.offsetTop);
        this.scrolledToTarget = true;
      }

      if (!target) { this.scrolledToTarget = true; }
    }
  }

  removeCase(casee) {
    const history = this.props.history;
    Modal.confirm({
      title: `Delete case ${casee.title}`,
      description: 'Are your sure you want to delete this case?',
      okText: 'Delete',
      okType: 'danger',
      onOk() {
        removeCase.call({
          caseId: casee._id, userId: casee.userId, ownerId: casee.owner.id,
        }, (err, res) => {
          if (err) {
            message.error('An error occurred!');
          } else {
            message.success('Case deleted successfully');
            history.push(`/profile/org/${casee.owner.id}/cases`);
          }
        });
      },
    });
  }

  removeNeed(need) {
    const history = this.props.history;
    const orgId = this.props.match.params.id;
    Modal.confirm({
      title: `Delete need ${need.title}`,
      description: 'Are your sure you want to delete this need?',
      okText: 'Delete',
      okType: 'danger',
      onOk() {
        removeNeed.call({
          needId: need._id, userId: need.userId, caseId: need.caseId, orgId,
        }, (err, res) => {
          if (err) {
            message.error('An error occurred!');
          } else {
            message.success('Need deleted successfully');
            history.push(`/profile/org/${orgId}/cases/${need.caseId}`);
          }
        });
      },
    });
  }

  markCollected(donation) {
    markCollected.call({ donation }, (err, res) => {
      if (err) {
        message.error(err.toString());
      } else {
        message.success('Donation status set to collected');
      }
    });
  }

  renderCauses() {
    const causes = this.props.causes;
    return causes.map(cause => <Tag key={cause._id} color="orange">{cause.name}</Tag>);
  }

  needColumns() {
    const orgId = this.props.match.params.id || this.props.case.owner.id;
    const fromFeed = this.props.mode && this.props.mode === 'feed';
    return [
      {
			  title: 'Title',
			  dataIndex: 'title',
			  key: 'title',
      },
      {
			  title: 'Needed',
			  dataIndex: 'quantityNeeded',
			  key: 'quantityNeeded',
      },
      {
			  title: 'Fulfilled',
			  dataIndex: 'quantityFulfilled',
			  key: 'quantityFulfilled',
      },
      {
			  title: 'Urgency',
			  dataIndex: 'urgency',
			  key: 'urgency',
      },
      {
        title: 'Actions',
        key: 'actions',
        render: (text, record) => (
          <div>
            {fromFeed
              ? (
                <div>
                  <Link to={`/cases/${record.caseId}/needs/${record._id}`}>View</Link>
                  <Divider type="vertical" />
                  <Link to={`/cases/${record.caseId}/needs/${record._id}/donate`}>Donate</Link>
                </div>
              ) : (
                <div>
                  <Link to={`/profile/org/${orgId}/cases/${record.caseId}/needs/${record._id}`}>View</Link>
                  <Divider type="vertical" />
                  <Link to={`/profile/org/${orgId}/cases/${record.caseId}/needs/${record._id}/edit`}>Edit</Link>
                  <Divider type="vertical" />
                  <a href="javascript:void(0);" onClick={() => this.removeNeed(record)}>Delete</a>
                </div>
              )}
          </div>
        ),
      },
    ];
  }

  donationColumns() {
    return [
      {
			  title: 'ID',
			  dataIndex: '_id',
			  key: 'id',
      },
      {
			  title: 'Type',
			  dataIndex: 'type',
			  key: 'type',
      },
      {
			  title: 'Quantity',
			  key: 'quantity',
        render: (text, record) => {
          const unit = (record.moneyValue == 0 || !record.moneyValue) && record.type == 'money' ? 'EGP' : 'Items';
          return (
            <span>
              {`${record.quantity} ${unit}`}
            </span>
          );
        },
      },
      {
        title: 'Status',
        dataIndex: 'status',
        key: 'status',
      },
      {
        title: 'Actions',
        key: 'actions',
        render: (text, record) => (
          <div>
            <Link to={`/cases/${record.caseId}/needs/${record.needId}`}>View Need</Link>
            {record.status == 'Pending' && this.props.canManage
              ? (
                <span>
                  <Divider type="vertical" />
                  <a href="javascript:void(0);" onClick={() => this.markCollected(record)}>Mark Collected</a>
                </span>
              ) : ''
            }
          </div>
        ),
      },
    ];
  }

  donorColumns() {
    return [
      {
			  title: 'ID',
			  dataIndex: '_id',
			  key: 'id',
      },
      {
			  title: 'Email Address',
			  dataIndex: 'emails.0.address',
			  key: 'email',
      },
      {
			  title: 'Username',
			  dataIndex: 'username',
			  key: 'username',
      },
      {
        title: 'Actions',
        key: 'actions',
        render: (text, record) => (
          <div>
            <Link to={`/profile/${record._id}`}>Profile</Link>
          </div>
        ),
      },
    ];
  }

  render() {
    const { ready } = this.state;
    if (!ready) return <Spin size="large" />;
    const orgId = this.props.match.params.id || this.props.case.owner.id;
    const fromFeed = this.props.mode && this.props.mode === 'feed';
    const needsSource = this.props.needs.map((need) => {
      need.key = need._id;
      return need;
    });

    const donationsSource = this.props.donations.map((donation) => {
      donation.key = donation._id;
      return donation;
    });

    const donorsSource = this.props.donors.map((donor) => {
      donor.key = donor._id;
      return donor;
    });

    return (
      <div className="single-view case-view posrel">
        <div>
          <div className="single-view-actions">
            {fromFeed ? ''
              : (
                <div>
                  <Link to={`/profile/org/${orgId}/cases/${this.props.case._id}/edit`}>
                    <Button type="default" icon="edit">
                  Edit
                    </Button>
                  </Link>
              &nbsp;
                  <Link to={`/profile/org/${orgId}/cases/${this.props.case._id}/new_need`}>
                    <Button type="default" icon="plus">
                  New Need
                    </Button>
                  </Link>
              &nbsp;
                  <Button type="danger" icon="delete" onClick={() => this.removeCase(this.props.case)}>
                Delete
                  </Button>
                </div>
              )}
          </div>
          <table>
            <thead>
              <tr>
                <td colSpan="2">
                  <h2> Case details </h2>
                </td>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>Case Title</td>
                <td>{this.props.case.title}</td>
              </tr>
              <tr>
                <td>Case Description</td>
                <td>{this.props.case.description}</td>
              </tr>
              <tr>
                <td>Case Location</td>
                <td>{this.props.case.location}</td>
              </tr>
              <tr>
                <td>Case Causes </td>
                <td>{this.renderCauses()}</td>
              </tr>
              <tr>
                <td>Responsible</td>
                <td>
                  {this.props.responsible._id
                    ? this.props.responsible.username || this.props.responsible.emails[0].address : ''}
                </td>
              </tr>
              <tr>
                <td>Researcher</td>
                <td>
                  {this.props.researcher._id
                    ? this.props.researcher.username || this.props.researcher.emails[0].address : ''}
                </td>
              </tr>
              <tr>
                <td>Owner</td>
                {this.props.owner.username
                  ? <td>{this.props.owner.username || this.props.owner.emails[0].address}</td>
                  : <td>{this.props.owner.name.en}</td>
                }
              </tr>
              <tr>
                <td>Status</td>
                <td>{this.props.case.status}</td>
              </tr>
              <tr>
                <td>Emergnecy</td>
                <td>{this.props.case.urgency}</td>
              </tr>
              <tr>
                <td>Due Date</td>
                <td>{this.props.case.dueDate.toString()}</td>
              </tr>
              <tr>
                <td>Beneficiary</td>
                <td>
                  {!!this.props.ben && this.props.ben._id
                    ? (
                      <Link to={`/profile/org/${this.props.match.params.id}/beneficiaries/${this.props.ben._id}`}>
                        {this.props.ben.name.en}
                      </Link>
                    ) : ''
                  }
                </td>
              </tr>
            </tbody>
          </table>
          <Divider />
          <div ref="needs">
            <h2>Case needs</h2>
            <Table dataSource={needsSource} columns={this.needColumns()} pagination={false} />
          </div>
          <br />
          <br />
          <div ref="donations">
            <h2>Case donations</h2>
            <Table dataSource={donationsSource} columns={this.donationColumns()} pagination={false} />
          </div>
          <br />
          <br />
          <div ref="donors">
            <h2>Case donors</h2>
            <Table dataSource={donorsSource} columns={this.donorColumns()} pagination={false} />
          </div>
        </div>
      </div>
    );
  }
}

Single.propTypes = {
  case: PropTypes.object,
  responsible: PropTypes.object,
  researcher: PropTypes.object,
  owner: PropTypes.object,
  causes: PropTypes.array,
  loading: PropTypes.bool,
  ben: PropTypes.object,
  needs: PropTypes.array,
  canManage: PropTypes.bool,
  donors: PropTypes.array,
};

export default withTracker((props) => {
  const caseId = props.match.params.cid;
  const orgId = props.match.params.id;
  const sub = Meteor.subscribe('cases.single', orgId, caseId);
  const casee = Cases.findOne({ _id: caseId });
  const caseReady = sub.ready();
  let owner; let ben; let responsible; let researcher; let causes; let needs; let canManage;


  let donations; let donationsReady; let
    donors;
  if (caseReady) {
    const sub1 = Meteor.subscribe('case.users', casee);
    const sub2 = Meteor.subscribe('organizations.single', casee.owner.id);
    const sub3 = Meteor.subscribe('causes.case', casee);
    const sub4 = Meteor.subscribe('beneficiaries.single', orgId, casee.beneficiary.id);
    const sub5 = Meteor.subscribe('needs.case', casee);

    responsible = Meteor.users.findOne(casee.responsible);
    researcher = responsible;
    owner = casee.owner.type === 'individual'
      ? Meteor.users.findOne({ _id: casee.owner.id })
      : Organizations.findOne({ _id: casee.owner.id });
    causes = Causes.find({ _id: { $in: casee.causeIds } });
    ben = Beneficiaries.findOne(casee.beneficiary.id);
    needs = Needs.find({ caseId: casee._id });
    canManage = Authorize(`manage-org-${casee.owner.id}`, true);
  }

  // Load the donors after every thing is loaded
  if (!!owner && !!ben) {
    const donationsSub = Meteor.subscribe('donations.case', caseId);
    donationsReady = donationsSub.ready();
    donations = Donations.find({ caseId }).fetch();
    if (donationsSub.ready()) {
      const donorIds = _.pluck(donations, 'userId');
      Meteor.subscribe('case.donors', donorIds);
      donors = Meteor.users.find({ _id: { $in: donorIds } });
    }
  }

  return {
    case: casee,
    responsible: responsible || {},
    researcher: researcher || {},
    owner,
    causes: causes ? causes.fetch() : [],
    loading: !owner || !ben,
    ben,
    needs: needs ? needs.fetch() : [],
    canManage,
    donations: donationsReady ? donations : [],
    donors: donors ? donors.fetch() : [],
  };
})(Single);
