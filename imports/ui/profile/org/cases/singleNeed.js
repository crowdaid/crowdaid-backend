import React from 'react';
import { Link } from 'react-router-dom';
import { Meteor } from 'meteor/meteor';
import { withTracker } from 'meteor/react-meteor-data';
import PropTypes from 'prop-types';

import Cases from '/imports/api/cases/cases';
import Needs from '/imports/api/needs/needs';
import Donations from '/imports/api/donations/donations';
import { removeNeed } from '/imports/api/needs/methods';
import { markCollected } from '/imports/api/donations/methods';
import { Authorize } from '/imports/api/utils';

import {
  Table, Modal, Button, Icon, Spin, Tag, Divider,
} from 'antd/lib';
import message from 'antd/lib/message';

class SingleNeed extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      ready: !props.loading,
    };
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      ready: !nextProps.loading,
	  });
  }

  removeNeed(need) {
    const history = this.props.history;
    const orgId = this.props.match.params.id;
    Modal.confirm({
      title: `Delete need ${need.title}`,
      description: 'Are your sure you want to delete this need?',
      okText: 'Delete',
      okType: 'danger',
      onOk() {
        removeNeed.call({
          needId: need._id, userId: need.userId, caseId: need.caseId, orgId,
        }, (err, res) => {
          if (err) {
            message.error('An error occurred!');
          } else {
            message.success('Need deleted successfully');
            history.push(`/profile/org/${orgId}/cases/${need.caseId}`);
          }
        });
      },
    });
  }

  markCollected(donation) {
    markCollected.call({ donation }, (err, res) => {
      if (err) {
        message.error(err.toString());
      } else {
        message.success('Donation status set to collected');
      }
    });
  }

  donationColumns() {
    return [
      {
			  title: 'ID',
			  dataIndex: '_id',
			  key: 'id',
      },
      {
			  title: 'Type',
			  dataIndex: 'type',
			  key: 'type',
      },
      {
			  title: 'Quantity',
			  key: 'quantity',
        render: (text, record) => {
          const unit = (record.moneyValue == 0 || !record.moneyValue) && record.type == 'money' ? 'EGP' : 'Items';
          return (
            <span>
              {`${record.quantity} ${unit}`}
            </span>
          );
        },
      },
      {
        title: 'Status',
        dataIndex: 'status',
        key: 'status',
      },
      {
        title: 'Actions',
        key: 'actions',
        render: (text, record) => (
          <div>
            <Link to={`/cases/${record.caseId}`}>View case</Link>
            {record.status == 'Pending' && this.props.canManage
              ? (
                <span>
                  <Divider type="vertical" />
                  <a href="javascript:void(0);" onClick={() => this.markCollected(record)}>Mark Collected</a>
                </span>
              ) : ''
            }
          </div>
        ),
      },
    ];
  }

  donorColumns() {
    return [
      {
			  title: 'ID',
			  dataIndex: '_id',
			  key: 'id',
      },
      {
			  title: 'Email Address',
			  dataIndex: 'emails.0.address',
			  key: 'email',
      },
      {
			  title: 'Username',
			  dataIndex: 'username',
			  key: 'username',
      },
      {
        title: 'Actions',
        key: 'actions',
        render: (text, record) => (
          <div>
            <Link to={`/profile/${record._id}`}>Profile</Link>
          </div>
        ),
      },
    ];
  }

  render() {
    const { ready } = this.state;
    if (!ready) return <Spin size="large" />;
    const orgId = this.props.match.params.id;
    const caseId = this.props.match.params.cid;

    const donationsSource = this.props.donations.map((donation) => {
      donation.key = donation._id;
      return donation;
    });

    const donorsSource = this.props.donors.map((donor) => {
      donor.key = donor._id;
      return donor;
    });

    return (
      <div className="single-view need-view posrel">
        <div>
          <div className="single-view-actions">
            {orgId && !this.props.mode
              ? (
                <div>
                  <Link to={`/profile/org/${orgId}/cases/${caseId}`}>
                    <Button type="default" icon="left">
                    Back to case
                    </Button>
                  </Link>
                &nbsp;
                  <Link to={`/profile/org/${orgId}/cases/${caseId}/needs/${this.props.need._id}/edit`}>
                    <Button type="default" icon="edit">
                    Edit
                    </Button>
                  </Link>
                &nbsp;
                  <Button type="danger" icon="delete" onClick={() => this.removeNeed(this.props.need)}>
                  Delete
                  </Button>
                </div>
              ) : (
                <div>
                  {this.props.need.status == 'fulfilled' ? <Tag color="green">Fulfilled</Tag> : ''}
                &nbsp;
                &nbsp;
                  <Tag color="blue">
                    {`${this.props.collectedDonations.length} completed donations`}
                  </Tag>
                &nbsp;
                &nbsp;
                  <Link to={`/cases/${this.props.caseId}/needs/${this.props.need._id}/donate`}>
                    <Button type="default" icon="pay-circle">
                    Donate
                    </Button>
                  </Link>
                </div>
              )}
          </div>
          <table>
            <thead>
              <tr>
                <td colSpan="2">
                  <h2> Need details </h2>
                </td>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>Need Title</td>
                <td>{this.props.need.title}</td>
              </tr>
              <tr>
                <td>Need Description</td>
                <td>{this.props.need.description}</td>
              </tr>
              <tr>
                <td>Is Quantifiable?</td>
                <td>{this.props.need.isQuantifiable ? 'Yes' : 'No'}</td>
              </tr>
              <tr>
                <td>Quantity Needed </td>
                <td>{this.props.need.quantityNeeded}</td>
              </tr>
              <tr>
                <td>Quantity Fulfilled</td>
                <td>{this.props.need.quantityFulfilled}</td>
              </tr>
              <tr>
                <td>Due Date</td>
                <td>{this.props.need.dueDate.toString()}</td>
              </tr>
              <tr>
                <td>Urgency</td>
                <td>{this.props.need.urgency}</td>
              </tr>
              <tr>
                <td>Frequency</td>
                <td>{this.props.need.frequency}</td>
              </tr>
              <tr>
                <td>Status</td>
                <td>{this.props.need.status}</td>
              </tr>
            </tbody>
          </table>
          <Divider />
          <div ref="donations">
            <h2>Need donations</h2>
            <Table dataSource={donationsSource} columns={this.donationColumns()} pagination={false} />
          </div>
          <br />
          <br />
          <div ref="donors">
            <h2>Need donors</h2>
            <Table dataSource={donorsSource} columns={this.donorColumns()} pagination={false} />
          </div>
        </div>
      </div>
    );
  }
}

SingleNeed.propTypes = {
  need: PropTypes.object,
  loading: PropTypes.bool,
  caseId: PropTypes.string,
  donations: PropTypes.array,
  donors: PropTypes.array,
};

export default withTracker((props) => {
  const needId = props.match.params.nid;
  const caseId = props.match.params.cid;
  const orgId = props.match.params.id;
  const needHandle = Meteor.subscribe('needs.single', needId);
  const donationsHandle = Meteor.subscribe('donations.need', needId);
  const need = Needs.findOne({ _id: needId });
  const donations = Donations.find({ needId }).fetch();
  const collectedDonations = Donations.find({ needId, status: 'Collected' });
  let donors;
  if (donationsHandle.ready()) {
    const donorIds = _.pluck(donations, 'userId');
    const donorsHandle = Meteor.subscribe('need.donors', donorIds);
    donors = Meteor.users.find({ _id: { $in: donorIds } });
  }

  return {
    need,
    loading: !needHandle.ready(),
    caseId,
    donations,
    donors: donors ? donors.fetch() : [],
    collectedDonations: collectedDonations.fetch(),
    canManage: Authorize(`manage-org-${orgId}`, true),
  };
})(SingleNeed);
