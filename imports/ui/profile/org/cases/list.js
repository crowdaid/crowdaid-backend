import React from 'react';
import { Link } from 'react-router-dom';
import { Meteor } from 'meteor/meteor';
import { ReactiveVar } from 'meteor/reactive-var';
import { withTracker } from 'meteor/react-meteor-data';
import PropTypes from 'prop-types';
import Cases from '/imports/api/cases/cases';
import { removeCase } from '/imports/api/cases/methods';
import { Table, Modal, Divider } from 'antd/lib';
import message from 'antd/lib/message';

const PerPage = 10;
const PageNumber = new ReactiveVar(1);

class List extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      ready: false,
    };
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      ready: !nextProps.loading,
	  });
  }

  removeCase(casee) {
    const history = this.props.history;
    const orgId = this.props.orgId;
    Modal.confirm({
      title: `Delete case ${casee.title}`,
      description: 'Are your sure you want to delete this case?',
      okText: 'Delete',
      okType: 'danger',
      onOk() {
        removeCase.call({
          caseId: casee._id, userId: casee.userId, ownerId: casee.owner.id,
        }, (err, res) => {
          if (err) {
            message.error('An error occurred!');
          } else {
            message.success('Case deleted successfully');
            history.push(`/profile/org/${orgId}/cases`);
          }
        });
      },
    });
  }

  render() {
    const orgId = this.props.match.params.id;
    const dataSource = this.props.cases.map((casee) => {
      casee.key = casee._id;
      return casee;
    });

    const columns = [
      {
			  title: 'Title',
			  dataIndex: 'title',
			  key: 'title',
      },
      {
			  title: 'Location',
			  dataIndex: 'location',
			  key: 'location',
      },
      {
			  title: 'Urgency',
			  dataIndex: 'urgency',
			  key: 'urgency',
      },
      {
			  title: 'Status',
			  dataIndex: 'status',
			  key: 'status',
      },
      {
        title: 'Actions',
        key: 'actions',
        render: (text, record) => (
          <div>
            <Link to={`/profile/org/${orgId}/cases/${record._id}`}>View</Link>
            <Divider type="vertical" />
            <Link to={`/profile/org/${orgId}/cases/${record._id}/edit`}>Edit</Link>
            <Divider type="vertical" />
            <a href="javascript:void(0);" onClick={() => this.removeCase(record)}>Delete</a>
          </div>
        ),
      },
    ];

    return (
      <Table pagination={this.props.pagination} dataSource={dataSource} columns={columns} />
    );
  }
}

List.propTypes = {
  orgId: PropTypes.string,
  user: PropTypes.object,
  pagination: PropTypes.object,
  loading: PropTypes.bool,
  cases: PropTypes.array,
};

export default withTracker((props) => {
  const orgId = props.match.params.id;
  const sub = Meteor.subscribe('org.cases', orgId);
  const totalCases = Cases.find({ 'owner.id': orgId }).count();
  const cases = Cases.find({ 'owner.id': orgId }, {
  	limit: PerPage, skip: (PageNumber.get() - 1) * PerPage, sort: { createdAt: 'DESC' },
  });
  return {
    orgId,
    user: Meteor.user(),
    cases: cases.fetch(),
    loading: !sub.ready(),
    pagination: {
      position: 'bottom',
      total: totalCases,
      defaultCurrent: 1,
      pageSize: PerPage,
      onChange(page, perPage) {
        PageNumber.set(page);
      },
    },
  };
})(List);
