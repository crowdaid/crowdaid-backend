import React from 'react';
import { Meteor } from 'meteor/meteor';
import { withTracker } from 'meteor/react-meteor-data';
import { moment } from 'meteor/momentjs:moment';
import PropTypes from 'prop-types';
import Cases from '/imports/api/cases/cases';
import { updateCase } from '/imports/api/cases/methods';
import { caseStatuses, urgencyFlags, caseOwnerTypes } from '/imports/api/commonValues.js';
import Organizations from '/imports/api/organizations/organizations';
import Causes from '/imports/api/causes/causes';
import Beneficiaries from '/imports/api/beneficiaries/beneficiaries';

import {
  Form, Input, Select, DatePicker, Button, Icon, Spin,
} from 'antd/lib';
import message from 'antd/lib/message';

const FormItem = Form.Item;
const Option = Select.Option;

class Edit extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      staff: [],
      bens: [],
      loading: false,
      searchval: null,
      ready: !props.loading,
    };

    this.dueDate = null;
    this.causes = [];
    this.urgency = null;
    this.status = null;
    this.benId = null;
    this.responsible = null;
    this.responsibleVal = null;
    this.benVal = null;
  }

  componentWillReceiveProps(nextProps) {
    if (!nextProps.loading) {
      this.dueDate = nextProps.case.dueDate;
      this.causes = nextProps.case.causeIds;
      this.urgency = nextProps.case.urgency;
      this.status = nextProps.case.status;
      this.benId = nextProps.case.beneficiary.id;
      this.responsible = nextProps.case.responsible;
      if (this.responsibleVal == null) {
        const resp = nextProps.resp;
        console.log(resp);
        this.responsibleVal = resp.username ? resp.username : resp.emails[0].address;
      }

      if (!this.benVal) {
        this.benVal = nextProps.ben.name.en;
      }
    }

    this.setState({ ready: !nextProps.loading });
  }

  handleSubmit() {
    this.setState({ loading: true });
    const form = document.getElementById('editCaseForm');
    const orgId = this.props.orgId;
    const newCaseData = {
      _id: this.props.match.params.cid,
      title: form.title.value,
      description: form.description.value,
      location: form.location.value,
      causeIds: this.causes,
      dueDate: this.dueDate,
      responsible: this.responsible,
      researcher: this.responsible,
      beneficiary: {
        type: 'individual',
        id: this.benId,
      },
      owner: {
        type: this.props.case.owner.type,
        id: this.props.case.owner.id,
      },
      status: this.status,
      urgency: this.urgency,
      userId: Meteor.userId(),
    };

    updateCase.call(newCaseData, (err, res) => {
      if (err) {
        message.error(err.toString());
        this.setState({ loading: false });
      } else {
        message.success('Case updated successfully');
        this.setState({ loading: false });
        this.props.history.push(`/profile/org/${orgId}/cases/${newCaseData._id}`);
      }
    });
  }

  ucfirst(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
  }

  handleBenSearch(value) {
    this.setState({ bens: Beneficiaries.find({ orgId: this.props.orgId }) });
  }

  handleBenChange(value) {
    this.benId = value;
    this.setState({ searching: false, bens: [] });
  }

  handleResponsibleSearch(value) {
    const org = this.props.org;
    const userIds = [org.claimedBy].concat(org.employees);
    this.setState({ staff: Meteor.users.find({ _id: { $in: userIds } }) });
  }

  handleResponsibleChange(value) {
    this.responsible = value;
    this.setState({ searching: false, staff: [] });
  }

  render() {
    const {
      staff, bens, loading, searchval, ready,
    } = this.state;
    if (!ready) { return <Spin size="large" />; }
    const flags = urgencyFlags.map(flag => <Option key={flag} defaultValue={flag}>{this.ucfirst(flag)}</Option>);
    const statuses = caseStatuses.map(st => <Option key={st} defaultValue={st}>{this.ucfirst(st)}</Option>);
    const causes = this.props.causes.map(cause => <Option key={cause._id} defaultValue={cause._id}>{cause.name}</Option>);
    const org_staff = staff.map(user => <Option key={user._id} defaultValue={user._id}>{user.username || user.emails[0].address}</Option>);
    const org_bens = bens.map(ben => <Option key={ben._id} defaultValue={ben._id}>{ben.name.en}</Option>);
    return (
      <div>
        <h2>
Edit Case
          {this.props.case.title}
        </h2>
        <Form layout="vertical" id="editCaseForm" style={{ width: '70%' }}>
          <FormItem label="Case Title">
            <Input
              id="title"
              type="text"
              defaultValue={this.props.case.title}
              placeholder="Case Title"
            />
          </FormItem>
          <FormItem label="Case Description">
            <Input.TextArea
              id="description"
              defaultValue={this.props.case.description}
              rows={4}
              placeholder="Case Description"
            />
          </FormItem>
          <FormItem label="Case Location">
            <Input
              id="location"
              type="text"
              defaultValue={this.props.case.location}
              placeholder="Case Location"
            />
          </FormItem>
          <FormItem label="Due Date">
            <DatePicker format="YYYY-MM-DD" id="dueDate" defaultValue={moment(this.dueDate)} onChange={(moment, string) => { this.dueDate = string; }} />
          </FormItem>
          <FormItem label="Related Causes">
            <Select
              mode="multiple"
              defaultValue={this.causes}
              onChange={(value) => { this.causes = value; }}
              placeholder="Select causes related to this case"
              id="causes"
            >
              {causes}
            </Select>
          </FormItem>
          <FormItem label="Beneficiary">
            <Select
              onSearch={this.handleBenSearch.bind(this)}
              onChange={this.handleBenChange.bind(this)}
              showSearch
              filterOption={false}
              defaultValue={this.benVal}
              notFoundContent={searchval == null ? 'Type to start search' : 'No results found'}
              placeholder="Select the beneficiary of this case"
              id="status"
            >
              {org_bens}
            </Select>
          </FormItem>
          <FormItem label="Responsible">
            <Select
              onSearch={this.handleResponsibleSearch.bind(this)}
              onChange={this.handleResponsibleChange.bind(this)}
              showSearch
              filterOption={false}
              defaultValue={this.responsibleVal}
              notFoundContent={searchval == null ? 'Type to start search' : 'No results found'}
              placeholder="Who will be responsible for this case?"
              id="responsible"
            >
              {org_staff}
            </Select>
          </FormItem>
          <FormItem label="Case Urgency">
            <Select
              onChange={(value) => { this.urgency = value; }}
              defaultValue={this.urgency}
              placeholder="Select case urgency"
              id="urgency"
            >
              {flags}
            </Select>
          </FormItem>
          <FormItem label="Case Status">
            <Select
              onChange={(value) => { this.status = value; }}
              defaultValue={this.status}
              placeholder="Select case status"
              id="status"
            >
              {statuses}
            </Select>
          </FormItem>
          <FormItem>
            <Button key="submit" type="primary" onClick={this.handleSubmit.bind(this)} loading={loading}>Save Changes</Button>
          </FormItem>
        </Form>
      </div>
    );
  }
}

Edit.propTypes = {
  case: PropTypes.object,
  orgId: PropTypes.string,
  org: PropTypes.object,
  causes: PropTypes.array,
  loading: PropTypes.bool,
  resp: PropTypes.object,
  ben: PropTypes.object,
};

export default withTracker((props) => {
  const caseId = props.match.params.cid;
  const orgId = props.match.params.id;
  const sub0 = Meteor.subscribe('organizations.single', orgId);
  const sub = Meteor.subscribe('cases.single', orgId, caseId);
  const sub1 = Meteor.subscribe('org.beneficiaries', orgId);
  const orgReady = sub0.ready();
  const caseReady = sub.ready();
  const org = Organizations.findOne(orgId);
  const casee = Cases.findOne(caseId);
  let causes; let resp; let
    ben;
  if (org && casee) {
    const sub2 = Meteor.subscribe('org.users', org);
    resp = Meteor.users.findOne(casee.responsible);
    ben = Beneficiaries.findOne(casee.beneficiary.id);
    const sub3 = Meteor.subscribe('causes.org', org);
    causes = Causes.find({ _id: { $in: org.causes } });
  }
  return {
    case: casee,
    orgId,
    org,
    causes: causes ? causes.fetch() : [],
    loading: !orgReady || !caseReady || !casee || !resp || !ben,
    resp,
    ben,
  };
})(Edit);
