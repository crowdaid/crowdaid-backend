import React from 'react';
import { Meteor } from 'meteor/meteor';
import { withTracker } from 'meteor/react-meteor-data';
import PropTypes from 'prop-types';
import Needs from '/imports/api/needs/needs';
import { updateNeed } from '/imports/api/needs/methods';
import { urgencyFlags, Units, needFrequencies } from '/imports/api/commonValues';
import {
  Form, Input, InputNumber, Select, DatePicker, Button, Switch, Icon, Spin, Row, Col, Divider,
} from 'antd/lib';
import message from 'antd/lib/message';

const FormItem = Form.Item;
const Option = Select.Option;

class EditNeed extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      quantifiable: true,
      unit: 'EGP',
      loading: false,
      ready: false,
    };

    this.quantity = 1;
    this.dueDate = null;
    this.urgency = null;
    this.frequency = null;
  }

  componentWillReceiveProps(nextProps) {
    if (!nextProps.loading) {
      this.quantity = nextProps.need.quantityNeeded;
      this.dueDate = nextProps.need.dueDate;
      this.urgency = nextProps.need.urgency;
      this.frequency = nextProps.need.frequency;
      this.setState({
        quantifiable: nextProps.need.isQuantifiable,
        unit: nextProps.need.unitOfMeasure,
        ready: true,
      });
    }
  }

  setQuantifiability(val) {
    // val is a boolean value
    this.setState({ quantifiable: val });
  }

  handleQuantityChange(e) {
    const value = Math.round(Number(e.target.value));
    this.quantity = value;
    document.getElementById('quantity').value = value == 0 ? 1 : value;
  }

  handleSubmit() {
    this.setState({ loading: true });
    const form = document.getElementById('editNeedForm');
    const orgId = this.props.match.params.id;
    const caseId = this.props.match.params.cid;
    const needId = this.props.match.params.nid;
    const newNeed = {
      _id: needId,
      caseId,
      title: form.title.value,
      description: form.description.value,
      isQuantifiable: this.state.quantifiable,
      quantityNeeded: this.quantity,
      unitOfMeasure: this.state.unit,
      itemPrice: form.price ? form.price.value : this.props.need.itemPrice,
      dueDate: this.dueDate,
      urgency: this.urgency,
      frequency: this.frequency,
      status: this.props.need.status,
      userId: Meteor.userId(),
      orgId,
    };

    updateNeed.call(newNeed, (err, res) => {
      if (err) {
        message.error(err.toString());
        this.setState({ loading: false });
      } else {
        message.success('Need updated successfully');
        this.setState({ loading: false });
        this.props.history.push(`/profile/org/${orgId}/cases/${caseId}/needs/${needId}`);
      }
    });
  }

  ucfirst(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
  }

  render() {
    const {
      quantifiable, unit, loading, ready,
    } = this.state;
    if (!ready) { return <Spin size="large" />; }
    const need = this.props.need;
    const flags = urgencyFlags.map(flag => <Option key={flag} defaultValue={flag}>{this.ucfirst(flag)}</Option>);
    const units = Units.map(unit => <Option key={unit} defaultValue={unit}>{unit}</Option>);
    const frequencies = needFrequencies.map(f => <Option key={f} defaultValue={f}>{f}</Option>);
    const unitSelect = (
      <Select
        onChange={(value) => { this.setState({ unit: value }); }}
        placeholder="Unit"
        id="unit"
        style={{ width: '80' }}
        defaultValue={need.unitOfMeasure}
      >
        {units}
      </Select>
    );
    return (
      <div>
        <h2>
Edit Need:
          {need.title}
        </h2>
        <Form layout="vertical" id="editNeedForm" style={{ width: '70%' }}>
          <FormItem label="Need Title">
            <Input
              id="title"
              type="text"
              defaultValue={need.title}
              placeholder="Need Title"
            />
          </FormItem>
          <FormItem label="Need Description">
            <Input.TextArea
              id="description"
              defaultValue={need.description}
              rows={4}
              placeholder="Need Description"
            />
          </FormItem>
          <Row>
            <Col span="9">
              <FormItem label="Is it Quantifiable?" labelCol={{ span: 15 }} wrapperCol={{ span: 9 }}>
                <Switch checkedChildren="Yes" unCheckedChildren="No" defaultChecked={need.isQuantifiable} onChange={checked => this.setQuantifiability(checked)} />
              </FormItem>
            </Col>
            <Col span="15">
              {quantifiable
                ? (
                  <Row>
                    <Col span="16">
                      <FormItem>
                        <Input
                    type="number"
                    min={1}
                    style={{ width: '95%' }}
                    defaultValue={need.quantityNeeded}
                    onChange={this.handleQuantityChange.bind(this)}
                    id="quantity"
                    placeholder="Required Quantity"
                    addonAfter={unitSelect}
                  />
                      </FormItem>
                    </Col>
                    <Col span="8">
                      {unit != 'EGP'
                        ? (
                    <FormItem>
                    <InputNumber defaultValue={need.itemPrice} style={{ width: '100%' }} precision={0} min={1} step={1} id="price" placeholder="Item Price" />
                  </FormItem>
                        ) : ''
									}
                    </Col>
                  </Row>
                ) : ''
	          }
            </Col>
          </Row>
          <Row>
            <Col span={7}>
              <FormItem label="Due Date">
                <DatePicker style={{ width: '95%' }} format="YYYY-MM-DD" id="dueDate" defaultValue={moment(need.dueDate)} onChange={(moment, string) => { this.dueDate = string; }} />
              </FormItem>
            </Col>
            <Col span={9}>
              <FormItem label="Need Payment Frequency">
                <Select
                  style={{ width: '95%' }}
                  onChange={(value) => { this.frequency = value; }}
                  placeholder="Select payment frequency"
                  defaultValue={need.frequency}
                  id="frequency"
                >
                  {frequencies}
                </Select>
              </FormItem>
            </Col>
            <Col span={8}>
              <FormItem label="Need Urgency">
                <Select
                  onChange={(value) => { this.urgency = value; }}
                  placeholder="Select need urgency"
                  defaultValue={need.urgency}
                  id="urgency"
                >
                  {flags}
                </Select>
              </FormItem>
            </Col>
          </Row>
          <Divider />
          <FormItem>
            <Button key="submit" type="primary" onClick={this.handleSubmit.bind(this)} loading={loading}>Save Changes</Button>
          </FormItem>
        </Form>
      </div>
    );
  }
}

EditNeed.propTypes = {
  need: PropTypes.object,
  loading: PropTypes.bool,
};

export default withTracker((props) => {
  const needId = props.match.params.nid;
  const orgId = props.match.params.id;
  const sub = Meteor.subscribe('needs.single', needId);
  const need = Needs.findOne(needId);
  const needReady = sub.ready();

  return {
    need,
    loading: !needReady,
  };
})(EditNeed);
