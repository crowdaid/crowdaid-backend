import React from 'react';
import { Meteor } from 'meteor/meteor';
import { createNeed } from '/imports/api/needs/methods';
import { urgencyFlags, Units, needFrequencies } from '/imports/api/commonValues';
import {
  Form, Input, InputNumber, Select, DatePicker, Button, Switch, Icon, Spin, Row, Col, Divider,
} from 'antd/lib';
import message from 'antd/lib/message';

const FormItem = Form.Item;
const Option = Select.Option;

export default class CreateNeed extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      quantifiable: true,
      unit: 'EGP',
      loading: false,
    };

    this.quantity = 1;
    this.dueDate = null;
    this.urgency = null;
    this.frequency = null;
  }

  setQuantifiability(val) {
    // val is a boolean value
    this.setState({ quantifiable: val });
  }

  handleQuantityChange(e) {
    const value = Math.round(Number(e.target.value));
    this.quantity = value;
    document.getElementById('quantity').value = value <= 0 ? 1 : value;
  }

  handleSubmit() {
    this.setState({ loading: true });
    const form = document.getElementById('createNeedForm');
    const orgId = this.props.match.params.id;
    const caseId = this.props.match.params.cid;
    const newNeed = {
      caseId,
      title: form.title.value,
      description: form.description.value,
      isQuantifiable: this.state.quantifiable,
      quantityNeeded: this.quantity,
      unitOfMeasure: this.state.unit,
      itemPrice: form.price ? form.price.value : 0,
      dueDate: this.dueDate,
      urgency: this.urgency,
      frequency: this.frequency == null ? 'Once' : this.frequency,
      status: 'untouched',
      userId: Meteor.userId(),
      orgId,
    };

    createNeed.call(newNeed, (err, res) => {
      if (err) {
        message.error(err.toString());
        this.setState({ loading: false });
      } else {
        message.success('Need created successfully');
        this.setState({ loading: false });
        this.props.history.push(`/profile/org/${orgId}/cases/${caseId}/needs/${res}`);
      }
    });
  }

  ucfirst(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
  }

  render() {
    const { quantifiable, unit, loading } = this.state;
    const flags = urgencyFlags.map(flag => <Option key={flag} defaultValue={flag}>{this.ucfirst(flag)}</Option>);
    const units = Units.map(unit => <Option key={unit} defaultValue={unit}>{unit}</Option>);
    const frequencies = needFrequencies.map(f => <Option key={f} defaultValue={f}>{f}</Option>);
    const unitSelect = (
      <Select
        onChange={(value) => { this.setState({ unit: value }); }}
        placeholder="Unit"
        id="unit"
        style={{ width: '80' }}
        defaultValue="EGP"
      >
        {units}
      </Select>
    );
    return (
      <div>
        <h2>Add New Need</h2>
        <Form layout="vertical" id="createNeedForm" style={{ width: '70%' }}>
          <FormItem label="Need Title">
            <Input
              id="title"
              type="text"
              placeholder="Need Title"
            />
          </FormItem>
          <FormItem label="Need Description">
            <Input.TextArea
              id="description"
              rows={4}
              placeholder="Need Description"
            />
          </FormItem>
          <Row>
            <Col span="9">
              <FormItem label="Is it Quantifiable?" labelCol={{ span: 15 }} wrapperCol={{ span: 9 }}>
                <Switch checkedChildren="Yes" unCheckedChildren="No" defaultChecked onChange={checked => this.setQuantifiability(checked)} />
              </FormItem>
            </Col>
            <Col span="15">
              {quantifiable
                ? (
                  <Row>
                    <Col span="16">
                      <FormItem>
                        <Input
                    type="number"
                    min={1}
                    style={{ width: '95%' }}
                    onChange={this.handleQuantityChange.bind(this)}
                    id="quantity"
                    placeholder="Required Quantity"
                    addonAfter={unitSelect}
                  />
                      </FormItem>
                    </Col>
                    <Col span="8">
                      {unit != 'EGP'
                        ? (
                    <FormItem>
                    <InputNumber style={{ width: '100%' }} precision={0} min={1} step={1} id="price" placeholder="Item Price" />
                  </FormItem>
                        ) : ''
									}
                    </Col>
                  </Row>
                ) : ''
	          }
            </Col>
          </Row>
          <Row>
            <Col span={7}>
              <FormItem label="Due Date">
                <DatePicker style={{ width: '95%' }} format="YYYY-MM-DD" id="dueDate" onChange={(moment, string) => { this.dueDate = string; }} />
              </FormItem>
            </Col>
            {quantifiable
              ? (
                <Col span={9}>
                  <FormItem label="Need Payment Frequency">
                    <Select
                      style={{ width: '95%' }}
                      onChange={(value) => { this.frequency = value; }}
                      placeholder="Select payment frequency"
                      id="frequency"
                    >
                      {frequencies}
                    </Select>
                  </FormItem>
                </Col>
              ) : ''
						}
            <Col span={8}>
              <FormItem label="Need Urgency">
                <Select
                  onChange={(value) => { this.urgency = value; }}
                  placeholder="Select need urgency"
                  id="urgency"
                >
                  {flags}
                </Select>
              </FormItem>
            </Col>
          </Row>
          <Divider />
          <FormItem>
            <Button key="submit" type="primary" onClick={this.handleSubmit.bind(this)} loading={loading}>Create Need</Button>
          </FormItem>
        </Form>
      </div>
    );
  }
}
