import React from 'react';
import { Meteor } from 'meteor/meteor';
import { withTracker } from 'meteor/react-meteor-data';
import PropTypes from 'prop-types';
import Cases from '/imports/api/cases/cases';
import { createCase } from '/imports/api/cases/methods';
import { caseStatuses, urgencyFlags, caseOwnerTypes } from '/imports/api/commonValues.js';
import Organizations from '/imports/api/organizations/organizations';
import Causes from '/imports/api/causes/causes';
import Beneficiaries from '/imports/api/beneficiaries/beneficiaries';

import {
  Form, Input, Select, DatePicker, Button, Icon, Spin,
} from 'antd/lib';
import message from 'antd/lib/message';

const FormItem = Form.Item;
const Option = Select.Option;

class Create extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      staff: [],
      bens: [],
      loading: false,
      searchval: null,
      ready: !props.loading,
    };

    this.dueDate = null;
    this.causes = [];
    this.urgency = null;
    this.status = null;
    this.benId = null;
    this.responsible = null;
  }

  componentWillReceiveProps(nextProps) {
    this.setState({ ready: !nextProps.loading });
  }

  handleSubmit() {
    this.setState({ loading: true });
    const form = document.getElementById('createCaseForm');
    const orgId = this.props.match.params.id;
    const newCase = {
      title: form.title.value,
      description: form.description.value,
      location: form.location.value,
      causeIds: this.causes,
      dueDate: this.dueDate,
      responsible: this.responsible,
      researcher: this.responsible,
      beneficiary: {
        type: 'individual',
        id: this.benId,
      },
      owner: {
        type: 'organization',
        id: orgId,
      },
      status: this.status,
      urgency: this.urgency,
      userId: Meteor.userId(),
    };

    createCase.call(newCase, (err, res) => {
      if (err) {
        message.error(err.toString());
        this.setState({ loading: false });
      } else {
        message.success('Case created successfully');
        this.setState({ loading: false });
        this.props.history.push(`/profile/org/${orgId}/cases/${res}`);
      }
    });
  }

  ucfirst(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
  }

  handleBenSearch(value) {
    this.setState({ bens: Beneficiaries.find({ orgId: this.props.orgId }) });
  }

  handleBenChange(value) {
    this.benId = value;
    this.setState({ searching: false, bens: [] });
  }

  handleResponsibleSearch(value) {
    const org = this.props.org;
    const userIds = [org.claimedBy].concat(org.employees);
    this.setState({ staff: Meteor.users.find({ _id: { $in: userIds } }) });
  }

  handleResponsibleChange(value) {
    this.responsible = value;
    this.setState({ searching: false, staff: [] });
  }

  render() {
    const {
      staff, bens, loading, searchval, ready,
    } = this.state;
    if (!ready) { return <Spin size="large" />; }
    const flags = urgencyFlags.map(flag => <Option key={flag} defaultValue={flag}>{this.ucfirst(flag)}</Option>);
    const statuses = caseStatuses.map(st => <Option key={st} defaultValue={st}>{this.ucfirst(st)}</Option>);
    const causes = this.props.causes.map(cause => <Option key={cause._id} defaultValue={cause._id}>{cause.name}</Option>);
    const org_staff = staff.map(user => <Option key={user._id} defaultValue={user._id}>{user.username || user.emails[0].address}</Option>);
    const org_bens = bens.map(ben => <Option key={ben._id} defaultValue={ben._id}>{ben.name.en}</Option>);
    return (
      <div>
        <h2>Create New Case</h2>
        <Form layout="vertical" id="createCaseForm" style={{ width: '70%' }}>
          <FormItem label="Case Title">
            <Input
              id="title"
              type="text"
              placeholder="Case Title"
            />
          </FormItem>
          <FormItem label="Case Description">
            <Input.TextArea
              id="description"
              rows={4}
              placeholder="Case Description"
            />
          </FormItem>
          <FormItem label="Case Location">
            <Input
              id="location"
              type="text"
              placeholder="Case Location"
            />
          </FormItem>
          <FormItem label="Due Date">
            <DatePicker format="YYYY-MM-DD" id="dueDate" onChange={(moment, string) => { this.dueDate = string; }} />
          </FormItem>
          <FormItem label="Related Causes">
            <Select
              mode="multiple"
              onChange={(value) => { this.causes = value; }}
              placeholder="Select causes related to this case"
              id="causes"
            >
              {causes}
            </Select>
          </FormItem>
          <FormItem label="Beneficiary">
            <Select
              onSearch={this.handleBenSearch.bind(this)}
              onChange={this.handleBenChange.bind(this)}
              showSearch
              filterOption={false}
              notFoundContent={searchval == null ? 'Type to start search' : 'No results found'}
              placeholder="Select the beneficiary of this case"
              id="beneficiary"
            >
              {org_bens}
            </Select>
          </FormItem>
          <FormItem label="Responsible">
            <Select
              onSearch={this.handleResponsibleSearch.bind(this)}
              onChange={this.handleResponsibleChange.bind(this)}
              showSearch
              filterOption={false}
              notFoundContent={searchval == null ? 'Type to start search' : 'No results found'}
              placeholder="Who will be responsible for this case?"
              id="responsible"
            >
              {org_staff}
            </Select>
          </FormItem>
          <FormItem label="Case Urgency">
            <Select
              onChange={(value) => { this.urgency = value; }}
              placeholder="Select case urgency"
              id="urgency"
            >
              {flags}
            </Select>
          </FormItem>
          <FormItem label="Case Status">
            <Select
              onChange={(value) => { this.status = value; }}
              placeholder="Select case status"
              id="status"
            >
              {statuses}
            </Select>
          </FormItem>
          <FormItem>
            <Button key="submit" type="primary" onClick={this.handleSubmit.bind(this)} loading={loading}>Create Case</Button>
          </FormItem>
        </Form>
      </div>
    );
  }
}

Create.propTypes = {
  orgId: PropTypes.string,
  org: PropTypes.object,
  causes: PropTypes.array,
  loading: PropTypes.bool,
};

export default withTracker((props) => {
  const orgId = props.match.params.id;
  const sub = Meteor.subscribe('organizations.single', orgId);
  const orgReady = sub.ready();
  const org = Organizations.findOne(orgId);
  const sub1 = Meteor.subscribe('org.beneficiaries', orgId);
  let causes;
  if (orgReady) {
    const sub2 = Meteor.subscribe('causes.org', org);
    const sub3 = Meteor.subscribe('org.users', org);
    causes = Causes.find({ _id: { $in: org.causes } });
  }

  return {
    orgId,
    org,
    causes: causes ? causes.fetch() : [],
    loading: !orgReady,
  };
})(Create);
