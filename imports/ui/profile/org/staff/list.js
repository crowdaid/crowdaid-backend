import React from 'react';
import { Link } from 'react-router-dom';
import { Meteor } from 'meteor/meteor';
import { ReactiveVar } from 'meteor/reactive-var';
import { withTracker } from 'meteor/react-meteor-data';
import PropTypes from 'prop-types';
import Organizations from '/imports/api/organizations/organizations';
import { isManager } from '/imports/api/utils';
import {
  Table, Button, Tag, Icon,
} from 'antd/lib';

const PerPage = 10;
const PageNumber = new ReactiveVar(1);

class List extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      ready: false,
    };
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      ready: !nextProps.loading,
	  });
  }

  viewProfile(user) {
    this.props.history.push(`/profile/${user._id}`);
  }

  render() {
    const dataSource = this.props.users.map((user) => {
      user.key = user._id;
      return user;
    });

    const columns = [
      {
			  title: 'Username',
			  dataIndex: 'username',
			  key: 'username',
      },
      {
			  title: 'Email Address',
			  dataIndex: 'emails[0].address',
			  key: 'email',
      },
      {
        title: 'Actions',
        key: 'actions',
        render: (text, record) => (
          <span>
            {isManager(record)
              ? <a href="javascript:void(0);" onClick={() => this.viewProfile(record)}>Manager</a>
              : <a href="javascript:void(0);" onClick={() => this.viewProfile(record)}>Employee</a>
						}
          </span>
        ),
      },
    ];

    return (
      <Table pagination={this.props.pagination} dataSource={dataSource} columns={columns} />
    );
  }
}

List.propTypes = {
  user: PropTypes.object,
  org: PropTypes.object,
  loading: PropTypes.bool,
  users: PropTypes.array,
};

export default withTracker((props) => {
  const orgId = props.match.params.id;
  const sub = Meteor.subscribe('organizations.single', orgId);
  const org = Organizations.findOne({ _id: orgId });
  const ids = org ? [org.claimedBy].concat(org.employees) : [];
  const sub1 = org ? Meteor.subscribe('org.users', org) : {};
  const users = Meteor.users.find({ _id: { $in: ids } });
  return {
    user: Meteor.user(),
    users: users.fetch(),
    org,
    loading: !(!!org && sub1.ready()),
    pagination: {
      position: 'bottom',
      total: users.count(),
      defaultCurrent: 1,
      pageSize: PerPage,
      onChange(page, perPage) {
        PageNumber.set(page);
      },
    },
  };
})(List);
