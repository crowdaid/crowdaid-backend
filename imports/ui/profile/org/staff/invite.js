import React from 'react';
import { inviteEmployee } from '/imports/api/organizations/methods';
import { Form, Input, Button } from 'antd/lib';
import message from 'antd/lib/message';

const FormItem = Form.Item;

export default class Invite extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
    };
  }

  handleSubmit() {
    this.setState({ loading: true });
    const orgId = this.props.match.params.id;
    const form = document.getElementById('inviteForm');
    inviteEmployee.call({
      orgId,
      email: form.email.value,
    }, (err, res) => {
      if (err) {
        this.setState({ loading: false });
        const error = err.reason ? err.reason : err.toString();
        message.error(error);
      } else {
        this.setState({ loading: false });
        message.success(res);
        this.props.history.push(`/profile/org/${orgId}/staff`);
      }
    });
  }

  render() {
    const { loading } = this.state;
    return (
      <div>
        <h2>Invite New Employee</h2>
        <Form layout="horizontal" id="inviteForm" style={{ width: '70%' }}>
          <FormItem label="Email Address">
            <Input
              id="email"
              type="text"
              placeholder="Enter Email Address of invited employee"
            />
          </FormItem>
          <FormItem>
            <Button key="submit" type="primary" onClick={this.handleSubmit.bind(this)} loading={loading}>Send Invitation</Button>
          </FormItem>
        </Form>
      </div>
    );
  }
}
