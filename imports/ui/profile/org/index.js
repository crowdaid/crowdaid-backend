import React from 'react';
import { Switch, Route } from 'react-router-dom';
import { withTracker } from 'meteor/react-meteor-data';

import {
  Tabs, Menu, Button, Dropdown, Icon,
} from 'antd/lib';
import Details from './details';
import Edit from '/imports/ui/admin/orgs/edit';

import StaffList from './staff/list';
import InviteStaff from './staff/invite';

import BensList from './bens/list';
import CreateBen from './bens/create';
import SingleBen from './bens/single';
import EditBen from './bens/edit';

import CasesList from './cases/list';
import CreateCase from './cases/create';
import SingleCase from './cases/single';
import EditCase from './cases/edit';

import CreateNeed from './cases/createNeed';
import SingleNeed from './cases/singleNeed';
import EditNeed from './cases/editNeed';


const TabPane = Tabs.TabPane;

export default class Organization extends React.Component {
  constructor(props) {
    super(props);
    this.tabs = ['details', 'staff', 'beneficiaries', 'cases'];
    const pathArr = props.location.pathname.split('/');
    this.activeTab = this.tabs.indexOf(pathArr[4]) !== -1 ? pathArr[4] : 'details';
    this.state = {
      activeTab: this.activeTab,
    };
  }

  componentWillReceiveProps(nextProps) {
    const pathArr = nextProps.location.pathname.split('/');
    if (pathArr.length === 4 && this.state.activeTab != 'details') {
      // User clicked organizations menu item at the left sidebar
      this.setState({ activeTab: 'details' });
    } else if (pathArr.length >= 5 && this.tabs.indexOf(pathArr[4]) != -1) {
      const tab = pathArr[4];
      if (this.state.activeTab != tab) {
        this.setState({ activeTab: tab });
      }
    }
  }

  handleTabChange(key) {
    this.setState({ activeTab: key });
    this.props.history.push(`/profile/org/${this.props.match.params.id}/${key}`);
  }

  handleTabClick(key) {
    const pathArr = this.props.location.pathname.split('/');
    if (this.state.activeTab == key && pathArr.length >= 5) {
      this.props.history.push(`/profile/org/${this.props.match.params.id}/${key}`);
    }
  }

  handleAction({ item, key, keyPath }) {
    this.setState({ activeTab: item.props.tab });
    const history = this.props.history;
    const orgId = this.props.match.params.id;
    const baseUrl = `/profile/org/${orgId}/`;
    switch (key) {
      case 'edit':
        history.push(`${baseUrl}edit`);
        break;
      case 'invite':
        history.push(`${baseUrl}staff/invite`);
        break;
      case 'newben':
        history.push(`${baseUrl}beneficiaries/create`);
        break;
      case 'newcase':
        history.push(`${baseUrl}cases/create`);
        break;
      default:
        history.push(baseUrl);
    }
  }

  actions() {
    const menu = (
      <Menu onClick={this.handleAction.bind(this)}>
        <Menu.Item key="edit" tab="details">Edit Organization</Menu.Item>
        <Menu.Item key="invite" tab="staff">Invite Employee</Menu.Item>
        <Menu.Item key="newben" tab="beneficiaries">New Beneficiary</Menu.Item>
        <Menu.Item key="newcase" tab="cases">New Case</Menu.Item>
      </Menu>
    );

    return (
      <Dropdown overlay={menu} placement="bottomRight">
        <Button>
          Actions
          {' '}
          <Icon type="down" />
        </Button>
      </Dropdown>
    );
  }

  render() {
    const { activeTab } = this.state;
    return (
      <Switch>
        <Tabs tabBarExtraContent={this.actions()} defaultActiveKey={this.activeTab} activeKey={activeTab} onChange={this.handleTabChange.bind(this)} onTabClick={this.handleTabClick.bind(this)}>
          <TabPane tab="Details" key="details">
            <Route exact path="/profile/org/:id" render={props => <Details {...props} />} />
            <Route path="/profile/org/:id/details" render={props => <Details {...props} />} />
            <Route path="/profile/org/:id/edit" render={props => <Edit from="profile" {...props} />} />
          </TabPane>
          <TabPane tab="Staff" key="staff">
            <Route exact path="/profile/org/:id/staff" render={props => <StaffList {...props} />} />
            <Route path="/profile/org/:id/staff/invite" render={props => <InviteStaff {...props} />} />
          </TabPane>
          <TabPane tab="Beneficiaries" key="beneficiaries">
            <Route exact path="/profile/org/:id/beneficiaries" render={props => <BensList {...props} />} />
            <Route path="/profile/org/:id/beneficiaries/create" render={props => <CreateBen {...props} />} />
            <Route path="/profile/org/:id/beneficiaries/:bid/edit" render={props => <EditBen {...props} />} />
            <Route
              exact
              path="/profile/org/:id/beneficiaries/:bid"
              render={(props) => {
                if (props.match.params.bid == 'create') return <div />;
                return <SingleBen {...props} />;
              }}
            />
          </TabPane>
          <TabPane tab="Cases" key="cases">
            <Route exact path="/profile/org/:id/cases" render={props => <CasesList {...props} />} />
            <Route exact path="/profile/org/:id/cases/create" render={props => <CreateCase {...props} />} />
            <Route exact path="/profile/org/:id/cases/:cid/edit" render={props => <EditCase {...props} />} />
            <Route
              exact
              path="/profile/org/:id/cases/:cid"
              render={(props) => {
                if (props.match.params.cid == 'create') return <div />;
                return <SingleCase {...props} />;
              }}
            />
            <Route exact path="/profile/org/:id/cases/:cid/new_need" render={props => <CreateNeed {...props} />} />
            <Route exact path="/profile/org/:id/cases/:cid/needs/:nid" render={props => <SingleNeed {...props} />} />
            <Route exact path="/profile/org/:id/cases/:cid/needs/:nid/edit" render={props => <EditNeed {...props} />} />
          </TabPane>
        </Tabs>
      </Switch>
    );
  }
}
