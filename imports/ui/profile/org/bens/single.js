import React from 'react';
import { Link } from 'react-router-dom';
import { Meteor } from 'meteor/meteor';
import { withTracker } from 'meteor/react-meteor-data';
import PropTypes from 'prop-types';
import Beneficiaries from '/imports/api/beneficiaries/beneficiaries';
import { removeBeneficiary } from '/imports/api/beneficiaries/methods';
import {
  Modal, Button, Icon, Spin,
} from 'antd/lib';
import message from 'antd/lib/message';

class Single extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      ready: !props.loading,
    };
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      ready: !nextProps.loading,
	  });
  }

  removeBeneficiary(ben) {
    const history = this.props.history;
    Modal.confirm({
      title: `Delete beneficiary ${ben.name.en}`,
      description: 'Are your sure you want to delete this beneficiary?',
      okText: 'Delete',
      okType: 'danger',
      onOk() {
        removeBeneficiary.call({
          benId: ben._id, userId: ben.userId, orgId: ben.orgId,
        }, (err, res) => {
          if (err) {
            message.error('An error occurred!');
          } else {
            message.success('Beneficiary deleted successfully');
            history.push(`/profile/org/${ben.orgId}/beneficiaries`);
          }
        });
      },
    });
  }

  render() {
    const { ready } = this.state;
    const orgId = this.props.match.params.id;
    return (
      <div className="single-view ben-view posrel">
        { !ready ? <Spin size="large" />
          : (
            <div>
              <div className="single-view-actions">
                <Link to={`/profile/org/${orgId}/beneficiaries/${this.props.ben._id}/edit`}>
                  <Button type="default" icon="edit">
                Edit
                  </Button>
                </Link>
            &nbsp;
                <Button type="danger" icon="delete" onClick={() => this.removeBeneficiary(this.props.ben)}>
              Delete
                </Button>
              </div>
              <table>
                <thead>
                  <tr>
                    <td colSpan="2">
                      <h2> Beneficiary details </h2>
                    </td>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>English Name</td>
                    <td>{this.props.ben.name.en}</td>
                  </tr>
                  <tr>
                    <td>Arabic Name</td>
                    <td>{this.props.ben.name.ar}</td>
                  </tr>
                  <tr>
                    <td>National Id Number</td>
                    <td>{this.props.ben.nationalIdNumber}</td>
                  </tr>
                  <tr>
                    <td>Beneficiary Type </td>
                    <td>{this.props.ben.type}</td>
                  </tr>
                  <tr>
                    <td>Birth Date</td>
                    <td>{this.props.ben.birthDate.toString()}</td>
                  </tr>
                  <tr>
                    <td>About</td>
                    <td>{this.props.ben.about}</td>
                  </tr>
                  <tr>
                    <td>Verification Status</td>
                    <td>{this.props.ben.verificationStatus}</td>
                  </tr>
                </tbody>
              </table>
            </div>
          )
      }
      </div>
    );
  }
}

Single.propTypes = {
  ben: PropTypes.object,
  loading: PropTypes.bool,
};

export default withTracker((props) => {
  const benId = props.match.params.bid;
  const orgId = props.match.params.id;
  const sub = Meteor.subscribe('beneficiaries.single', orgId, benId);
  const ben = Beneficiaries.findOne({ _id: benId });
  return {
    ben,
    loading: !sub.ready(),
  };
})(Single);
