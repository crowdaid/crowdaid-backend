import React from 'react';
import { Meteor } from 'meteor/meteor';
import { withTracker } from 'meteor/react-meteor-data';
import PropTypes from 'prop-types';
import {
  Form, Input, Select, DatePicker, Button, Icon, Spin,
} from 'antd/lib';
import message from 'antd/lib/message';
import Beneficiaries from '/imports/api/beneficiaries/beneficiaries';
import { createBeneficiary } from '/imports/api/beneficiaries/methods';
import { beneficiaryTypes, verificationStatuses } from '/imports/api/commonValues.js';

const FormItem = Form.Item;
const Option = Select.Option;

class Create extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
    };

    this.birthDate = '';
    this.type = '';
    this.verification = '';
  }

  handleSubmit() {
    this.setState({ loading: true });
    const orgId = this.props.match.params.id;
    const form = document.getElementById('createBenForm');
    createBeneficiary.call({
      name: {
        ar: form.arName.value,
        en: form.enName.value,
		  },
      type: this.type,
      nationalIdNumber: form.nationalId.value,
      about: form.about.value,
      birthDate: this.birthDate,
      address: form.address.value,
      verificationStatus: this.verification,
      orgId,
      userId: form.userId.value,
    }, (err, res) => {
      if (err) {
        // An error occurred
        alert(err);
        this.setState({ loading: false });
      } else {
        this.setState({ loading: false });
        message.success('Beneficiary created successfully');
        this.props.history.push(`/profile/org/${orgId}/beneficiaries/${res}`);
      }
    });
  }

  ucfirst(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
  }

  render() {
    if (!this.props.user) { return <Spin size="large" />; }
    const { loading } = this.state;
    const types = beneficiaryTypes.map(type => <Option key={type} defaultValue={type}>{this.ucfirst(type)}</Option>);
    const statuses = verificationStatuses.map(st => <Option key={st} defaultValue={st}>{this.ucfirst(st)}</Option>);
    return (
      <div>
        <h2>Create New Beneficiary</h2>
        <Form layout="vertical" id="createBenForm" style={{ width: '70%' }}>
          <FormItem label="Arabic Name">
            <Input
              id="arName"
              type="text"
              placeholder="Beneficiary Arabic name"
            />
          </FormItem>
          <FormItem label="English Name">
            <Input
              id="enName"
              type="text"
              placeholder="Beneficiary English name"
            />
          </FormItem>
          <FormItem label="National Id Number">
            <Input
              id="nationalId"
              type="text"
              placeholder="National Id Number"
            />
          </FormItem>
          <FormItem label="Date of Birth">
            <DatePicker format="YYYY-MM-DD" id="birthDate" onChange={(moment, string) => { this.birthDate = string; }} />
          </FormItem>
          <FormItem label="Beneficiary Address">
            <Input
              id="address"
              type="text"
              placeholder="Beneficiary Address"
            />
          </FormItem>
          <FormItem label="About the beneficiary">
            <Input.TextArea
              id="about"
              type="textarea"
              rows={4}
              placeholder="About the beneficiary"
            />
          </FormItem>
          <FormItem label="Beneficiary Type">
            <Select
              onChange={(value) => { this.type = value; }}
              placeholder="Please select type"
              id="type"
            >
              {types}
            </Select>
          </FormItem>
          <FormItem label="Beneficiary verification status">
            <Select
              onChange={(value) => { this.verification = value; }}
              placeholder="Select verification status"
              id="status"
            >
              {statuses}
            </Select>
          </FormItem>
          <FormItem>
            <Button key="submit" type="primary" onClick={this.handleSubmit.bind(this)} loading={loading}>Create Beneficiary</Button>
          </FormItem>
          <input type="text" id="userId" defaultValue={this.props.user ? this.props.user._id : ''} style={{ display: 'none' }} />
        </Form>
      </div>
    );
  }
}

Create.propTypes = {
  user: PropTypes.object,
};

export default withTracker(props => ({
  user: Meteor.user(),
}))(Create);
