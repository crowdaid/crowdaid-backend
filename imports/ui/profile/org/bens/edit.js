import React from 'react';
import { Meteor } from 'meteor/meteor';
import { withTracker } from 'meteor/react-meteor-data';
import { moment } from 'meteor/momentjs:moment';
import PropTypes from 'prop-types';
import {
  Form, Input, Select, DatePicker, Button, Icon, Spin,
} from 'antd/lib';
import message from 'antd/lib/message';
import Beneficiaries from '/imports/api/beneficiaries/beneficiaries';
import { updateBeneficiary } from '/imports/api/beneficiaries/methods';
import { beneficiaryTypes, verificationStatuses } from '/imports/api/commonValues.js';

const FormItem = Form.Item;
const Option = Select.Option;

class Edit extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      ready: false,
    };

    this.birthDate = '';
    this.type = '';
    this.verification = '';
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.ben && this.type == '') {
      this.birthDate = nextProps.ben.birthDate;
      this.type = nextProps.ben.type;
      this.verification = nextProps.ben.verificationStatus;
    }

    this.setState({
      ready: !nextProps.loading,
	  });
  }

  handleSubmit() {
    this.setState({ loading: true });
    const orgId = this.props.match.params.id;
    const form = document.getElementById('editBenForm');
    updateBeneficiary.call({
      _id: this.props.match.params.bid,
      name: {
        ar: form.arName.value,
        en: form.enName.value,
		  },
      type: this.type,
      nationalIdNumber: form.nationalId.value,
      about: form.about.value,
      birthDate: this.birthDate,
      address: form.address.value,
      verificationStatus: this.verification,
      orgId,
      userId: form.userId.value,
    }, (err, res) => {
      if (err) {
        // An error occurred
        alert(err);
        this.setState({ loading: false });
      } else {
        this.setState({ loading: false });
        message.success('Beneficiary updated successfully');
        this.props.history.push(`/profile/org/${orgId}/beneficiaries/${this.props.ben._id}`);
      }
    });
  }

  ucfirst(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
  }

  render() {
    const { loading, ready } = this.state;
    if (!ready) { return <Spin size="large" />; }
    const ben = this.props.ben;
    const types = beneficiaryTypes.map(type => <Option key={type} defaultValue={type}>{this.ucfirst(type)}</Option>);
    const statuses = verificationStatuses.map(st => <Option key={st} defaultValue={st}>{this.ucfirst(st)}</Option>);
    return (
      <div>
        <h2>
Edit Beneficiary:
          {ben.name.en}
        </h2>
        <Form layout="vertical" id="editBenForm" style={{ width: '70%' }}>
          <FormItem label="Arabic Name">
            <Input
              id="arName"
              type="text"
              defaultValue={ben.name.ar}
              placeholder="Beneficiary Arabic name"
            />
          </FormItem>
          <FormItem label="English Name">
            <Input
              id="enName"
              type="text"
              defaultValue={ben.name.en}
              placeholder="Beneficiary English name"
            />
          </FormItem>
          <FormItem label="National Id Number">
            <Input
              id="nationalId"
              type="text"
              defaultValue={ben.nationalIdNumber}
              placeholder="National Id Number"
            />
          </FormItem>
          <FormItem label="Date of Birth">
            <DatePicker format="YYYY-MM-DD" id="birthDate" defaultValue={moment(this.birthDate)} onChange={(moment, string) => { this.birthDate = string; }} />
          </FormItem>
          <FormItem label="Beneficiary Address">
            <Input
              id="address"
              type="text"
              defaultValue={ben.address}
              placeholder="Beneficiary Address"
            />
          </FormItem>
          <FormItem label="About the beneficiary">
            <Input.TextArea
              id="about"
              type="textarea"
              defaultValue={ben.about}
              rows={4}
              placeholder="About the beneficiary"
            />
          </FormItem>
          <FormItem label="Beneficiary Type">
            <Select
              onChange={(value) => { this.type = value; }}
              placeholder="Please select type"
              defaultValue={ben.type}
              id="type"
            >
              {types}
            </Select>
          </FormItem>
          <FormItem label="Beneficiary verification status">
            <Select
              onChange={(value) => { this.verification = value; }}
              placeholder="Select verification status"
              defaultValue={ben.verificationStatus}
              id="status"
            >
              {statuses}
            </Select>
          </FormItem>
          <FormItem>
            <Button key="submit" type="primary" onClick={this.handleSubmit.bind(this)} loading={loading}>Save Changes</Button>
          </FormItem>
          <input type="text" id="userId" defaultValue={this.props.user ? this.props.user._id : ''} style={{ display: 'none' }} />
        </Form>
      </div>
    );
  }
}

Edit.propTypes = {
  user: PropTypes.object,
  ben: PropTypes.object,
  loading: PropTypes.bool,
};

export default withTracker((props) => {
  const benId = props.match.params.bid;
  const orgId = props.match.params.id;
  const sub = Meteor.subscribe('beneficiaries.single', orgId, benId);
  const ben = Beneficiaries.findOne({ _id: benId });
  return {
    user: Meteor.user(),
    ben,
    loading: !sub.ready(),
  };
})(Edit);
