import React from 'react';
import { Link } from 'react-router-dom';
import { Meteor } from 'meteor/meteor';
import { ReactiveVar } from 'meteor/reactive-var';
import { withTracker } from 'meteor/react-meteor-data';
import PropTypes from 'prop-types';
import Beneficiaries from '/imports/api/beneficiaries/beneficiaries';
import { removeBeneficiary } from '/imports/api/beneficiaries/methods';
import { Table, Modal, Divider } from 'antd/lib';
import message from 'antd/lib/message';

const PerPage = 10;
const PageNumber = new ReactiveVar(1);

class List extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      ready: false,
    };
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      ready: !nextProps.loading,
	  });
  }

  removeBeneficiary(ben) {
    const history = this.props.history;
    Modal.confirm({
      title: `Delete beneficiary ${ben.name.en}`,
      description: 'Are your sure you want to delete this beneficiary?',
      okText: 'Delete',
      okType: 'danger',
      onOk() {
        removeBeneficiary.call({
          benId: ben._id, userId: ben.userId, orgId: ben.orgId,
        }, (err, res) => {
          if (err) {
            message.error('An error occurred!');
          } else {
            message.success('Beneficiary deleted successfully');
            history.push(`/profile/org/${ben.orgId}/beneficiaries`);
          }
        });
      },
    });
  }

  render() {
    const orgId = this.props.match.params.id;
    const dataSource = this.props.bens.map((ben) => {
      ben.key = ben._id;
      return ben;
    });

    const columns = [
      {
			  title: 'Full Name',
			  dataIndex: 'name.en',
			  key: 'fullname',
      },
      {
			  title: 'National Id',
			  dataIndex: 'nationalIdNumber',
			  key: 'nationalId',
      },
      {
			  title: 'Type',
			  dataIndex: 'type',
			  key: 'type',
      },
      {
			  title: 'Verification',
			  dataIndex: 'verificationStatus',
			  key: 'verification',
      },
      {
        title: 'Actions',
        key: 'actions',
        render: (text, record) => (
          <div>
            <Link to={`/profile/org/${orgId}/beneficiaries/${record._id}`}>View</Link>
            <Divider type="vertical" />
            <Link to={`/profile/org/${orgId}/beneficiaries/${record._id}/edit`}>Edit</Link>
            <Divider type="vertical" />
            <a href="javascript:void(0);" onClick={() => this.removeBeneficiary(record)}>Delete</a>
          </div>
        ),
      },
    ];

    return (
      <Table pagination={this.props.pagination} dataSource={dataSource} columns={columns} />
    );
  }
}

List.propTypes = {
  user: PropTypes.object,
  pagination: PropTypes.object,
  loading: PropTypes.bool,
  bens: PropTypes.array,
};

export default withTracker((props) => {
  const orgId = props.match.params.id;
  const sub = Meteor.subscribe('org.beneficiaries', orgId);
  const totalBens = Beneficiaries.find({ orgId }).count();
  const bens = Beneficiaries.find({ orgId }, {
  	limit: PerPage, skip: (PageNumber.get() - 1) * PerPage, sort: { createdAt: 'DESC' },
  });
  return {
    user: Meteor.user(),
    bens: bens.fetch(),
    loading: !sub.ready(),
    pagination: {
      position: 'bottom',
      total: totalBens,
      defaultCurrent: 1,
      pageSize: PerPage,
      onChange(page, perPage) {
        PageNumber.set(page);
      },
    },
  };
})(List);
