import { Meteor } from 'meteor/meteor';
import { ValidatedMethod } from 'meteor/mdg:validated-method';

// eslint-disable-next-line import/prefer-default-export
export const ping = new ValidatedMethod({
  name: 'connection.ping',
  validate: null,
  run() {
    if (!Meteor.isServer) return 'PONG From Client';
    return 'PONG From Server';
  },
});
