/* eslint-disable no-underscore-dangle */
/**
 * Created by ahmad on 6/27/17.
 */
import { Meteor } from 'meteor/meteor';
import { Accounts } from 'meteor/accounts-base';
import { Roles } from 'meteor/alanning:roles';
import Organizations from '../../api/organizations/organizations';


Accounts.config({
  forbidClientAccountCreation: true,
});


ServiceConfiguration.configurations.upsert(
  { service: 'facebook' },
  {
    $set: {
      appId: Meteor.settings.oauth.facebook.appId,
      secret: Meteor.settings.oauth.facebook.secret,
    },
  },
);

// on create a user
Accounts.onCreateUser((options, account) => {
  const user = account;
  // add role donor by default
  user.roles = ['donor'];
  if (user.services.facebook) {
    const existingUser = Accounts.findUserByEmail(user.services.facebook.email);
    if (!existingUser) {
      user.emails = options.emails;
      user.mergedServices = {
        original: {
          name: 'facebook',
          createdAt: new Date(),
        },
      };
      user.username = options.profile.name.toLowerCase().replace(/(^\s+|[^a-zA-Z0-9 ]+|\s+$)/g, '').replace(/\s+/g, ' ');
      user.profile = options.profile;
      return user;
    }
    existingUser.services.facebook = user.services.facebook;
    existingUser.profile = options.profile;
    Meteor.users.remove({ _id: existingUser._id }); // remove existing record
    return existingUser; // record is re-inserted
  }

  if (user.services.password) {
    const existingUser = Meteor.users.findOne({ 'registered_emails.address': options.email });
    if (!existingUser) {
      const mergedServices = {
        original: {
          name: 'password',
          createdAt: new Date(),
        },
      };
      user.mergedServices = mergedServices;
      user.profile = options.profile;
      return user;
    }
    existingUser.username = options.username;
    existingUser.services.password = user.services.password;
    user.profile = options.profile;
    if (!existingUser.emails) {
      existingUser.emails = [
        {
          address: options.email,
          verified: true,
        },
      ];
    } else {
      existingUser.emails.push(
        {
          address: options.email,
          verified: true,
        },
      );
    }
    Meteor.users.remove({ _id: existingUser._id }); // remove existing record
    return existingUser; // record is re-inserted
  }

  return user;
});

// Login user with the right permissions
const emailRex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
Accounts.registerLoginHandler(({ pass, email, as }) => {
  if (!as) {
    throw new Error('The login request must have "as" property ');
  }
  if (!pass) {
    throw new Error('The login request must have "pass" property for password');
  }
  if (!email) {
    throw new Error('The login request must have "email" property for email');
  }
  if (!emailRex.test(String(email).toLowerCase())) {
    throw new Meteor.Error(400, 'Email is invaild');
  }
  // check user by email
  const user = Accounts.findUserByEmail(email);
  if (!user) {
    throw new Meteor.Error(403, 'User not found');
  }

  if (user.isRemoved) {
    throw new Meteor.Error(403, 'User is Disabled');
  }

  if (!user.services || !user.services.password
    || !(user.services.password.bcrypt || user.services.password.srp)) {
    throw new Meteor.Error(403, 'User has no password set');
  }
  // check if the email is verified email
  const userEmail = user.emails.filter(em => em.address === email)[0];
  if (!userEmail.verified) {
    throw new Meteor.Error(401, 'This address is not verified yet, login with your first email address and resend the verification email');
  }
  // check is the user have the permission "as"
  const isAllowed = Roles.userIsInRole(user._id, as);

  if (!isAllowed) {
    throw new Meteor.Error(401, `You don't have the permission to login as ${as}`);
  }

  // check if the user is owner or employee and have organizationId
  if ((Roles.userIsInRole(user._id, 'owner') && !user.organizationId)
  || (Roles.userIsInRole(user._id, 'employee') && !user.organizationId)) {
    throw new Meteor.Error(401, "You currently not at any organization, you can't login");
  }
  // check if the current organization is removed
  if (Roles.userIsInRole(user._id, 'owner') || Roles.userIsInRole(user._id, 'employee')) {
    const userCurrentOrg = Organizations.findOne({ _id: user.organizationId });
    // check if the organization is removed
    if (userCurrentOrg.isRemoved) {
      // get the user other organizations if any
      const userOtherOrgs = Organizations
        .find({ _id: { $ne: user.organizationId }, isRemoved: { $ne: true }, employees: user._id })
        .fetch();
      if (userOtherOrgs.length === 0) {
        throw new Meteor.Error(401,
          'Your current organization had been removed, and you currently not at any other organization');
      } else {
        // set the current organization, the first one found, and log the user
        Meteor.users.update({ _id: user._id }, {
          $set: {
            organizationId: userOtherOrgs[0]._id,
          },
        });
      }
    }
  }
  return Accounts._checkPassword(user, pass);
});

const adminDashboardUrl = Meteor.settings.public.adminInterface;
const organizationDashboardUrl = Meteor.settings.public.organizationInterface;
// admin enrollment -on the admin-dashboard
Accounts.urls.enrollAccount = (token) => {
  const user = Meteor.users.findOne({ 'services.password.reset.token': token });
  const tokenData = user.services.password.reset;
  if (tokenData.role === 'owner' || tokenData.role === 'employee') return (`${organizationDashboardUrl}/organizations-users/register/${token}`);
  return (`${adminDashboardUrl}/create-account/${token}`);
};

// configure verifyEmail services
Accounts.urls.verifyEmail = (token) => {
  const user = Meteor.users.findOne({ 'services.email.verificationTokens.token': token });
  if (Roles.userIsInRole(user._id, ['owner', 'employee'])) return (`${organizationDashboardUrl}/verify-email/${token}`);
  return (`${adminDashboardUrl}/verify-email/${token}`);
};

Accounts.emailTemplates.siteName = 'CrowdAid';
Accounts.emailTemplates.from = 'CrowdAid Admin <admin@nezam.io.com>';

Accounts.emailTemplates.enrollAccount.subject = user => 'CrowdAid create account';

Accounts.emailTemplates.enrollAccount.html = (user, url) => {
  const role = user.roles[0];
  const org = user.organizationId && Organizations.findOne({ _id: user.organizationId });
  if (role === 'owner') { // owner invitation template
    return (`
    You have been invited by <b>CrowdAid</b> 
    to be the owner of ${org.name.ar} organization <br>
    To activate your account, simply click on 
    <a href='${url}'> <b>Create Account</b></a> and fill your profile 🔐:
    `);
  } if (role === 'employee') { // employee invitation template
    return (`
    You have been invited by ${org.owner} 
    to be part of ${org.name.ar} organization team<br>
    To activate your account, simply click on 
    <a href='${url}'> <b>Create Account</b></a> and fill your profile 🔐:
    `);
  }
  return (`
    You have been invited by <b>CrowdAid</b> 
    to be an admin in crowdAid system <br>
    To activate your account, simply click on 
    <a href='${url}'> <b>Create Account</b></a> and fill your profile 🔐:
    `);
};
