// Import server startup through a single index entry point
import { Meteor } from 'meteor/meteor';
import './middlewares';
import './register-api';
import './oauth-login';
import './accounts';
import './connection';

// if (Meteor.isDevelopment) {
process.env.MAIL_URL = Meteor.settings.env.MAIL_URL;
// }
