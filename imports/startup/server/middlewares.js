import cors from 'cors';
import { WebApp } from 'meteor/webapp';

WebApp.connectHandlers.use(cors({ origin: true }));
