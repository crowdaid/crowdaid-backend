import React from 'react';
import { BrowserRouter, Route } from 'react-router-dom';

// route components
import AppContainer from '../../ui/containers/appContainer.jsx';

export const renderRoutes = () => (
  <BrowserRouter>
    {/*https://github.com/ReactTraining/react-router/issues/4781#issuecomment-287631944*/}
    <Route component={AppContainer} />
  </BrowserRouter>
);
