### Crowdaid backend
* serves as backend for all client interfaces:
 1. [Admin dashboard](https://gitlab.com/crowdaid/admin-dashboard).
 2. [Organization dashboard](https://gitlab.com/crowdaid/organization-dashboard).
 3. [Mobile app for Donors](https://gitlab.com/crowdaid/crowdaid-mobile).
